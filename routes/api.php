<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**************************Auth************************************* */

use App\Http\Controllers\API\Auth\AccountRecoveryController;
use App\Http\Controllers\API\Auth\ForgotPasswordController;
use App\Http\Controllers\API\Auth\CheckCodeController;
use App\Http\Controllers\API\Auth\ResetPasswordController;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\Auth\PropertyRightController;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\SmsController;
use Illuminate\Support\Facades\Route;

Route::get('/auth', [RegisterController::class, 'authLogin'])
                ->middleware('guest')
                ->name('auth-login');
//process : reg, confirmation code, login
Route::get('/start-steps-reg', [LoginController::class, 'startStepsReg'])
                ->middleware('guest');
Route::post('/register', [RegisterController::class, 'firstStepRegister']);

Route::post('confirmation-code',[RegisterController::class,'confirmationCode']);

Route::post('/register2', [RegisterController::class, 'secondStepRegisterPass'])
                ->middleware('guest');
Route::post('/register3', [RegisterController::class, 'thirdStepRegister'])
                ->middleware('guest');
Route::post('/register4', [RegisterController::class, 'fourthStepRegister'])
                ->middleware('guest');
Route::post('/login', [LoginController::class, 'store'])
                ->middleware('guest');
//opertaion account recovery(reset pass) by : mobile , proporty rights
Route::post('select-option-account-recovery',  [ForgotPasswordController::class,'selectOption']);//to select an option (through mobile or Property rights)
// through mobile
Route::post('password/passport_num',  ForgotPasswordController::class);//to send  entered into email entered
Route::post('password/code/check', CheckCodeController::class);////to check code entered (that sent into email) with the code that it  sent into email
Route::post('password/reset', ResetPasswordController::class);// to make reset pass this user through password entered now
//through property rights
Route::post('property-rights', PropertyRightController::class);

//get all notification for a user this system
Route::get('/get-all-notifications/{userId}', [LoginController::class, 'allNotifications'])->middleware('auth:api');
//logout
Route::middleware(['auth:api'])->group(function(){
    Route::get('/logout', [LoginController::class, 'destroy'])
                    ->name('api-llogout');
});
