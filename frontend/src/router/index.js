import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "../store/index.js"
// let token = localStorage.getItem("token")

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: "Home",
    component: () => import('@/views/dashboard/Dashboard.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){ 
        next()
      }else{
        next("/login")
      }
    }
  },
  {
    path: '/dashboard',
    component: () => import('@/views/dashboard/Dashboard.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){ 
        next() 
      }else{
        next("/login")
      }

    }
  },
  {
    path: '/dashboard',
    component: () => import('@/views/dashboard/Dashboard.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){ 
        next() 
      }else{
        next("/login")
      }

    }
  },
  
  {
    path: '/profile',
    component: () => import('@/views/pages/Admin/Profile/Profile.vue'),
//     beforeEnter: (to, from, next) => {
// let token = store.state.token

//       // ...
//       if(token){ 
//         next() 
//       }else{
//         next("/login")
//       }
 
//     }
  },
  {
    path: '/verification-requests -management',
    component: () => import('@/views/pages/Admin/Users/Verification Requests Management.vue'),
//     beforeEnter: (to, from, next) => {
// let token = store.state.token

//       // ...
//       if(token){ 
//         next() 
//       }else{
//         next("/login")
//       }
 
//     }
  },

  {
    path: '/login',
    component: () => import('@/views/pages/Admin/Auth/Login.vue'),
    meta: {
      layout: 'blank',
    },
    // beforeEnter: (to, from, next) => {
    //   // ...
    //   if(token){
    //     next("/dashboard")
    //   }else{
    //     next()
    //   }

    // }
    //   beforeEnter: (to, from, next) => {
    //   // ...
    //   if(token){
    //     next('/dashboard')
    //   }

    // }
  },
  {
    path: '/account-recovery',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/Account Recovery.vue'),
    meta: {
      layout: 'blank',
    },

  
  },
  {
    path: '/by-mobile',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/ByMobile/By Mobile.vue'),

  },
  {
    path: '/code-confimation',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/ByMobile/Check Code.vue'),

  },
  {
    path: '/reset-password',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/ByMobile/Reset Password.vue'),

  },
  

  {
    path: '/by-property-rights',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/ByPropertyRights/By Property Rights.vue'),

  },
  
  {
    path: '/by-property-rights',
    component: () => import('@/views/pages/Admin/Auth/AccountRecovery/ByPropertyRights/By Property Rights.vue'),

  },
  {
    path: '/accounts-verified-management',
    component: () => import('@/views/pages/Admin/AccountVerified/Accounts Verified Management.vue'),
    beforeEnter: (to, from, next) => {
      // ...
let token = store.state.token

      if(token){
        next()
      }else{
        next("/login")
      }
    }

  },
  
  

  {
    path: '/users-management',
    component: () => import('@/views/pages/Admin/Users/Users Management.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  }, 
  {
    path: '/trash-users-management',
    component: () => import('@/views/pages/Admin/Users/Trash Users Management'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  }, 
  {
    path: '/roles-management',
    component: () => import('@/views/pages/Admin/Roles/Roles Management.vue'),
    
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  {
    path: '/trash-roles-management',
    component: () => import('@/views/pages/Admin/Roles/Trash Roles Management.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  }, 

  
   
  {
    path: '/accounts-recovery-management-by-property-rights',
    component: () => import('@/views/pages/Admin/AccountsRecovery/Accounts Recovery By Property Rights Management.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }

  }, 
  {
    path: '/accounts-recovery-management-by-mobile',
    component: () => import('@/views/pages/Admin/AccountsRecovery/Accounts Recovery By Mobile Management.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }

  }, 
  //For Wallet SuperAdmin
  {
    path: '/accepting-redeem-requests-management',
    component: () => import('@/views/pages/Admin/Wallet/AcceptingRedeemRequests Management'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },  
  {
    path: '/reviweing-requests-management',
    component: () => import('@/views/pages/Admin/Wallet/ReviweingRequests Management'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  
  {
    path: '/delivery-money-management',
    component: () => import('@/views/pages/Admin/Wallet/DeliveryMoney Management'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },

  {
    path: '/delegates-management',
    component: () => import('@/views/pages/Admin/Wallet/Delegates Management'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  {
    path: '/before-charging-management',
    component: () => import('@/views/pages/Admin/Wallet/Before Charging Management.vue'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  
  {
    path: '/charging/:id',
    component: () => import('@/views/pages/Admin/Wallet/Charging'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  {
    path: '/finished-traveling',
    component: () => import('@/views/pages/Admin/Wallet/FinishedTraveling'),
    beforeEnter: (to, from, next) => {
let token = store.state.token

      // ...
      if(token){
        next()
      }else{
        next("/login")
      }
    }
  },
  {
    path: '/error-404',
    name: 'error-404',
    component: () => import('@/views/Error.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '*',
    redirect: 'error-404',
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
