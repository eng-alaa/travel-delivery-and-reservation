import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    baseURL: "https://www.sharmhostxyz.xyz",
    token: null,
    user: {}
  }, 
  mutations: {},
  actions: {},
  modules: {},
})
