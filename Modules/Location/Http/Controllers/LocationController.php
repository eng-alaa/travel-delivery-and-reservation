<?php

namespace Modules\Location\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Location\Entities\Location;
use Modules\Location\Http\Requests\LastLocationUserRequest;
use Modules\Auth\Entities\User;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('location::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('location::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('location::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('location::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
    
    //user
    
    public function getLocation(){
        $ip=request()->ip();
       $location= \Location::get($ip);
       return $location;
       
       $longitude=$location->longitude;
       $latitude=$location->latitude;
       $data=[
           'longitude'=>$longitude,
           'latitude'=>$latitude,
           ];
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'location user has been getten successfully',
            'data'=>$data
        ]);

    }
    
    //
    //هادا الزر موجود عشان الشخص كل فترة وهو بالرحلة يضغط عليه فبتحددث موقع الشخص هادا فبتخزن عندي بجدول موقع هادا الشخث
    //وكل عملية ضغط زر بتخزن موقعه بالجدول عشان لما بدي اتتبع رحلته انا كام متلا ب مجرد بكتي بيناته وبشوف هو لوين وصل من خلال اخر عملية تحديث له 
    
    public function updateLocation(){
        //حخزن الطول والعرض فلما الام تبحث انا من خلال الطول والعرض اللي بعته تاع التطبيق حتمسك الطول والعرض هادا وعالخريطة تظهر الموقع للام 
        $getLocation=$this->getLocation();
        $location=new  Location();
        $location->user_id=auth()->user()->id;
        $location->longitude=$getLocation->longitude;
        $location->latitude=$getLocation->latitude;
        $location->save();
        return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'location user has been updated successfully',
                'data'=>$location->load('user')
            ]);
    }
    
    public function lastLocationUser(LastLocationUserRequest $request){
        $data=$request->validated();
       $user= User::where(['passport_num'=>$data['passport_num']])->first();
      $getLastLocationUser= Location::where(['user_id'=>$user->id])->latest()->first();
              return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'location user has been getten successfully',
                'data'=>$getLastLocationUser->load('user')
            ]);
      
    }
}
