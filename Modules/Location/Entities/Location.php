<?php

namespace Modules\Location\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
class Location extends Model
{

        public function user(){
        return $this->belongsTo(User::class);
    }
}
