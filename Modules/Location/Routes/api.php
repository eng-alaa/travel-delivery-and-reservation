<?php

use Illuminate\Http\Request;
use Modules\Location\Http\Controllers\LocationController;
Route::middleware(['auth:api'])->prefix('locations')->group(function(){
            Route::get('/update-location', [LocationController::class,'updateLocation']);
            Route::post('/last-location-user', [LocationController::class,'lastLocationUser']);

});