<?php

namespace Modules\Operation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class AddPassportNumIntoReadyTravelingRequest.
 */
class AddPassportNumIntoReadyTravelingRequest extends FormRequest
{
    /**
     * @var BaseRepository
    */
    protected $baseRepo;
    /**
     * AddPassportNumIntoReadyTravelingRequest constructor.
     */
    public function __construct(BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
           //  for only superadministrator and driver   
        $authorizeRes= $this->baseRepo->authorizeSuperAndDriver();
        if($authorizeRes==true){
            return true;
        }else{
            return $this->failedAuthorization();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'passport_num'=>['required','numeric','exists:users,passport_num','unique:users,passport_num'],
            'passport_num'=>['required','numeric','exists:users,passport_num'],
                        'travel' => ['required', 'in:0,1']
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
           

        
        ];
    }
        /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
         throw new AuthorizationException(__('Only the superadministrator and driver can do this .'));
    }
}
