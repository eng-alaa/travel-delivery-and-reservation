<?php

namespace Modules\Operation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;

/**
 * Class ReseveForClientRequest.
 */
class ReseveForClientRequest extends FormRequest
{
    /**
     * @var BaseRepository
    */
    protected $baseRepo;
    /**
     * StoreRoleRequest constructor.
     */
    public function __construct(BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;
    }
    /**
     * Determine if the Role is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
           //  for only superadministrator and traveler   
        $authorizeRes= $this->baseRepo->authorizeSuperAndTraveler();
        if($authorizeRes==true){
            return true;
        }else{
            return $this->failedAuthorization();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'type' => ['required', 'in:1,2,3'],
            // 'passport_num'=>['required','numeric','exists:users,passport_num'],
         //   'client_id'=>['required','numeric','exists:users,id'],
                     'travel' => ['required', 'in:0,1'],

         'clients' => ['sometimes'],
         'clients.*'=>['exists:users,passport_num'],
            'car_id' => ['exists:cars_names,id'],
                    'security_code'=>['numeric']

        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

            'clients.*.exists' => __('One or more clients were not found'),

        ];
    }
        /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
         throw new AuthorizationException(__('Only the superadministrator and traveler can do this .'));
    }
}
