<?php

namespace Modules\Operation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class AddSecurityCodeReservationRequest.
 */
class AddSecurityCodeReservationRequest extends FormRequest
{
    /**
     * @var BaseRepository
    */
    protected $baseRepo;
    /**
     * StoreUserRequest constructor.
     */
    public function __construct(BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
           //  for only superadministrator and traveler   
        $authorizeRes= $this->baseRepo->authorizeSuperAndTraveler();
        if($authorizeRes==true){
            return true;
        }else{
            return $this->failedAuthorization();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'security_code' => ['required','max:225'],
            'passport_num'=>['required','numeric','exists:users,passport_num'],

        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        
        ];
    }
        /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
     protected function failedAuthorization()
     {
         throw new AuthorizationException(__('Only the superadministrator and traveler can do this .'));
     }
}
