<?php

namespace Modules\Operation\Http\Controllers\API\User;

use Modules\Operation\Entities\Payment;
use Modules\Operation\Entities\ReadyTraveling;
use Modules\Operation\Entities\TypeReservation;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\UserOriginalWallet;
use Modules\Wallet\Entities\UserHiddenWallet;
use Modules\Auth\Entities\User;
use Modules\Auth\Entities\Role;
use Modules\Operation\Entities\Reservation;
use Modules\Wallet\Entities\SystemWallet;
use Modules\Wallet\Entities\PointsNotAllowedUse;

use Modules\Operation\Http\Requests\addTypeReservation;

use Modules\Operation\Http\Requests\ReseveForClientRequest;
use Modules\Operation\Http\Requests\AddDocumentsRequest;

use Modules\Operation\Entities\Documentation;
use Illuminate\Support\Facades\Storage;
use Modules\Operation\Entities\CarName;
use Modules\Operation\Entities\CarType;
use DB;
use Modules\Operation\Http\Requests\AddPassportNumIntoReadyTravelingRequest;
use Modules\Operation\Http\Requests\AddPaymentRequest;
use Modules\Operation\Http\Requests\AddCarToReservationRequest;
use Modules\Operation\Http\Requests\AddSecurityCodeReservationRequest;
use Modules\Operation\Http\Requests\AddSecurityCodePaymentRequest;
use Modules\Operation\Entities\Color;

class ReservationController extends Controller
{
            public function __construct()
    {
         
         //for user(users that can open the app)=>traveler , driver
        //traveler
         $this->middleware(['permission:getAllReservationsRecords_read'])->only('getAllReservationsRecord');         
         $this->middleware(['permission:cancelReservation_cancel'])->only('cancelReservation');      
         $this->middleware(['permission:getAllReservationsUser_read'])->only('getAllReservationsUser');         
         $this->middleware(['permission:addDocuments_store'])->only('addDocuments');         
         $this->middleware(['permission:getDocumentations_read'])->only('getDocumentations'); 
         $this->middleware(['permission:typesReservations_read'])->only('typesReservations'); 
         $this->middleware(['permission:typesReservations_read'])->only('typesReservations'); 
         $this->middleware(['permission:addTypeReservation_store'])->only('addTypeReservation'); 
         $this->middleware(['permission:getPointsCountTraveler_read'])->only('getPointsCountTraveler'); 
         $this->middleware(['permission:getTypesCar_read'])->only('getTypesCar'); 
         $this->middleware(['permission:getDataCarsViaType_read'])->only('getDataCarsViaType'); 
         $this->middleware(['permission:addCarToReservation_store'])->only('addCarToReservation'); 
         $this->middleware(['permission:getDataCar_read'])->only('getDataCar'); 
         $this->middleware(['permission:addReservation_store'])->only('addReservation'); 
         $this->middleware(['permission:getDataCar_read'])->only('getDataCar'); 
         $this->middleware(['permission:myReservations_read'])->only('myReservations'); 
         $this->middleware(['permission:showReservation_show'])->only('showReservation'); 
         //driver
         $this->middleware(['permission:getAllConfirmedReservations_read'])->only('getAllConfirmedReservations'); 
         $this->middleware(['permission:cancelConfirmReservation_update'])->only('cancelConfirmReservation'); 
         $this->middleware(['permission:createJurney_store'])->only('createJurney'); 
         $this->middleware(['permission:openJurney_read'])->only('openJurney'); 
         $this->middleware(['permission:addPassportNumIntoReadyTraveling_store'])->only('addPassportNumIntoReadyTraveling'); 
         $this->middleware(['permission:startJurney_update'])->only('startJurney'); 
         $this->middleware(['permission:finishingJurney_update'])->only('finishingJurney'); 
         $this->middleware(['permission:paymentsTravelers_read'])->only('paymentsTravelers'); 
         
         

         
    }

    public function storeUserHiddenWallet($pointsCountReservation,$operation_num,$userId){
        $UserHiddenWallet= new UserHiddenWallet();
        $UserHiddenWallet->reservation_points_count='AR_'.$pointsCountReservation;
        $UserHiddenWallet->operation_num=$operation_num;
        $UserHiddenWallet->user_id=$userId;
        $UserHiddenWallet->save();
    }
    public function updateReservation($reservation,$operation_num,$type,$status,$points_count,$carId=null){
       // $type=1->vip, $type=2->comfortable , $type->seating
       //status=1->reservation pending , status=3->reservation completed
        $reservation->operation_num=$operation_num;
        $reservation->type=$type;
        $reservation->status=$status;
        $car = CarName::where(['id'=>$data['car_id']])->first();
        $pointsCountVip=$car->price;
        $reservation->points_count=$points_count;
        $reservation->save();
    }
        public function storeReservation($newReservation,$userResrvationId,$clientId,$type,$travel,$status,$points_count,$carId=null){
            //ضمنت اول م اعمل حجز مين م كان يعمل حينحجز عند سواق معين  عشان لما اعمل اضافة وتاكيد الحجز انا كسواق وهو المسافر اصلا مش موجود لالي فما بزبط 
            //get all drivers
            $operation_num=mt_rand(100000, 999999);
            // $type=1->vip, $type=2->comfortable , $type=3->seating
            //status=1->reservation pending , status=3 ->reservation completed
            $newReservation->user_id=$userResrvationId;
            $newReservation->client_id=$clientId;
            $newReservation->operation_num=$operation_num;
            $newReservation->type=$type;
            $newReservation->type=$travel;
            $newReservation->car_id=$carId;
            $newReservation->status=$status;
            $newReservation->points_count=$points_count;
            return $newReservation;
    }
    
        //All cases for this fun : 1. from not allowed table only 2. from not allow table and  original wallet 3. from original wallet only
        public function operationsUserInsideWallet ($reservationToUser){//decreaseFromOriginalWalletAndStoreInHiddenWallet
            //decrease from original wallet and store points_count in hidden wallet user
            //decrease from original wallet (col. : allowed_points_count or table PointsNotAllowedUse)
            //1.decrease from  table PointsNotAllowedUse
            $pointsUserFromNotAllowedRedeem= PointsNotAllowedUse::where(['user_id'=>$reservationToUser->user_id])->first();
                if(!empty($pointsUserFromNotAllowedRedeem)){//check if this user have point in not allow table , if not -> will work on original wallet this user oonlyy
                 $pointsUserFromNotAllowedRedeemNum = explode('_',$pointsUserFromNotAllowedRedeem->points_count, 2)[1];
                 if($pointsUserFromNotAllowedRedeemNum!==0){
                    if($pointsUserFromNotAllowedRedeemNum>=$reservationToUser->points_count){//if his points in table not allowed , exist count points >= points count to reservation , will take all points required from this table
                        $total_pointsUserInNotAllowed= $pointsUserFromNotAllowedRedeemNum-$reservationToUser->points_count;
                        $latestWallet=SystemWallet::latest()->first();
                        if($total_pointsUserInNotAllowed!==0){//after take points from table not allowed will decrease , so when dcrease -> check points count in table not allowed , if reach to  0 , will delete it and work on original wallet oonlyy
                            $pointsUserFromNotAllowedRedeem->points_count=$latestWallet->wallet_num.'_'.$total_pointsUserInNotAllowed;
                              $pointsUserFromNotAllowedRedeem->save();
                              //2. store points_count in hidden wallet user
                                $this->storeUserHiddenWallet($reservationToUser->points_count,$reservationToUser->operation_num,$reservationToUser->user_id);
                                return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>'reservation for a user has been succefully , via take points for reservation from table not allowed points',
                                    'data'=>$reservationToUser
                                ]);
                        }else{// if reach to  0 , will delete it (we finished from reservation , because points reservation = point in not allwed table  , will poknits in this table =0 so will delete it (we dont need any points for reservation now , finished from it ->took all my points from table not allowed )
                        // and work on original wallet oonlyy, but points count for reservation not from original , because we take some points from not allowed table , 
                        //will calculate how much now we need points for reservation
                        //ex: points count for reservation =30 , in not allowed table for this client 10 just , so will take this 10 and delete this row , remain points to reservation = 20 will take it from original wallet
                            $pointsUserFromNotAllowedRedeem->delete();
                            $operation_num = $reservationToUser->operation_num; 
                            //$UserHiddenWallet= new UserHiddenWallet();
                            $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                           
                            return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>'derease these from wallet and delete row this user from not allow because now',
                                    'data'=>$reservationToUser
                                ]);
    
                        }
                    }else{//points count in table not allow <$reservationToUser->points_count , will sum it on points in original to know is enough or not
                        $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->user_id])->first();
                        $pointsUserFromNotAllowedRedeemNum = explode('_',$pointsUserFromNotAllowedRedeem->points_count, 2)[1];
                        $pointsUserFromOriginalWalletNum = explode('_',$UserOriginalWallet->allowed_points_count, 2)[1];
                        $totalPointsInWalletAndNotAllowed= $pointsUserFromOriginalWalletNum+$pointsUserFromNotAllowedRedeemNum;
                        if($totalPointsInWalletAndNotAllowed>=$reservationToUser->points_count){//check sum of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                            //3. decrease from original wallet (col. : allowed_points_count)
                            $remainedPointsWallet= $reservationToUser->points_count-$pointsUserFromNotAllowedRedeemNum;
                            $UserOriginalWallet->allowed_points_count=$UserOriginalWallet->wallet_num.'_'.($pointsUserFromOriginalWalletNum-$remainedPointsWallet);
                            $UserOriginalWallet->save();
                            $pointsUserFromNotAllowedRedeem->delete();
                            $operation_num = $reservationToUser->operation_num; 
                            $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                            return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'$totalPointsInOriginalWalletAndNotAllowed >= $reservationToUser->points_count , so derease these from wallet and delete row this user from not allow because now sure empty pointscount',
                                'data'=>$reservationToUser
                            ]);
                        }else{
                            return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>'this user havent enough points to completing reservation($totalPointsInOriginalWalletAndNotAllowed < $reservationToUser->points_count)',
                                    'data'=>$reservationToUser
                                ]);
                        }
                }
                             
             }else{//$pointsUserFromNotAllowedRedeemNum==0, so this client exist in table pioints not allowed but his points =0 in it , so if be this case , must delete it , and after that will go into original wallet
                 $pointsUserFromNotAllowedRedeem->delete();
                  $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->user_id])->first();
                    if($UserOriginalWallet->allowed_points_count>=$reservationToUser->points_count){//check points in original walet  of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                         // decrease from original wallet (col. : allowed_points_count)
                          $remainedPointsWallet= $UserOriginalWallet->allowed_points_count-$reservationToUser->points_count;
                            $UserOriginalWallet->allowed_points_count=$remainedPointsWallet;
                            $UserOriginalWallet->save();
                            $operation_num = $reservationToUser->operation_num;
                            $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                            //change status reservation this user
                                $reservationToUser->status=3;//reservation operation completed  
                                $reservationToUser->save();
                                return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>'reservation for a user has been succefully , via take points for reservation from wallet original',
                                    'data'=>$reservationToUser
                                ]);
                    }else{
                        return \response()->json([
                            'code'=>400,
                            'status'=>false,
                            'message'=>'this user havent enough points to completing reservation($UserOriginalWallet->allowed_points_count < $reservationToUser->points_count)',
                            'data'=>$reservationToUser
                        ]);
                    }
             }
                }else{
                    $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->user_id])->first();
                    if(!empty($UserOriginalWallet)){//else: this user haveent original user , this ما بزبط لازم يكون عندو فبهندلها
                        $allowed_points_countNum = explode('_',$UserOriginalWallet->allowed_points_count, 2)[1];
                        if($allowed_points_countNum>=$reservationToUser->points_count){//check points in original walet  of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                        // decrease from original wallet (col. : allowed_points_count)
                         $remainedPointsWallet= $allowed_points_countNum-$reservationToUser->points_count;
                        $UserOriginalWallet->allowed_points_count=$UserOriginalWallet->wallet_num.'_'.($remainedPointsWallet);
                        $UserOriginalWallet->save();
                        $operation_num = $reservationToUser->operation_num; 
                        $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                        return \response()->json([
                            'code'=>200,
                            'status'=>true,
                            'message'=>'$UserOriginalWallet->allowed_points_count>=$reservationToUser->points_count',
                            'data'=>$reservationToUser
                        ]);
                    }else{
                        return \response()->json([
                            'code'=>400,
                            'status'=>false,
                            'message'=>'this reservation has been rejected , because the user havent enough money for this rervation',
                            'data'=>$reservationToUser
                        ]);
                    }
                      }else{
                        $UserOriginalWallet= new UserOriginalWallet();
                        $UserOriginalWallet->allowed_points_count=0;
                        $UserOriginalWallet->user_id=$reservationToUser->client_id;
                        $UserOriginalWallet->save();
                        $operation_num = $reservationToUser->operation_num; 
                        $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                        return \response()->json([
                            'code'=>200,
                            'status'=>true,
                            'message'=>'this user haveent original user, so we created a wallet quickly , to be exist in next operations',
                            'data'=>$UserOriginalWallet
                        ]);
                      }
                }
                return true;
        }
    public function reseveForClientUser(ReseveForClientRequest $request,$pointsCountReservationStorageTotal){
        //req-> client_id , type , travel
        $data=$request->validated();
        $userId=auth()->user()->id;
        $messages=[];
        $dataMsgs=[];
        $msgNotFound=null;
        //لوب عكل المسافرين اللي ضفتهم للحجز بصفحة الحجز عشان هنا لما اضغط احجز الان حيعمل لوب عليهم وكل واحد فيهم بزبط ولا لا ولو واحد ما بزب احكيلو  باسمو انو تم الحجز له من قبل 
        foreach(json_decode($data['clients']) as $clientPassport){
            //check if client passport if exist in table reservation  
            $client=User::where('passport_num',$clientPassport)->first();
            if($client==null){
                $msgNotFound='passport_num not exist :'.$clientPassport;
                array_push($messages,$msgNotFound);
            }else{
                $reservation=  Reservation::where(['client_id'=>$client->id])->first();
                //يعني هادا الكلينت محجوزلو قبل هيك هيك بالتالي بزبط يعمل لكن لو كان لاول مرة لازم يدخل كود الامان وبعد م يدخله نتاكد انو كود الامان تمام لهادا اليوزر بالاصل
               //حجز لاول مرة 
                if(empty($reservation)){
                    //لازم احنا نطلب منه يدخل الكود بس في هاي الحالة انو فعليا مش محجوزلو قبل هيك 
                    // $data=[
                    //     'passport_num'=>$clientPassport
                    // ];
                    //لو في شخص من اللي بحجزلهم مش حاطط كود الامان بكتبلو في شخص جوازو كدا لم يتم الحجز له بسبب انه لا يضع كود الامان فعليه وضع كود الامان 
                  //اي يجب ان يدخل كود الامان اي يدهب الى الصفحة التي يدخل كود الامان فيها 
                  //هاتلي كل كلينت شوفلي الو كود امان ولا لا 
    //                     if($client->security_code==null){
    // //بهاي الحالة اول م تظهر هاي الراسلة من ضمن الرسائل اللي بتطلع بالريسبونس تاعة الحجز لاكتر من شخص ع طول بظهرلو واجهة ادخل كود الامان لجواز السفر كدا كدا عشان يكتبه ويتخزن لهادا الكلينت 
    //                     array_push($messages,'cannt reserve for this passport_num :'.$client->passport_num.' '.',because not put security code and this client reserve at once ');
    //                     array_push($dataMsgs,$client->passport_num);
    
    //                     }else{
                            ///بعد م فحصنا تمام المفروض لما ييجي هنا وهو مش محجوز من قبل والو كود اول مرة حتيجي هاي الرسالة لكن بعد ههيك حنحكيلو تمام بنفع الحجز لانو تم فحصه 
                                //مش حييجي هنا الا لو كان بنحجز الو لاول مرة 
                            // if($client->checked==1){
                                //خلص هنا تمام بحجز لالول مرة وتم الفحص له بالتالي عادي هلا يكمل الحجز 
                                
                                
                                
                                                                        $pointsCountReservation=    Storage::get('pointsCountReservation');
                                //check in wallet this user  to compare points in his wallet with points require for the type reservation
                                $PointsNotAllowedUse=    PointsNotAllowedUse::where(['user_id'=>$userId])->first();
                                $UserOriginalWallet=  UserOriginalWallet::where(['user_id'=>$userId])->first();
                                if(!empty($PointsNotAllowedUse)&&!empty($UserOriginalWallet)){
                                    $pointsUserFromNotAllowedNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                                    $pointsUserAllowed = explode('_',$UserOriginalWallet->allowed_points_count, 2)[1];
                                    $totalPointsCountUser= $pointsUserFromNotAllowedNum +  $pointsUserAllowed;
                                }else{
                                    $pointsUserAllowed = explode('_',$UserOriginalWallet->allowed_points_count, 2)[1];
                                    $totalPointsCountUser= $pointsUserAllowed;
                                }
                                $typeReservation=  Storage::get('typeReservation');
                                    //store reservation
                                if($typeReservation==3){
                                    if($totalPointsCountUser>=$pointsCountReservationStorageTotal){//حنقارن نقاطي اللي بمحفظتي مع كل مجموع النقاط المحتاج احجز فيهم هنا حاجز لاكتر من واحد بنفس التايب غير عن هداك واحد 
                                  //احنا هينا بالفور بنشوف الحجز تاع الاول بزبط نقاطو ولا لا لو بزبط تمام انحجزلو والتاني ما كفت نقاطي لاحجزلو بوقفو وبحكيلو ما كفت وما انحجز 
                                  //او من الاول بشوف كل النقاط اللي معي بتكفي بكل الحجوزات هيك احسن عشان ما احجز لواحد والتانين لا  بس الناس اللي بتكون محجوزلها من قبل عدي بكمللها الحجز لانو مش محتاجة بتكون مني شي غير يتغير الستستس تبعها وما بتحسب مني ايشي 
                                        //if client not reserv in prevoius time 
                                    $newReservation= new   Reservation();
                                    $type=3;
                                    $status=3;
                                    $points_count=$pointsCountReservation;//الحجز الواجد نقاطو مش النقاط اللكلية اللازمة لكل الحجوزات لا بس بنقارن فيها النقاط الكلية اما لما نجحجز بنحجز لواحد واحد 
                            
                                    $reservation=$this->storeReservation($newReservation,$userId,$client->id,$type,$data['travel'],$status,$points_count);
                                    if(!is_object($reservation)){
                                        array_push($messages,'cannt store reservation for this passport_num ,because'.$reservation);
                                            array_push($dataMsgs,$client->passport_num);

                                    }else{
                                        
                                        //call method decrease from original wallet , not allow table ad store in hidden wallet
                                        $this->operationsUserInsideWallet($reservation);
                                        $msgSuccess='reservation has been completed for passport_num:'.$reservation->client->passport_num;
                                        array_push($messages,$msgSuccess);
                                    }
                                    }else{
                                        array_push($messages,'rejected this reservation , because your type vip for this taraveler passport_num:');
                                                                array_push($dataMsgs,$client->passport_num);

                                    } 
                                    }elseif($typeReservation==2){
                                    if($totalPointsCountUser>=$pointsCountReservationStorageTotal){
                            
                                    //لو مش حاجز اصلا فانا ححجزلو عادي ححط اليورز ايدي انا والكلينت ايدي ايدي الشخص اللي بدي احجزلو وعدد المنقاط حسب النوع والستيتس 2 ع طول هنا و  ورقم العملية
                                    $newReservation= new   Reservation();
                                    $clientId=$client->id;
                                    $type=2;
                                    $status=3;
                                    $points_count=$pointsCountReservation;
                                    $reservation= $this->storeReservation($newReservation,$userId,$client->id,$type,$data['travel'],$status,$points_count);
                                    if(!is_object($reservation)){
                                        array_push($messages,'cannt store reservation for this passport_num');
                                            array_push($dataMsgs,$client->passport_num);

                                    }else{
                    
                                        //call method decrease from original wallet , not allow table ad store in hidden wallet
                                            $this->operationsUserInsideWallet($reservation);
                                        //return $reservation;
                                        $msgSuccess='reservation has been completed for passport_num:';
                                        array_push($messages,$msgSuccess);
                                                                array_push($dataMsgs,$reservation->client->passport_num);

                                    }
                                        }else{
                                            array_push($messages,'rejected this reservation , because your type comfotable for this taraveler passport_num');
                                
                                                           array_push($dataMsgs,$client->passport_num);

                                
                                         }   
                                    }elseif($typeReservation==1){
                                        //هنا بغض النظر مين السواق هنا بختار السيارة نوغعها 
                                    if($totalPointsCountUser>=$pointsCountReservationStorageTotal){
                                    //لو مش حاجز اصلا فانا ححجزلو عادي ححط اليورز ايدي انا والكلينت ايدي ايدي الشخص اللي بدي احجزلو وعدد المنقاط حسب النوع والستيتس 2 ع طول هنا و  ورقم العملية
                                    $newReservation= new   Reservation();
                                    $clientId=$client->id;
                                    $type=1;
                                    $status=3;
                               $car = CarName::where(['id'=>Storage::get('car_id')])->first();
                                    $pointsCountVip=$car->price;
                                    $reservation=  $this->storeReservation($newReservation,$userId,$client->id,$type,$data['travel'],$status,$pointsCountVip,$car->id);
                                    if(!is_object($reservation)){
                                        array_push($messages,'cannt store reservation for this passport_num ');
                                            array_push($dataMsgs,$client->passport_num);

                                    }else{
                    
                                        //call method decrease from original wallet , not allow table ad store in hidden wallet
                                        $this->operationsUserInsideWallet($reservation);
                                        $msgSuccess='reservation has been completed for passport_num:';
                                      array_push($messages,$msgSuccess);
                                                              array_push($dataMsgs,$reservation->client->passport_num);

                                    }
                            
                                    }
              
                                    }
                                
                            // }else{
                            //     //اي بنحجز لالو لاول مرة ولسا لم يتم فحصه 
                            
                            // //لو الو كود الامان بس طبعا بحنحجز لالو لاول مرة حظهرلو بالرسائل ادخل كود الامان بردو لالنه يحجز لالول مرة 
                            //                     array_push($messages,'cannt reserve for this passport_num :,because  this client reserve at once ');
                            //                             array_push($dataMsgs,$client->passport_num);

                            // }
                            
                            
                        // }
              
                    
                }else{//this client has been reserved in previous time
                    
                    //لازم نخليه يحط كود الامان لو كان بحجز لاول مرة او مش مدخل بالاصل  : م هو نفس الشي طالما بحجز لاول مرة سواء مدخخل من قبل ولا لا لازم يحطه ولو كان بحجز لالول مرة او مش لازل مرة وهو مش مدخخل من قبل كود الامان فاخميلو دخله 
                    //
                         //لازم نوفقه هنا لو الكلينت الكود تاه مش موجود عشان باعتين رسالة انو هادا الكملينت بدنا يكتب كود ولسا ما كتب لنعرف الحجز تاعه 
               //ما يدخل هنا الا لو كان في كود لالو 
                      //بدنا اياه يعمل حجز بس في حالة حاجز قبل هيك بالنظام وبس 
                      //ونمنعه من الحجز انما نخليه ينجبر يكتب الكود في حالة كان بحجز لاول مرة او حاجز من قبل وكود الامان مش موجود 
                      //لا المفروض كود الامان ينكتب من اول حجز 
                        //هلا بالحالتين حنغحص لو مش محجوز مش قبل حندخل بالفحص فالحالة الاولى لو الو كود حنقارنه بيه ولو مش الو حنكرتله 
                        
                        //هاي الحالة بحجزش لالول مرة وبنفحص كود الامان موجود من قبل 
                            //  if($client->security_code!==null){
                                if($reservation->original_status==1){//this reservation still pending , from delagete , will the user complete this reservation for this client
                                //update reservation
                                $reservation->status=3;
                                $reservation->user_id=$userId;
                                $reservation->client_id=$client->id;
                                $reservation->save();

                                $msgSuccess='reservation been pending  , but now became completed for passport_num:';
                                    array_push($messages,$msgSuccess);
                                                            array_push($dataMsgs,$reservation->client->passport_num);

                                }
                                elseif($reservation->original_status==2){
                                    array_push($messages,'this reservation still pending(allowed) in delegate for this taraveler passport_num:');
                                                            array_push($dataMsgs,$client->passport_num);

                                }elseif($reservation->original_status==3){
                                    array_push($messages,'this reservation  now is using, so cannt make reservation again  for this taraveler passport_num:');
                                                            array_push($dataMsgs,$client->passport_num);

                                }
                                 
                            //  }else{
                            //      //بحجزش لاول مرة لكن ما الو كود الامان 
                            //                              array_push($messages,'cannt reserve for this passport_num :,because this client not reserve at once but this client not fount   security code for his  ');
                            //     array_push($dataMsgs,$client->passport_num);

                                 
                            //  }
                       
                        }
                }
        }
        return $messages;
                return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>$messages,
                                    'data'=>$dataMsgs
                                ]);
        
    }

///////////////////////////////////////////////////////////////////////////

//get this reservation in page reservation for this user
    public function getAllReservationsUser(){
        //client : The person who was reserved
        //user : The person who made the reservation
        $userId=auth()->user()->id;
       $userReservedForhimself= Reservation::where(['user_id'=>null,'status'=>2,'client_id'=>$userId])->first();
       if(!empty($userReservedForhimself)){
            $userReservedForhimself->load('client');
       }
        $clientGen=null;
       $userGen=null;
       $userReservedAnotherPerson= Reservation::where(['user_id'=>$userId,'status'=>2])->where('client_id','!=',null)->get();
          if(!empty($userReservedAnotherPerson)){   
           foreach($userReservedAnotherPerson as $userReserved){
               $clientGen=$userReserved->load('client');
           }
       }
       
          $clientReservedForYou= Reservation::where(['status'=>2,'client_id'=>$userId])->where('user_id','!=',null)->first();
                if(!empty($clientReservedForYou)){
                 $userGen=$clientReservedForYou->load('user');

                }
                if(empty($clientGen)&&empty($userGen)){
                    $message=[
                        "there is not found any reservations for you"
                        
                        ];
                     $data=[
                        null
                    ];
                }elseif(empty($clientGen)){
                    $message=[
                        "reservation has been successfully for you via the delegate, exporter ticket'.'type:'.$userReservedForhimself->type",
                    "person :'.$userGen->name.'reserved a ticket for you, exporter ticket, type:'.$clientReservedForYou->type"
                        ];
                $data=[
                    $userReservedForhimself,
                    $clientReservedForYou
                    ];
                }elseif(empty($userGen)){
                    if(empty($userReservedForhimself)){
                    $message=[
                        "you reserved an importer ticket,type:'.$userReserved->type"
                        ];
                      $data=[
                    $userReserved
                    ];
                        
                    }else{
                                        $message=[
                        "reservation has been successfully for you via the delegate, exporter ticket'.'type:'.$userReservedForhimself->type",
                        "you reserved an importer ticket,type:'.$userReserved->type"
                        ];
                      $data=[
                    $userReservedForhimself,
                    $userReserved
                    ];
                    }
                }else{
                    if(empty($userReservedForhimself)){
                    $message=[
                    "you reserved a ticket an importer ticket,type:'.$userReserved->type",
                    "person :'.$userGen->name.'reserved a ticket for you, exporter ticket, type:'.$clientReservedForYou->type" 
                        ];
                      $data=[
                    $userReserved,
                    $clientReservedForYou
                    ];
                        
                    }else{
                        $message=[
                                       "reservation has been successfully for you via the delegate, exporter ticket'.'type:'.$userReservedForhimself->type",
                    "you reserved a ticket an importer ticket,type:'.$userReserved->type",
                    "person :'.$userGen->name.'reserved a ticket for you, exporter ticket, type:'.$clientReservedForYou->type" 
                        ];
                      $data=[
                    $userReservedForhimself,
                    $userReserved,
                    $clientReservedForYou
                    ];
                    }
                }
                    
                    
                        return \response()->json([
                                    'code'=>200,
                                    'status'=>true,
                                    'message'=>$message, 
                                    'data'=>$data
                                ]);
                
                
        
    }

    
    public function addDocuments(AddDocumentsRequest $request){
        //req-> passport_num , birth_date
        //فحص هل الجواز الرقم موجود بجدول اليوزرز وهل تاريخ الميلاد يطابق هادا الجدواز بعد ذلك يتم اضافة هادا الوثيقة بجدول الوثائق اللي فيها يوزر ايدي  بنكريت يعني صف بنحط فيه اايوزر ايدي تاع الاوث ورقم الجواز والتاريخ 
        //او بدل م نحط رقم الجواز والتاريخ هنا نحط عمود كلينت ايدي  لا خلص عشان الشربكة
        $data=$request->validated();
        $userId=auth()->user()->id;
        $passportClient=User::where(['passport_num'=>$data['passport_num']])->first();
        if(!empty($passportClient)){
         $clientDocumentation=   Documentation::where(['client_id'=>$passportClient->id,'user_id'=>$userId])->first();
            if(!empty($clientDocumentation)){
                 return \response()->json([
                                'code'=>400,
                                'status'=>false,
                                'message'=>'this client already  added into your documentation',
                                'data'=>null
                            ]);
            }
            if($passportClient->birth_date==$data['birth_date']){
            $documentation = new   Documentation();
            $documentation->user_id=$userId;
            $documentation->client_id=$passportClient->id ;
           // $documentation->passport_num=$data['passport_num'];
            //$documentation->birth_date=$data['birth_date'];
            $documentation->save();
                                                                     return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'document has been added',
                                'data'=>$documentation->load(['user','client'])
                            ]);
            }else{
                                                                                     return \response()->json([
                                'code'=>406,
                                'status'=>false,
                                'message'=>'birth_date not suite with passport num',
                                'data'=>null
                            ]);
            }
        }else{
                                                                                 return \response()->json([
                                'code'=>407,
                                'status'=>false,
                                'message'=>'this passport num not exist in system',
                                'data'=>null
                            ]);
        }
    }
    
    public function getDocumentations(){
                $userId=auth()->user()->id;
   $documentationsUser= Documentation::where(['user_id'=>$userId])->with('client')->get();
                    $user=User::where(['id'=>$userId])->first();
                    $data=[
                        'user'=>$user->passport_num,
                        'documentationsUser'=>$documentationsUser
                    ];                                
                    return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'getting data successfully',
                                'data'=>$data
                            ]);
    }

    public function getAllReservationsRecords(Request $request)
    {
     $reservationsRecord  = Reservation::paginate($request->total);

    //بنعمل اظهار لرقم العملية والمبلغ والتاريخ ورقم المعرف اللي هو البيرسنل ايدي تاع الشخص اللي انحجزلو تدكرة يعني الكلينت ايدي 

                                                                                       return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'getting data successfully',
                                'data'=>$reservationsRecord
                            ]);
    }

    public function cancelReservation($id){
      $reservation=  Reservation::where(['id'=>$id])->first();
      if(!empty($reservation)){
          //points count return into user
            $userId=$reservation->user_id;
            $points_count=$reservation->points_count;

          $PointsNotAllowedUse=  PointsNotAllowedUse::where(['user_id'=>$userId])->first();
        $PointsNotAllowedUseNum=  explode('_',$PointsNotAllowedUse->points_count, 2)[1];
          $PointsNotAllowedUse->points_count = $PointsNotAllowedUseNum+$points_count;
          $PointsNotAllowedUse->save();
          //delete this reservation
          $reservation->delete();
                                                                               return \response()->json([
                                'code'=>201,
                                'status'=>true,
                                'message'=>'this ticket rervation has been deleted',
                                'data'=>$reservation
                            ]);
      }else{
                                                                                         return \response()->json([
                                'code'=>404,
                                'status'=>false,
                                'message'=>'this id reservation not found',
                                'data'=>null
                            ]);
      }
    }   
    public function cancelAllReservations($user_id){
        $reservations=  Reservation::where(['user_id'=>$user_id])->get();
        if($reservations->count()!==0){
            $totalPointsCountReservations=0;
            foreach($reservations as $reservation){
                $totalPointsCountReservations=$totalPointsCountReservations+$reservation->points_count;
                            //delete this reservation
            $reservation->delete();
            }
            //points count return into user
              $points_count=$totalPointsCountReservations;
  
            $PointsNotAllowedUse=  PointsNotAllowedUse::where(['user_id'=>$user_id])->first();
          $PointsNotAllowedUseNum=  explode('_',$PointsNotAllowedUse->points_count, 2)[1];
            $PointsNotAllowedUse->points_count = $PointsNotAllowedUseNum+$points_count;
            $PointsNotAllowedUse->save();
            $data=[
                'totalPointsCountReservations'=>$totalPointsCountReservations
            ];

                                                                                 return \response()->json([
                                  'code'=>201,
                                  'status'=>true,
                                  'message'=>'this ticket rervation has been deleted',
                                  'data'=>$data
                              ]);
        }else{
                                                                                           return \response()->json([
                                  'code'=>404,
                                  'status'=>false,
                                  'message'=>'this user havent any  reservations for any client',
                                  'data'=>null
                              ]);
        }
      }   
// get types reservations
    public function typesReservations(){
      $typesReservationsCount=  TypeReservation::count();
      if($typesReservationsCount!==0){
          $typesReservations=  TypeReservation::get();
          return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$typesReservations
        ]);
      }else{
        return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'data in table type reservation is empty',
            'data'=>null
        ]);
      }
    }
    
    //when the traveler : select type reservation in page traveler
    public function addTypeReservation(Request $request){
        $data=$request->all();//type
        
        Storage::put('typeReservation',$data['type']);
               $typeReservation=TypeReservation::where('type',$data['type'])->first();
             Storage::put('pointsCountReservation',$typeReservation->points_count);
             $pointsCountReservationStorage=Storage::get('pointsCountReservation');

        $data=[
            'type'=>$typeReservation->type,
            'pointsCountReservationStorage'=>$pointsCountReservationStorage
        ];
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>' type reservation  has been added successfully',
            'data'=>$data
        ]);
    }

    public function getPointsCountTraveler(){
       $typeReservation= Storage::get('typeReservation');
       if($typeReservation==1){
        return \response()->json([
            'code'=>400,
            'status'=>false,
            'message'=>'cannt get reservation points count because you select type reservation : vip , which ithis type its points unknown , depend on selecting type a car',
            'data'=>$typeReservation
        ]);
       }
       $typeReservation=TypeReservation::where('type',$typeReservation)->first();
       $pointsCountReservation=0;
       $pointsCountReservationStorageTotal=0;
       if(!empty($typeReservation)){
        //   $pointsCountReservation=   $typeReservation->points_count;
        //   Storage::put('pointsCountReservation',$pointsCountReservation);
           $pointsCountReservationStorage=  Storage::get('pointsCountReservation');
        $pointsCountReservationStorageTotal=$pointsCountReservationStorageTotal+$pointsCountReservationStorage;
        Storage::put('pointsCountReservationStorageTotal',$pointsCountReservationStorageTotal);

       }
       $data=[
        'points Count this type Reservation'=>$pointsCountReservationStorage . 'point',//عدد النقاط للحجز هادا اللي اخترته هلا بتحطيه تحت كل مسافر بحجزلو 
        'total points count reservation'=>$pointsCountReservationStorageTotal//عدد نقاط تاع الحجوزات اللي حجزتهم كلهم هلا  بتحطيه عند كلمة نقاط محجوزة 
       ];
       return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'points Count Reservation',
        'data'=>$data
    ]);
    }


     //هلا لو الحجز كان ايشي غير ال في اي بي : خلص عادي الضغط ع زر ااضافة الحجز اما لو كان في ايبي 
     //ححكيلها لازم تعملي زر اضافة حجز بلون باهت عشان ما يقدر يضغط عليه وييجي زر بالجنب زر تخصيص عشان لما يضغط عليه يهرلو اواع السيارات اللي لما يضغط على اي نوع فيهم بظهرلو السيارات تاعون هالنوع ولما يضغط ع سيارة معينة  حيضغط ع زر تاكيد اللي هو بدخلله بالريكوسيت تاعو ايدي الكار اللي ضغط عليها 
     // اللي هي انا مخزناها بالسشن عندي تمام واول م تلاقي ريسبونس تمام من هادا الراوت تاع الاختيار التاكيد اي تم اختيار سيارة بهاي الحالة  حيصير اضافة حجز هادا الزر متفعل عشان يضغط عليه ويزبط الحجز باستخدام الكار هاي اي الفي ايبي 
     
         public function getTypesCar(){
            $typesCar= CarType::get();//get all types cars
        
            return \response()->json([
             'code'=>200,
             'status'=>true,
             'message'=>'data has been getten successfully',
             'data'=>$typesCar
         ]);
 
     }
         public function getDataCarsViaType($typeId){
        $CarsThisType= CarName::where(['car_type_id'=>$typeId])->with('carType')->get();//get all cars have this type(id this type)
        
        return \response()->json([
         'code'=>200,
         'status'=>true,
         'message'=>'Cars This Type',
         'data'=>$CarsThisType
     ]);
 
     }
         public function selectColorCar($carId,$colorId){
            $color= Color::where(['id'=>$colorId])->first();
                    Storage::put('color_id',$colorId);
                  $data=[
                      'color_car_user'=>$color->name
                      ];
            return \response()->json([
             'code'=>200,
             'status'=>true,
             'message'=>'added CarToReservation',
             'data'=>$data
         ]);
    }
     public function addCarToReservation(AddCarToReservationRequest $request){
         $data =$request->validated();//req-> car_id
         //حخزن اللي اختارها بالسشن رقم السيارة من خلالها بقدر اعرض السيارة اللي اختارها ولسا ما انحجزت ولما ااجي احجز بجيب من السشن ولو اختار تاني بتمسح اللي اختارها الاول وبحط الجديد
         //بهيك بااخد من السشن اخر اختيار اختارو للسيارة وهو اللي بحطه بالجدول تاع الحجز 
         Storage::put('car_id',$data['car_id']);
        $carId= Storage::get('car_id');
        $car=CarName::where(['id'=>$carId])->with('carType')->first();
        $data=[
            'car'=>$car
        ];
        return \response()->json([
             'code'=>200,
             'status'=>true,
             'message'=>'added CarToReservation',
             'data'=>$data
         ]);
         
     }
     
         public function getDataCarUser(){
            $color_id=  Storage::get('color_id');
            $color= Color::where(['id'=>$color_id])->first();
             $idCar=Storage::get('car_id');

            $car= CarName::where(['id'=>$idCar])->first();
            $data=[
                'car'=>$car,
                'color'=>$color->name
                ];
            return \response()->json([
             'code'=>200,
             'status'=>true,
             'message'=>'Cars This Type',
             'data'=>$data
         ]);
 
     }
     public function getColorsCar($carId){
        $car= CarName::where(['id'=>$carId])->first();
        
          return \response()->json([
             'code'=>200,
             'status'=>true,
             'message'=>'colors for car',
             'data'=>$car->colors
         ]);
     }
    public function addReservation(ReseveForClientRequest $request){
        $data=$request->validated();
        $userId=auth()->user()->id;
       $pointsCountReservationStorageTotal= Storage::get('pointsCountReservationStorageTotal');
       $typeReservation=Storage::get('typeReservation');

        $operation_num = mt_rand(1000000000, 9999999999); 
        $car_id=Storage::get('car_id');
        if($typeReservation!=="1"&&!empty($car_id)){//if reserv not vip and select a car -> cannt because selecting car only for type vip
            return \response()->json([
                'code'=>400,
                'status'=>false,
                'message'=>'reserved not vip and select a car -> cannt because selecting car only for type vip'
            ]);
        }elseif($typeReservation!=="1"){//reserved not vip and not select any car ->can do this
      
           //for decrease from wallet user
          $resultReservation= $this->reseveForClientUser($request,$pointsCountReservationStorageTotal);
          return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>$resultReservation
        ]);

        }elseif($typeReservation=="1"&&empty($car_id)){//reserved  vip and not select any car ->cannt do this

            return \response()->json([
                'code'=>402,
                'status'=>false,
                'message'=>'reserved  vip and not select any car ->cannt do this'
            ]);
        }elseif($typeReservation=="1"&&!empty($car_id)){//reserved  vip and  select any car ->can do this
            //for decrease from wallet user
            $resultReservation= $this->reseveForClientUser($request,$pointsCountReservationStorageTotal);
            return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>$resultReservation,
            ]);
        }

    }


    //for payment
    public function myReservations(Request $request){
        $userId=auth()->user()->id;
       $reservations= Reservation::where('user_id',$userId)->with(['client','car','car.carType'])->paginate($request->total);
       return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'reservations',
        'data'=>$reservations
    ]);
        
    }
    public function showReservation($id){//show reservation t make payment
        $reservation=Reservation::where('id',$id)->first();
        if(empty($reservation)){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'reservation not found',
                'data'=>null
            ]);
        }
            return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'reservation',
                'data'=>$reservation->load(['user','client'])
            ]);
    }

public function createJurney($typeJurney){
    //لازم قبل كلشي نفحص هل في رحلة حالية بالسيارة هاي  
    //و في حالية فلما يضغط ع فتح رحلة حتظهر الرحلة الحالية ولو ما في حتتكريت وحدة جدة واقدر اضيف فيها وهديك بقدر اضيف فيها لكن الفرق بينهم هديك فيها من الاول مسافرين او مكريتة من قبل اما هاي لسا جديد اتكريتت 
   //حنشوف السواق اللي فتح هاي الصفحة الحالي شو هي سيارته 
   $user=User::where(['id'=>auth()->user()->id])->first();
     if(empty($user->car)){
                            return \response()->json([
                        'code'=>409,
                        'status'=>false,
                        'message'=>'this driver maybe havent a car until now',
                        'data'=>null
                    ]);   
                        }
   $carCurrentJurney= ReadyTraveling::where(['car_id'=>$user->car->id,'status'=>1])->get();
    if(!empty($carCurrentJurney)){//في رحلة حاية بالسيارة فبعرضها 
           $data=[
               'currentJurneyCar'=>$carCurrentJurney->load(['client','car'])
               ];
            return \response()->json([
                'code'=>400,
                'status'=>false,
                'message'=>'already exist a current jurney in this car',
                'data'=>$data
            ]);
    }
    //ما في رحلة حالية للسيار 
        $jurney_num=mt_rand(100000, 999999);
        Storage::put('jurney_num',$jurney_num);
        $jurney_num_storage=Storage::get('jurney_num');
        Storage::put('type_jurney',$typeJurney);
        $type_jurney_storage=Storage::get('type_jurney');
        $readyTraveling=new ReadyTraveling();
        $readyTraveling->jurney_num=$jurney_num_storage;
        $readyTraveling->travel=$type_jurney_storage;
        $readyTraveling->save();
    $data=[
        'jurney_num'=>$jurney_num_storage,
        'type_jurney'=>$type_jurney_storage
        ];
    return \response()->json([//هادا اللي حتمسك ممن خلالهرقم الرجلة اللي انتشات وتحط الرقم بزر بدل زر فتح رحلة وتحط الرمز بناء ع نوع الرحلة اللي اخترتها ونوع بردو من خلاله حفعل زر النوع اللي ضغطت عليهكمان
                'code'=>200,
                'status'=>true,
                'message'=>'jurney has been created successfully with type this jurney',
                'data'=>$data
            ]);
}
    public function openJurney($jurneyNum){
        
        $driver=User::where(['id'=>auth()->user()->id])->first();
             if(empty($driver->car)){
                            return \response()->json([
                        'code'=>409,
                        'status'=>false,
                        'message'=>'this driver maybe havent a car until now ',
                        'data'=>null
                    ]);   
                        }
// dd($driver->car);
        //هو ما في الا رحلة وحدة حالية لكل سواث اكيد فهاتلي الرحلة الحالية للسيارة هاي 
       //هاتلي رحلة هادا السواق بما يحتويها اي كل الصفوف اللي فيها رقم الايدي تاع السيارة هاي وبحيث تكون حالية اي جيت لكل اللي فيهم هاي الكار والستيتس مش 3 عشان ما تكون انتهت 

        $currentJurneyCar = ReadyTraveling::where(['jurney_num'=>$jurneyNum])->get();
        if(count($currentJurneyCar)==0){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'this thurney num not found',
//                'message'=>'not found current jurney in your car , so will create it for you ',
                'data'=>null
            ]);  
        }

        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'jurney has been getten successfully with  travelers its',
            'data'=>$currentJurneyCar->load(['car','client'])
        ]);
    }
//for driver
//الرحلة الوحدة بتنعرف من خلال هاتلي من جدول السفر كل الصفوف اللي الهم ايدي سائق معين 
// وايدي الساءق هادا هو اللي بااكد الحجز وهيك  
//هاي الخطوة عشان امنع شمخص يدفع لسيارة مش سيارته اي لسائق مش سائقه اي لرحلة مش رحلته 
//في زر ليحط مع الريسكويست هادا المسافر دهاب ولا عودة 
    public function addPassportNumIntoReadyTraveling(AddPassportNumIntoReadyTravelingRequest $request){
    //هادا لما يدخل بيدخل مع كل مسافر رقم العملية وكل مسافربيدخل بنفس رقم العملية بالرحلة هاي فقبل م يدخل هنا لازم نكون عارفين الرقم الثابت لهاي الرحلة اللي هي كم حمولة السيارة بعدد الحمولة حيحط الركاب هدول ولو وصل لرقم المسافر 4 وحمولة هي 4 وكان بدو يدخل كمان حنحكيلو لا بزبطش لازم بس 4 ما بزبط اكتر 
    //نفس قصة بدء الرحلة بزبطش يبداها وهو اكتر من الرقم هادا ولا اقل وهادا بزبطش وهو وهو اكبر منه اما اقل مسموح لانو قاعد بيدخل فعند بدء الرحلة لما احكيلو بزبطش اقل من الرقم فدخل كمان بييجي هنا بيدخله وهنا بردو في فحص لازم اقل من الرقم
    
    //هو نفس الرقم وبتكريت واحد  تاني لمسافرين تانيين لو اجى يضيف بعد م خلصت الحملة لا خلص ما بتكريت واحد تاني لانو لا حنحكيلو  بزبطش 
    //بزبطش لازم بس هي الحمولة انتظر لانهاء الرحلة الحالية وبعد م تنتهي بزبط هنا فهنا لازم قبل م يدخل نفحص هل في رحلة لهادا السواق لسا ما انتهت 
    //فبنعرف هالشي من خلال  بندور ع هادا السواق كل المسافرين اللي معو الستيتس تاعهم 3 اي كلهم انتهو اما لو 1 او 2 اي في ناس بالانتظار وناس في الرحلة هلا 
           $data=$request->validated();//req: passport_num , type-> go(0) , back(1)

        ///لازم نكون متفقين ع شغلة : السائق الواحد ما بزبط يكون معه الا بعدد الحمولة عدد المسافرين اللي الستيتس تاعهم 1 عشان لما يدخلهم ما بزبط الا يكونو بعدد الحمولة ليصيرو 2 بردو بعدد الحمولة وبردو لما ينهيها اي 3 نا يزيط الا بعدد الحمولة 
        // فالرقم تاع الرحلة حيكون مختلف كل متلا الحمولة 8 عند كل 8 حيكون نفسه ونفس الستيتس 
        
        //هلا ما بزبط كل م يدخل عالراوت يكريت رقم جديد لازم الرقم هادا يتكريت لما يكون مع هادا السائق معه عدد الحمولة فبتكريت واحد تاني عشان الحمولة اللي بعد اللي هو اصلا مش حيقدر يضيف عشان لسا ما انتهت هاي الرحلة ولا بدت اصلا 
        //فبقدر يضيف عاللي بعد بس لما يصيرو كل اللي بالمرحلة اللي فاتت كلهم 3 اي انتهو 
            $jurney_num_storage=Storage::get('jurney_num');

        $client=User::where(['passport_num'=>$data['passport_num']])->first();

       $jurneyTravelerCount=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage,'client_id'=>$client->id])->count();//هاتلي كل المسافرين مع الرحلة اللي متخزنة بالستوريج  واحتياط مع هادا السائق اما لو حكينا
        if($jurneyTravelerCount!==0){
            return \response()->json([
                'code'=>412,
                'status'=>false,
                'message'=>'cannt add this client into this jurney , because this client already exist in this jurney',
                'data'=>null
            ]);
        }
       
       $jurneyCount=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->count();//هاتلي كل المسافرين مع الرحلة اللي متخزنة بالستوريج  واحتياط مع هادا السائق اما لو حكينا
       //ادا رقم الرحلة موجودة وبس كان واحد وكان اليوزر وكل الاعمدة نل اي هي رحلة اتكريتت وفاضيةما انضاف اشي فيها بهي الحالة 
       //حنضسف الربط الجوازات فيها 
       if($jurneyCount==0){
           return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'this jurney not exist in system',
                'data'=>null
            ]);
       }elseif($jurneyCount==1){
           $jurney=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->first();
          if($jurney->car_id==null&&$jurney->client_id==null){
        
                   //اي مكريتة وكلشي بس انو لساما فيها اي حد حنضيف 
                      //اي هنا حيدخل بس مرة وحدة لما يكون لسا مكريتة وما فيها اي حد فمجرد م ينضاف جد بيصير يروح عالالس 
                      $client=  User::where('passport_num',$data['passport_num'])->first();//traveler
                      //بدي اتاكد هل موجود اصلا بالنظام ولا لا 
                      if(empty($client)){
                           return \response()->json([
                        'code'=>413,
                        'status'=>false,
                        'message'=>'this client not exist in system',
                        'data'=>null
                    ]);
                      }
                      //بدي اتاكد هل هادا الجواز اصلا موجود بجدول الحجوزات 
                      $reservation= Reservation::where(['client_id'=>$client->id])->first();
                        if(!$reservation){
        
                                              return \response()->json([
                        'code'=>406,
                        'status'=>false,
                        'message'=>'cannt add this passport into your jurney because this passport not exist in reservations system',
                        'data'=>null
                    ]);
                    }
                      //فحص هل نوع السفر اللي حيضيفه لهادا الجواز اللي حيركب معه نفس نوع السفر اللي بالحجز تاعه ولا لا 
                        if($reservation->travel==$jurney->travel){
                            return \response()->json([
                        'code'=>408,
                        'status'=>false,
                        'message'=>'cannt add this passport into your jurney because this type travel (go , back) not match with your type travel in your reservation',
                        'data'=>null
                    ]);
                        }
        
                   $readyTraveling=new ReadyTraveling();
                   //السواق وهو بربطهم حيربطهم بالسيارة اللي هو فيها هلا ولو صارت مشكلة انو السواق اتغير ساعتها عادي لانو ارتبطو بالسيارة هاي هو السواق هادا اللي ببطهم بسيارته لكن لما تتغير سيارته السواق هادا هدول المسافرين بضلهم مع السيارة اللي ربطهم فيها اللي كانت تاعته بالاول 
                   //المهم القصة تكون بربط المسافرين بالسيارة تاعة السواق مش السواق نفسه عشان لما يتغير عادي يضلهم بالسيارة 
                   $user=User::where(['id'=>auth()->user()->id])->first();
                        if(empty($user->car)){
                             return \response()->json([
                        'code'=>409,
                        'status'=>false,
                        'message'=>'this driver maybe havent a car until now',
                        'data'=>null
                    ]);   
                        }
                        $readyTraveling->car_id=$user->car->id;//car driver //حاربط المسافر بالسيارة اللي انا فيها حاليا ولو انا اتغيرت كسواق بيضل مربوط بالسيارة هاي اللي اجى حد تاني يسوقها 
                        
                        $readyTraveling->client_id=$client->id;
                        // $readyTraveling->jurney_num=$jurney_num;
                        $readyTraveling->jurney_num=$jurney_num_storage;
                        $readyTraveling->operation_num_reservation=$reservation->operation_num;//رقم العملية تاعة الحجز اللي ااجى منه
                    //confirmed  this client
                    $readyTraveling->confirmed=1;
                        $readyTraveling->status=1;//wait to traveling 
                        $readyTraveling->save();
                        //زي م بنعبي بالسفر حنعبي بالسيارة 
                     //بدنا نشوف المركبة اللي بدو يعبي فيها كم حمولتها اللي هي سياراته الحالية ولو حد تاتي اجى يعبي  الحد التاني تلقائي طالما ساقها بالتالي مربوط فيها اي هي سيارته صارت بناخد ايدي الكار تاعة السواق التاني اللي هي بالاصل تاعة الاول اللي سابها وبطل مرتبط فيها وصار هادامرتبط فيها 
                     //فهاتها شوف كم حمولتها للسيارة اللي ارتبط فيها السواق التاني اللي هي نفسها صارت للتاني 
                     //فاليوزر هادا هو عبارة عن السواق التاني اللي اجى جديد وسيارته هي نفس تاعة الاول فالاول راح وسابها المهم السيارة تاعة الشخص المربوط فيها حالبا ولما يتغير بالمستقبل بتكون تاعة اللشخص اللي حيرتبط فيها المهم الركاب بضلهم فيها اما السواق هو اللي بتغير والسيارة نفسها بتضل بركابها فيها 
                     $loading=0;

                                  $car= CarName::where(['id'=>$readyTraveling->car->id])->first();//car
                    if($car->max_loading==$car->current_loading){
                        $loading=1;
                    }
                    $data=[
                        'traveler'=>$readyTraveling->load(['car','client']),
                        'loading'=>$loading
                        ];
                       $car= CarName::where(['id'=>$readyTraveling->car->id])->first();//car
                       $car->current_loading=1;
                       $car->save();
                         return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'passport this traveler has been added successfully',
                        'data'=>$readyTraveling->load(['car','client'])
                    ]);
           }
           
       }else{//يعني في رحلة مكريتة وكلشي ومضيوف لالها اشخاص فهلا بنضيف عادي لكن ممكن لازم نفحص هل خلصت الحمولة تاعة السيارة ولا كيف 
           //هنا حنبعت مع كل ريسبونس ربط تمام ,نبعت معه هل يظهر زر بدء الرحلة ام لا اللي هو بنضل نفحص حمولة السيارة تاعة الرحلة الحالية لودنج 1 اي فل صارت بهيك حيظهر الزر ولو 0 ما بظهر الزر 
           //جيت لاول صف او لاي صف لهاي الرحلة لانو خلص هاي الرحلة اللي انا حاليا بضيف لالها 
//حنحط هالشي بعد م يعمل اضافة لليوزر هادا اللي داخل هنا عشانه 
            $client=  User::where('passport_num',$data['passport_num'])->first();//traveler
              //بدي اتاكد هل موجود اصلا بالنظام ولا لا 
              if(empty($client)){
                   return \response()->json([
                'code'=>413,
                'status'=>false,
                'message'=>'this client not exist in system',
                'data'=>null
            ]);
              }
            //بدي اتاكد هل هادا الجواز اصلا موجود بجدول الحجوزات 
               $reservation= Reservation::where('client_id',$client->id)->first();
                if(!$reservation){

                                      return \response()->json([
                'code'=>406,
                'status'=>false,
                'message'=>'cannt add this passport into your jurney because this passport not exist in reservations system',
                'data'=>null
            ]);
                }
                                                   $type_jurney_storage=Storage::get('type_jurney');//نوع السفر تاع الرحلة لازم نوع الحجز اللي حضيفه هو نفس نوع هادا 
              //فحص هل نوع السفر اللي حيضيفه لهادا الجواز اللي حيركب معه نفس نوع السفر اللي بالحجز تاعه ولا لا 
                if($reservation->original_travel==$type_jurney_storage){
                    return \response()->json([
                'code'=>408,
                'status'=>false,
                'message'=>'cannt add this passport into your jurney because this type travel (go , back) not match with your type travel in your reservation',
                'data'=>null
            ]);
                }
           //لو حكينا مع هادا السواق وستيتس 1 اي بستنو عادي بس برقم الرحلة افضل 
           //المهم ما بزبط يدخل اكتر من العدد المسموح
           $driver=User::where('id',auth()->user()->id)->first();
                   $loading_car=$driver->car->max_loading;
                                    $countTravelersJurey=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->count();
          if($countTravelersJurey<$loading_car){
                             //لازم بس ياكد ل8 مسافرين ما بزبط اكتر 
                    $data=$request->validated();//req: passport_num , type-> go(0) , back(1)
                  $client=  User::where('passport_num',$data['passport_num'])->first();//traveler
                  //بدي اتاكد هل موجود اصلا بالنظام ولا لا 
                  if(empty($client)){
                       return \response()->json([
                    'code'=>404,
                    'status'=>false,
                    'message'=>'this client not exist in system',
                    'data'=>null
                ]);
                  }
                  
                //بدي اتاكد هل هادا الجواز اصلا موجود بجدول الحجوزات 
                   $reservation= Reservation::where('client_id',$client->id)->first();
                    if(!$reservation){
    
                                          return \response()->json([
                    'code'=>406,
                    'status'=>false,
                    'message'=>'cannt add this passport into your jurney because this passport not exist in reservations system',
                    'data'=>null
                ]);
                    }
                                       $type_jurney_storage=Storage::get('type_jurney');//نوع السفر تاع الرحلة لازم نوع الحجز اللي حضيفه هو نفس نوع هادا 
    
                  //فحص هل نوع السفر اللي حيضيفه لهادا الجواز اللي حيركب معه نفس نوع السفر اللي بالحجز تاعه ولا لا 
                    if($reservation->travel==$type_jurney_storage){
                        return \response()->json([
                    'code'=>408,
                    'status'=>false,
                    'message'=>'cannt add this passport into your jurney because this type travel (go , back) not match with your type travel in your reservation',
                    'data'=>null
                ]);
                    }
    
    // //بدي اتاكد هل هادا الجواز محجوزلو برحلة معينة اي لسائق لهادا ولا لا 
              //هو بالاصل ما بكون محجوز عند سائق معين بس من هاي الميثود بنحط عند سواق معين لانو المسافر بدور وين يروح عاي سواق فبعد م يركب  المسافر . السواق بمسك جوازو وبصضيفو لالو م هو هنا هيك هيك هادا الجدول موود فيه الدرايفر والمسافر فبنحط هادا السواق مع اله المسافر هادا 
              
               
                //    //بدي اتاكد هل هادا السائق في وسع ولا لا في سيارته
                //لا خلص مش ضروري لانو بالاصل المسافر محجوزلو ممع هادا فما بزبط مرة تانية اشوف في وسع ولا لا وهو بالاصل محجوز لهادا المسافر فبهيك حالة ممكن لما افحص  حيكون هادا المسافر مااخد وسع بالفعل ومحطوط لهاا السائق فخلص بالحجز نفسه بتحدد المسافر هادا للسائق هادا 
                
                   $driver= User::where('id',auth()->user()->id)->first();
                //    if($driver->loading_car==1){//loading car  completed
                //     return 'cannt add another traveler , because your car completed loading it';
                //    }
                    $readyTraveling=new ReadyTraveling();
                    $readyTraveling->car_id=$driver->car->id;//car driver
                    $readyTraveling->client_id=$client->id;
                    $readyTraveling->jurney_num=$jurney_num_storage;
                    $readyTraveling->operation_num_reservation=$reservation->operation_num;//رقم العملية تاعة الحجز اللي ااجى منه
                //confirmed  this client
                $readyTraveling->confirmed=1;
                    $readyTraveling->status=1;//wait to traveling 
                    $readyTraveling->save();
                    //زي م بنعبي بالسفر حنعبي بالسيارة 
                    $carDriver= CarName::where(['id'=>$readyTraveling->car->id])->first();//driver
                    $carDriver->current_loading=$countTravelersJurey+1; //بعد م انضاف بنشوف اكم عدد المسافرين صارو بالرحلة وبنضيفهم وهو بالاصل مش حيضيف المسافر هادا الا لما يتاكد انوتمام بالنسبة للحملة 
                    
                    $carDriver->save();
                    //المفروض خلص  ااخد الرحلة اللي انضاف فيها هادا اليوزر عشان هو بعد م اضيفه هادا حفحص هل احط الزر ولا لا تاع البدء 
                    //   $readyTraveling=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->first();
                     $loading=0;


                                  $car= CarName::where(['id'=>$readyTraveling->car->id])->first();//car
                    if($car->max_loading==$car->current_loading){
                        $loading=1;
                    }
                    $data=[
                        'traveler'=>$readyTraveling->load(['car','client']),
                        'loading'=>$loading
                        ];
                     return \response()->json([
                    'code'=>200,
                    'status'=>true,
                    'message'=>'passport this traveler has been added successfully',
                    'data'=>$data
                ]);
                    //بعد م ياكد ممنوع يحدف حد منهم  وممنوع يعمل انهاء للرحلة 
                //بقدر ينهي الرحلة بس في حالة م يكونو كل الركاب دافعين ولو حد مش دافع حبحكيلو في راكب بمقعد كدا مش مدفوع 
                //زي م احنا عاملين برحلة السواق كم شخص لم يدفع  يعني ببنلو كم عدد اللي ما دفعو بس بس ولو بدو يعرف مين ما دفع برووح ع لوحة الركاب بشوف وبتتبع الركاب 
                
               //هو من اول الحجز محجوز لالو فمش ضروري مرة تانية نكتب لهادا السائق م هو طبيعي مش حييجي عالسطر هادا الا يكون للسائق هادا محجوز 
                // //هادا ببيجي عليه بس لما يكون السيارة مليانة بحكيلو تمام خليك بالانتظار لتخلص رحلة السيارة هاي  
                
                //  //confirmed reservation this client بااكد الحجز لهادا الشخص اللي محجوزلو مش اللي حجز وممكن هو نفسه اللي حجز المهم التدكرة باكدها للشخص اللي راح يسافر فيها 
                //  //هو مرة وحدة اصلا بكون محجوزلو فبعمل فيرست 
       
                // // now this traveler ready to travel in car this driver
          }else{
                return \response()->json([
                    'code'=>407,
                    'status'=>false,
                    'message'=>'cannt add another traveler into your jurney , because reached  into max in loading',
                    'data'=>null
                ]);
          }
       }
  

}
//اصلا مستحيل يتخزن ايشي بجدول السفر الا لما السائق يضغط ع تاكيد فبتكريت صف للي ضغلطلو وبتاكلدلو
//هاتلي كل المسافرين اللي انا كسائق اكدتلهم 
//من خلال لوب عالمسافرين اللي معه وخلص بهيك بجيب كل المسافرين اللي اكدتلهم انا وبدي بس اللي بستنو بالرحلة بالتالي الستيتس 1 
//حنحط عمود التاكيد بجدول السفر لانو طبيعي الحجز مش حيتاكد الا من قبل السواق لما المسافر يكون معه اي بجدول السفر 

public function getAllConfirmedReservations(Request $request){//for this driver
    $driver=User::where(['id'=>auth()->user()->id])->first();
  $allTravelersConfirmedWithDriverWait=  ReadyTraveling::where(['car_id'=>$driver->car_id,'status'=>1,'confirmed'=>1])->with(['car','client'])->paginate($request->total);
//   $reservations= Reservation::where(['driver_id'=>auth()->user()->id,'confirmed'=>1])->get();
   if(count($allTravelersConfirmedWithDriverWait)==0){
        return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'not found any reservations confirmed with you(driver) and confirmed it',
                'data'=>null
            ]);
    }
                         $loading=0;

                                      $car= CarName::where(['id'=>$driver->car_id])->first();//car
                    if($car->max_loading==$car->current_loading){
                        $loading=1;
                    }
                    $data=[
                        'allTravelersConfirmedWithDriverWait'=>$allTravelersConfirmedWithDriverWait,
                        'loading'=>$loading
                        ];
    return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'all travelers with you that confirmed and wait the jurney',
                'data'=>$data
            ]);
}
public function cancelConfirmReservation($travelingId,$jurney_num){
    //هنا حنعمل كان شيئا لم يكن 
   //يعني حلغيه من الحجوزات وا\لسفر 
    //ما بنفع يكنسله لو بدات الرحلة 
  //يعني هنا حافحص الستيتسس لو كانت 1 بقدر اما لو 2 ما بقدر لانو بدا بالرحلة ولو 3 بنفعش لنو انتهت الرحلة فمن الاول كمان بصفحة تاكيد الحجوزات ما اعرض الناس اللي خاتاكدلهم وخلصو واتاكدلهم وهم بالرحلة 
  //لا المفروض عادي ينعرضو لانو هاي صفحة لاشوف الحجوازت اللي اتاكدت 
  //بس لا م هو طبيعي لو فعلا هو انتهى او بالرحلة فطبيعي متاكد فليش اعرضو بهاي الصفحة فاعرض بهاي الصفحة بس الناس اللي اتاكدو وبستنو تبدا الرحلة اما غيرهم اكيد ماكدين اصلا 
    //فاول م يعمل بدء رحلة ما بزبط الغي شي 
    //وكمان من اول م تبدا الرحلة في رقم بنتشا للرحلة
    //بحيث انا بالسيستم كادمن لما ادق رقم الرحلة ييجيني كدا كدا ومع اي سواق 
  //ومبين مع تفاصيلها دهاب للمعبر ولا عودة من المعبر 
   $traveling= ReadyTraveling::where(['id'=>$travelingId])->first();
//   dd($traveling);
   $reservation=Reservation::where(['operation_num'=>$traveling->operation_num_reservation])->first();
   if(empty($reservation)){
       return \response()->json([
        'code'=>404,
        'status'=>false,
        'message'=>'this reservation id not exist in reservation table system',
        'data'=>null
    ]);
   }
  $clientReservation=Reservation::where(['id'=>$reservation->id])->first();
  if(!$clientReservation){
      return \response()->json([
        'code'=>404,
        'status'=>false,
        'message'=>'this reservation id not exist in reservation table system',
        'data'=>null
    ]);
    
  }else{
      
      $jurney= ReadyTraveling::where(['jurney_num'=>$jurney_num,'client_id'=>$clientReservation->client_id])->first();//get any one from this jurney , because all it togother , or specfic clientId
      if($jurney){
    
          if($jurney->original_status==2){
                return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'cannt cancel this traveler because , now in jurney',
            'data'=>null
        ]);  
          }elseif($jurney->original_status==3){
                return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'cannt cancel this traveler because , he finished from  the jurney',
            'data'=>null
        ]);  
          }elseif($jurney->original_status==1){
    
            $reservation =  Reservation::where('id',$reservationId)->first();
              //delete it from table traveling
             $travelerWithDriver= ReadyTraveling::where(['jurney_num'=>$jurney_num,'user_id'=>$reservation->driver_id,'client_id'=>$reservation->client_id])->first();//حالغي اللسفر للشخص اللي مسافر مش الشخص اللي حجز لا اللي انحجزلو 
            $travelerWithDriver->delete();
    //هو هيك هيك ما بدخل اصلا اكتر من الغعدد المسموح لانو الراوت تاع الاضافة ما بدخل اكتر 
    //هنا طالما نقص واحد من السفر فلازم ينقص من السيارة لانو خلص التغى طار 
                   $carDriver= CarName::where(['id'=>$reservation->driver->car->id])->first();
                   $carDriver->current_loading=$carDriver->current_loading - 1;
                   $carDriver->save();
                   
          if(!empty($reservation)){
        
              $reservation->delete();
              return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'this reservation has been canceled successfully',
                'data'=>$reservation
            ]);
          }else{
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'this reservation not found or canceled in prev. time',
                'data'=>null
            ]);
          }
    
    
    
          }
      }
  }
}
    public function getAllPaymentsRecord(){
        $user=auth()->user();
        $personal_id=$user->personal_id;
       $allPaymentsRecord = Payment::where(['user_id'=>$user->id])->with(['driver','reservation'])->get();
       $data=[
           'personal_id'=>$personal_id,
           'allPaymentsRecord'=>$allPaymentsRecord
           ];
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'data has been getten successfully',
            'data'=>$data
        ]);
        }
    public function addPayment(AddPaymentRequest $request){
        //لما يضغط ع زر دفع الاول بتطلع صفحة الباركود وبحط الباركود وعند م يحطه الباركود بفحصه لو كان بنفع اي هو بنفس اليارة اللي راكب فيها بدفع تمام بنحكيله عالصفحة التانية وقبل هالفحص بنشوف هل مش دافع قبل هيم لو مش دافع بنحكيلو اكتب كود الامان بالاول 
        // وبرجع يحط الباركود ليتنقل عالصفحة اللي بعد اللي هي انت بتدفع 
        
        //الدفع ما نخليه يصير الا لما تكون الرحلة بادية 

        //بضفحة الحجوزات بتكون كل الحجوزات لهادا الشخص  سواء هو اللي حجز لالو ولا في حد حجزلو بقدر يفتح كل وحدة فيهم ويدفع وبنخصم من الشخص اللي حجز مش من هادا اللي بدو يدفع فممكن هو وممكن حد حجزلو 
//يعني بدي افتح الحجز وبهيك بااخد منه عدد النقاط والشخص اللي حجز 
    $data=$request->validated();
//     //بدي اشوف السيارة اللي دخل فيها هادا المسافر 
//   $user= User::where(['id'=>auth()->user()->id])->first();//traveler
//   dd($user);
//   $carId=$user->car->id;//السيارة اللي 

    $clientPayment=Payment::where(['user_id'=>auth()->user()->id])->first();
            $client=User::where(['id'=>auth()->user()->id])->first();

//     //يعني هادا الكلينت محجوزلو قبل هيك هيك بالتالي بزبط يعمل لكن لو كان لاول مرة لازم يدخل كود الامان وبعد م يدخله نتاكد انو كود الامان تمام لهادا اليوزر بالاصل
    // if(empty($clientPayment)){
//         //لازم احنا نطلب منه يدخل الكود بس في هاي الحالة انو فعليا مش محجوزلو قبل هيك 
//         //قصدي لو ما كان دافع قبل هيك 
        // if($client->security_code==null){
        //   return \response()->json([
        //     'code'=>400,
        //     'status'=>false,
        //     'message'=>'you must write your security code',
        //     'data'=>null
        // ]);
        
        // }

        
    // }
        // بدنا نعرف المسافر هادا باي سيارة راكب ووين المفروض يركب 
    $carTraveledUser=ReadyTraveling::where(['client_id'=>$client->id,'status'=>2])->first();//هادي سيارة المسافر  اللي حاليا هو فيها وبالرحلة ماشي 
    
    //بهاي السيارة المفروض يكون لانو انضاف فيها 
   //اي بهاي السيارة هو انضاف 
    $carAddedUser=ReadyTraveling::where(['client_id'=>$client->id,'status'=>1])->first();
//لازم السيارة اللي ماشي فيها حاليا هي نفس السيارة اللي اتسجل فيها بالاول من قبل السواق

//حنقارن الباركود اللي حطه برقم السيارة اللي المفروض يكون فيها اي باللي اتسجل فيها بالاول 
    if($data['barcode']!==$carAddedUser->car_id){
         return \response()->json([
        'code'=>407,
        'status'=>false,
        'message'=>'cannt add payment , because you not connect with this car , to pay in it ',
        'data'=>null
    ]);
    }
    //السيارة هاي مين سائقها مش السائق وين سيارته عشان قصة السائق ممكن يروح يسوق سيارة تانية 
    
   $driver=User::where(['car_id'=>$carAddedUser->car_id])->first();
   
   $data=[
       'driver'=>$driver->name,
       'car_id'=>$carAddedUser->car_id
       ];
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'your payment Its about to be completed. with driver ',
        'data'=>$data
    ]);
// //for this point : reach into -> points move from wallet payer into wallet driver(reservation status: completed)
}
    // public function addSecurityCodeForReservation(AddSecurityCodeReservationRequest $request){
    //     //حييجي هنا في حالة م يكون محجوزلو من قبل وفي الو كود بس بدنا اياه يحطه تاني لانو بحجز لالول مرة 
    //     //في حالة محجولز من قبل وما في الو كود فبحط الكود بردو عشان ما الو كود بالاصل ومش محجوزلو مش من قبل 
    //     //لو محجوزلو من قبل ولكن ما الو كود 
    //     //فاول م يدخل هنا بس مجرد حيتكريت الكود لو ما الو وحينفحص لو الو 
    //     $data=$request->validated();
    //     // $data=$request->all();
    //     $passport_num=$data['passport_num'];
    //   $user= User::where(['passport_num'=>$passport_num])->first();
    //   if($user->security_code==null){
    //     $user->security_code=$data['security_code'];
    //     $user->checked=1;
    //     $user->save();
    //     return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code has been added successfully',
    //             'data'=>$user
    //         ]);
    //   }else{
    //       if($user->security_code==$data['security_code']){
    //                   $user->checked=1;

    //           return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code is true',
    //             'data'=>$user
    //         ]);
    //       }else{
    //                   $user->checked=0;

    //                         return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code is false , pls try again',
    //             'data'=>$user
    //         ]);
    //       }
    //   }

      
    // }

    // public function addSecurityCodePayment(AddSecurityCodePaymentRequest $request){
    //     //في حالات بردو 
    //     //الاولى لو بدفع لالول مرة وموجود كود لالو فبنفحصه 
    //     //التانية لو بدفع لالول مرة ومش موجود لالو الكود فبكريتله 
    //     //التالتة لو دافع من قبل لكن بالصدفة لقينا انو ما الو 
    //     //بكل الحالات حنفحص هان هل الو او لا فلو الو بنفحص ولو ما الو بنكريت 

    
    
    //         $data=$request->validated();
    //     $userId=auth()->user()->id;
    //   $user= User::where(['id'=>$userId])->first();
    //   if($user->security_code==null){
    //     $user->security_code=$data['security_code'];
    //     $user->checked=1;
    //     $user->save();
    //     return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code has been added successfully',
    //             'data'=>$user
    //         ]);
    //   }else{
    //       if($user->security_code==$data['security_code']){
    //                   $user->checked=1;

    //           return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code is true',
    //             'data'=>$user
    //         ]);
    //       }else{
    //                   $user->checked=0;

    //                         return \response()->json([
    //             'code'=>200,
    //             'status'=>true,
    //             'message'=>'your security_code is false , pls try again',
    //             'data'=>$user
    //         ]);
    //       }
    //   }
    // }

//بنحط بالباريميتر  بندخلله ايدي كار  لنجيب السواق بندخخل ايدي الكار من خلال ريسبونس اللي من الراوت اللي قبل لما يحط الباركود صح وتمام 
    public function showForPayment($carId){

        $driver=User::where(['car_id'=>$carId])->first();
        $data=[
            'driver'=>$driver->name
            ];
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'data has been getten successfully',
            'data'=>$data
        ]);
        
    }
//هادا بعد يختار التدكرة من الاسماء تاعون المسافرين اللي هو حاجزلهم بصفحة انت بتدفع 

    public function completePayment(CompletePaymentRequest $request)
{
    $data=$request->validated();//req-> reservation_id for client to make pay for his
    //     //store payment in table payments
//     //user: a person that pay , client : driver
    $reservation= Reservation::where('id',$data['reservation_id'])->first();
    //to know driver this user , from table traveling that connect with this reservation , via operation_num_reservation
  $reservationTraveling= ReadyTraveling::where(['operation_num_reservation'=>$reservation->operation_num])->first();
   
  //هلا الرحلة اللي راكب فيها نوعها عبارة عن عودة ولا دهاب اي السيارة اللي راكب فيها هلا نوعها هيك بغض النظر مين السائق بدنا نعرف شو نوع اللي راكب فيها هلا اي نوع رحلته الحالية ونقارنهاربنوع تدكرته 
  if($reservationTraveling->travel!==$reservation->travel){
      return \response()->json([
            'code'=>411,
            'status'=>false,
            'message'=>'cannt pay , because type travel in your reservation not match with current your jurney',
            'data'=>null
        ]);
  }
    if(!$reservation){
        return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'this reservation not found to make payment on it',
            'data'=>null
        ]);
        }

//بنفحص هل ايدي الحجز اللي اخترته ه مع نفس السواق تاع السيارة اللي انا فيها يعني انا يوسف حجزت للبنى وو فلازم لما ادفعلهم يكونو اصلا انضافو بسيارة اللي انا فيها لاقدر ادفعلهم 

//فبدي اشوف ايد يالحجز اللي ضغطت عليه متلا تاع لبنى من خلالرقم العملية تاعتها بشوف وسنها بجدول اسفر لاعرف هي باي سيارة دخلت  فلو بسيارتي بقد ادفعلها لو مش بسيارتي دخلت ما بقدر ادفعلها هي بتدفع لنفسها عادي 
$traveling=ReadyTraveling::where(['operation_num_reservation'=>$reservation->operation_num])->first();
    if(empty($traveling)){
        return \response()->json([
            'code'=>410,
            'status'=>false,
            'message'=>'operation_num reservation not found in system traveling',
            'data'=>null
        ]);
    }
    
    //بدي اشوف سيارة الشخص اللي بدو يدفع عن التاني اللي هو يوسف حدور ع
  //حدور عالحالية ليوسف 
  
 $currentJurneyTraveler= ReadyTraveling::where(['client_id'=>auth()->user()->id,'status'=>1])->first();
    if($traveling->car_id!==$currentJurneyTraveler->car_id){
                return \response()->json([
                'code'=>406,
                'status'=>false,
                'message'=>'cannt pay  , because this traveler that want pay to his not in your car',
                'data'=>null
                ]);
    }
        //بنفعش يدفع بعد م يكون ناهي الرحلة السواق
        if($reservationTraveling->original_status==="3"){
            
            return \response()->json([
                'code'=>409,
                'status'=>false,
                'message'=>'cannt pay  , because you finished from jurney',
                'data'=>null
                ]);
            }
            //بنفعش يدفع وهي الرحلة لسا ما بدت لازم تبدا ليقدر يدفع 
                    if($reservationTraveling->original_status==="1"){
            
            return \response()->json([
                'code'=>408,
                'status'=>false,
                'message'=>'cannt pay  , because your jurney not start until now , pls wait start it',
                'data'=>null
                ]);
            }
            
        if($reservationTraveling->original_confirmed=="0"){
            
            return \response()->json([
                'code'=>400,
                'status'=>false,
                'message'=>'cannt pay now , because this reservation not confirmed until now',
                'data'=>null
                ]);
            }
            // dd($reservation);
    //بنقع ادفع لانو مؤكد حجزي والشخص اللي اكده هو اكيد السواق اللي انا دفعله فتمام لانو السواق ممنوع ياكد الا الجوزات اللي بسيارته 
    //فلما بدي اعرف مين الشخص اللي عمل تاكيد مش حروح الا لسواقي 
    //او احط درايفر ايدي وهو بكون نل ومحرد م ااكد ايدي السواق بيتعبا 
    $userId=$reservation->user_id;
    //clientId  هنا اليوزر هو المسافر والكلينت هو السواق لانو هنا انا كيوزر بدفع لمين للسواق 
    //هلا لما بقرا الباركود ادخلله بالريكويست هادا الباركود تاع السواق فبنعرض قدامي اسم السواق فهادا الباركود هو اللي بحدد لمين بدي انقل الفلوس 
    //الباركود بطلعلي اسم السواق مع رقم المعرف تاع السواق ورقم الكرسي 
    //لانو الباركود مش حعمل هاي الخطوة الا لما اكون راكبة مع السواق فالكرسي اللي قاعدة عليه حيكون فيه الباركود
    //فمن اول م اخلص من قراءته بطلعلي رقم الكرسي واسم ورقم معرف السواق 
    //فعملية الدفع تتم بعد قراءة الباركود اي حدفع للسواق اللي قرات الباركود اللي بالكرسي اللي بسيارته 
    //هلا عشان نتفادى انا كمسافر ادفع لحد تاني فمن اول م اضغط ع دفع بيشيك هل اسمي انا كمسافر موجو بالسيارة تاعة هادا السواق 
    //يعني هل هادا الراكب موجود من ضمن الناس اللي ماكد حجزهم عند هادا السواق 
    //بهيك منعت اي حد يدفع الا يكون رقم جوازه ماكد من قبل السواق اللي بدي ادفعلو

//بدي اتاكد انو الدفع حيتم للسواق اللي اكدلي 
  // بدي اتاكد انو السواق اللي حدفعلو هو نفس اللي اكدلي  
  // اللي اكدلي بمسكه من خلال
  //هو الدفع بيتم من خلال بفتح ع صفحة الحجوزات تاعتي وبختار حجز معين سواء انا اللي حجزت ولا في حد حجزلي اللهم انو في حد حجزلي فينخصم منه ولو انا اللي حجزت حجز معين فينخصم مني 
  // فلما ضغطت ع حجز معين من خلال الايدي تبعه بشوف رقم العملية تاعة الحجز هادي بشوف نفسها بجدول السفر لاعرف السواق اللي الحجز هادا صار صاحبه عند هادا السواق 
  //فمسكت السواق اللي الحجز هادا وهو هادا الواق اللي اكدلي اصلا فبقارنه ب .... 
  //هلا بدي السواق اللي حدفع عندو هو اللي اقارنه في اللي فوق 
  //هو مستحيل يدفع لحد تاني لانو طبيعي حنااخد السائق تاع الحجز هادا هو اللي حنعرض ه للمسافر ونحكيلو انت بتدفع لسائق كدا وهو تلقائي ايدي السائق هادا بنحط بالدفع لما اعمل دفع فما في داعي نعمل فحص لانو طبيعي تلقائي بتااخد سواق هادا الحجز وبنحط بالدفع اللي حعمله ع هادا الحجز 
  
    //         'message'=>'cannt pay now , because you pay for a driver not confirmed by him ',

        $operation_num=mt_rand(100000, 999999);
        //ادور عليا انا  كمسافر هل موجود بتاكيد الركاب اي بجدول السفر وماكد 
//لو موجود بتصير الدفع لو مش موجود ما بزبط الدفع 
    $userConfirmedInTraveling=ReadyTraveling::where(['client_id'=>$reservation->client_id,'confirmed'=>1,'status'=>1])->count();
    //اي شوفلي هادا اليوزر اللي الحجز تاعه هادا شوفلي اياه موجود السفر وماكد وبستنى بالرحلة لانو ممكن يكون باكتر من رحلة والرحلة متلا الاولى طبيعي مخلصة فاحنا بنحكي شوفلي اياه برحلة بتستنى تبدا وماكد 
    //لو مش موجد فما بزبط ادفع 
    if($userConfirmedInTraveling==0){
         return \response()->json([
        'code'=>407,
        'status'=>false,
        'message'=>'cannt add payment , because this traveler not exist in confirmedTraveling',
        'data'=>null
    ]);
        
    }
    //هلا عشان اعرف هادا الشخص هو مربوط مع هادي السيارة اي مسموح يدفعلها ولا لا حنحكي 
    //
    //حنجيب 1رقم المركبة اي الباركود الحالية اي اللي راكب المسافر هلا فيها 
    //كيف حعرف رقم المركبة اللي راكب فيها هي نفسها اللي المفروض يركب فيها لينفع الدفع 
 
     $payment=new Payment();
    $payment->user_id=$userId;//person that reserved this ticket (possible be this traveler or not)
    // $payment->driver_id=$reservationTraveling->user_id;//driver
    $payment->car_id=$reservationTraveling->car_id;//driver
    $payment->reservation_id=$idReservation;
 //   $payment->barcode=$data['barcode'];
    $payment->points_count=$reservation->points_count;
    $payment->operation_num=$operation_num;
    $payment->save();

    //في عمود ليبين هادا الحجز دفع ولا لا فبدنا نعدل فيه وندور عاليوزر اللي دفع للسواق هادا بجدول الحجز لنحطه تم الدفع
    //ولا ندور ولا شي مجرد احط الحجز اللي مسكته احطله تم الدفع 
    $reservation->paid=1;
    $reservation->save();
    //move points count from hidden wallet user into driver///
    //decrease points from hidden
    //لازم رقم العملية تاعة الحجز اللي بجدول الحجوزات تكون نفسها اللي اتخزنت بالمخفي عشان لما ارع لحجز شخص بالمخفية تبعته ارجع برقم العملية
    $UserHiddenWallet=UserHiddenWallet::where(['user_id'=>$userId,'operation_num'=>$reservation->operation_num])->first();//لانو في كتير حجوزات لهادا اليوزر متلا فلازم اعمل جيت حسب رقم العملية اللي هي تاعة الحجز 
    $reservationPointsCount = explode('_',$UserHiddenWallet->reservation_points_count, 2)[1];
    $UserHiddenWallet->reservation_points_count='AR_'.($reservationPointsCount-$reservation->points_count);
    $UserHiddenWallet->save();
    //increase points in wallet driver
  $PointsNotAllowedUse= PointsNotAllowedUse::where(['user_id'=>$reservation->driver_id])->first();
  $PointsNotAllowedUseNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
  $PointsNotAllowedUseWallet = explode('_',$PointsNotAllowedUse->points_count, 2)[0];

    if(!empty($PointsNotAllowedUse)){
        $PointsNotAllowedUse->user_id=$reservation->driver_id;
        $PointsNotAllowedUse->points_count=$PointsNotAllowedUseWallet.'_'.($PointsNotAllowedUseNum+$reservation->points_count);
        $PointsNotAllowedUse->save();
        //change status reservation because this  has been reserved (this reservation became in level payment)
        $reservation->status=3;
        $reservation->save();

    }else{
        $PointsNotAllowedUse=new PointsNotAllowedUse();
        $PointsNotAllowedUse->user_id=$reservation->driver_id;
        $PointsNotAllowedUse->points_count=($reservation->points_count);
        $PointsNotAllowedUse->save();
        //change status reservation because this  has been reserved
        $reservation->status=3;
        $reservation->save();
    }
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'your payment has been added ',
        'data'=>$payment
    ]);
}    //لما يضغط عالزر الاحمر اي بعد صفحة قراءة البارككود عطول , بصفحة انت بتدفع لسواق 

    public function startJurney(){

        
        //لازم اهندل انو : ما يبدا الرحلة باكتر من 8 لازم 8 واكتر امنعه احكيلو خليهم للرحلة اللي بعد 
        //1. اجيب كل المسافرين مع هادا السواق والستيتس 1 وادخلهم ما عدا لما يوصلو ل 8 
       //او اجيب رقم الرحلة اللي بالسشن اللي اتخزنت وانا بضيف الارقام هاي لالي كسواق فبجيب كل اللي خزنتهم فلازم 8 متلا لانو هناك ما ارضينا ايدخل الا 8 اما اكتر لا منعته بالتالي هنا لما اجيب عددهم حيكونو 8 او اقل ولو كان اقل ححكيلو 
       //بزبطش اقل روح ضيف كمان برحلتك فبروح هناك يضيف هادا الجواز فبنضاف معو وبهيك بقدر ابدا الرحلة لانو المسموح موجود هلا
       
        $jurney_num_storage=Storage::get('jurney_num');
       $ReadyTravelings=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->get();
                $driver=User::where('id',auth()->user()->id)->first();
        if(count($ReadyTravelings)==0){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'not found any travelers wait start jurney',
                'data'=>null
            ]);
        }
        $countStartingTaravelings=0;
        $inJurneyArr=[];
        $inJurney=null;
        $finishedJurneyArr=[];
        $finishedJurney=null;
        $errorMotionArr=[];
        $errorMotion=null;
        $travelerJurneyArr=[];
        $travelerJurneyNotPayArr=[];
        $loading=null;
        $errLoading=null;
        $success=false;
        $jurniesStartingAlready=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage,'status'=>2])->count();
        //اذا كان لا يساوي صفر او بيساوي نفس الحمولة بالتالي كلهم تمام بالرحلة بس لا المفروض لو واحد تمام الكل تمام بالرحلة لانو مش حتمشي الا كلهم تمام المهم بنختبر بالصفر افضل
        //لانو تحت حاكيين مش حيكون نجاح لال لما يكون اعددهم بنفس عدد الحمولة 
      //نقطع الشك من اليقين ونقارنه باللودنج عشان نضمن لهم راكبين هو هيك هيك المفروض كلهم راكبين بس يلا 
                        if(empty($driver->car)){
                            return \response()->json([
                        'code'=>409,
                        'status'=>false,
                        'message'=>'this driver maybe havent a car until now ',
                        'data'=>$travelerJurneyNotPayArr
                    ]);   
                        }
                               $loading_car=$driver->car->max_loading;
        if($jurniesStartingAlready==$loading_car){
            return \response()->json([
                        'code'=>400,
                        'status'=>false,
                        'message'=>'this jurney already starting now',
                        'data'=>null
                    ]);
        }
        foreach($ReadyTravelings as $ReadyTraveling){
          //  dd($ReadyTraveling);
            //لازم كل مسافر دافع للسواق اللي هي نفس رقم عملية الحجز موجود بالسفر فبنميك رقم العملية تاعته ونشوف هل دفع للسواق ولا لا 
        $reservationClient=  Reservation::where(['operation_num'=>$ReadyTraveling->operation_num_reservation])->first();
            if($reservationClient->original_paid=="0"){
                array_push($travelerJurneyNotPayArr,$ReadyTraveling->client->passport_num);
             
            }
            if($ReadyTraveling->original_status=="1"){//start jurney
                $countStartingTaravelings=$countStartingTaravelings+1;//يعني كل م يغير ستيتس رحلة واحدة  واخلص منها بزود ع هاي واحد عبل م تصير 8 احط باللودنج  0 عشان  هيناك لما ييجي يحجز عند هادا السائق يقدر يلاقيه فاضي ويحجز عندو
                array_push($travelerJurneyArr,$ReadyTraveling->client);
            }
          /////  //هلا ما بزبط ناس بالرحلة اللي انا فيها الا يكمونو 1 لانو لسا ما بديتها ولما ابداها كلهم يصيرو 2 ولازم بكل رحلة فيها 8 مسافرين بس اوفلما اضيف عند اضافة الجواز اهندلو احكيلو ما بزبط اتضيف بالرحلة اكتر من 8 بنفع اقل بس بردو ما حبدا بالرحلة الا اتضيفلا من عندك كمان عشان اقدر ابدا لانو ما ببدا وهم اقل من 8 
            
  
        }              
    if(count($travelerJurneyNotPayArr)!==0){
        $data=[
            'travelersJurneyNotPay'=>$travelerJurneyNotPayArr
            ];
        return \response()->json([
                        'code'=>406,
                        'status'=>false,
                        'message'=>'found a person or more not pay for a you (driver) , pls check this  traveler ',
                        'data'=>$travelerJurneyNotPayArr
                    ]);   
    }

        if(count($travelerJurneyArr)==$loading_car){
            foreach($travelerJurneyArr as $travelerJurney){
                //حدندور عالكلينت اللي ماسكين الايدي تاعه نمسكه من السفر عشاننغير الستستيس نقدر 
          //ما بزيط الشخص الواحد يكون بستنى بسيارتين فتمام بندور عليه وبحيث انو بكون بستنى 
             //change status into 2 to be in the jurney
            $clientReadyTraveling= ReadyTraveling::where(['client_id'=>$travelerJurney->id,'status'=>1])->first();
                    $clientReadyTraveling->status=2;
                    $clientReadyTraveling->save();
                     $user=User::where(['id'=>$clientReadyTraveling->user_id])->first();
                        $user->loading_car=1;//loading completed now
                        $user->save();
                        $success=true;
            }
            if($success){
                    $data=[
                        'travelersAllow'=>$travelerJurneyArr
            
                    ];
                    return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'now this jurney is ready',
                        'data'=>$data
                    ]);
                
            }
        //لا بزبط وهو اقل من 8 او اكتر من 8 ابدا بالرخلة 
        }elseif(count($travelerJurneyArr)<$loading_car){
            $data=[
                        'travelersWaitStart'=>$travelerJurneyArr
            
                    ];
                    return \response()->json([
                        'code'=>407,
                        'status'=>false,
                        'message'=>'cannt start this jurney , because until now not complete, so you can add another traveler into it',
                        'data'=>$data
                    ]);
        }elseif(count($travelerJurneyArr)>$loading_car){
                        $data=[
                        'travelersWaitStart'=>$travelerJurneyArr
            
                    ];
                    return \response()->json([
                        'code'=>408,
                        'status'=>false,
                        'message'=>'cannt start this jurney , because loading your car more its normal loading , pls cancel traveler from this jurney',
                        'data'=>$data
                    ]);
        }


                

    }
//هلا الرحلة الها 3 ستيتس : الاول بالانتظار اي 1 يعني ::: هادا المسافر كان محجوزو واتوافق عليه من قبل السواق ودخله عندو بالسيارة والستيتس 2 :: يعني اول م يضغط السواق بدء الرحلة عشان يصير ال 8 مسافرين معه الستيتس 1 وبعدين ستيتس 3 اي::: نهى الرحلة 

public function finishingJurney(){//delete all travelings with this driver , change status for anther jurny with this driver from 1 into 2 because it now is satrting
 //لازم ما حد يدخل هنا الا سواق

 //1. نبحث هل يوجد بالفعل مسافرين اصلا مع هادا السائق عشان ننهي رحلتهم  
 //2. هل كل رحلة فيهم بالفعل بادية هلا وما انتهت لننهيها  عشان لو في رحل معه بالانتظار نحكيلو هاي لسا بالانتظار ما بنفع تنهيها ولو  منهية نحكي هاي بالفعل منهية  اللي اكدلهم ومجرد م يتاكدو بنضافو عالرحلة تاعتي 
                $driver=User::where('id',auth()->user()->id)->first();
                if(empty($driver->car)){
                            return \response()->json([
                        'code'=>410,
                        'status'=>false,
                        'message'=>'this driver maybe havent a car until now ',
                        'data'=>null
                    ]);   
                        }
        $jurney_num_storage=Storage::get('jurney_num');
       $ReadyTravelings=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage])->get();
    if(count($ReadyTravelings)==0){
        return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'not found any traveler with this driver, until now , maybe no bady travel with this driver or maybe until now in level reservation not move traveler with this driver into start to jurney',
            'data'=>null
        ]);
    }

    $countFinishedTravelings=0;
    $errorMotionArr=[];
    $errorMotion=null;
    $waitStartJurneyArr=[];
    $waitStartJurney=null;
    $finishedJurneyArr=[];
    $finishedJurney=null;
    $travelerJurneyArr=[];
    $travelerJurneyNotPayArr=[];
    $loading=null;
    $success=false;
            $jurniesFinishedAlready=ReadyTraveling::where(['jurney_num'=>$jurney_num_storage,'status'=>3])->count();
            $loading_car=$driver->car->max_loading;
        if($jurniesFinishedAlready==$loading_car){
            return \response()->json([
                        'code'=>400,
                        'status'=>false,
                        'message'=>'this jurney already finished',
                        'data'=>null
                    ]);
        }
        //المفروض ما بزبط ينهي الرحلة الا يكون بادي اصلا فيها يعني ما بزبط من ستيتس 1 ل 3 لازم من البدء 2 للانتهاء3 زي ما بزبط يعمل بدء وهو مش دافع 
        
    //نفس قصة البدء برحلة هنا عند وضع انهاء لازم اكون موصلاهم كلهم
    foreach($ReadyTravelings as $ReadyTraveling){
                $reservationClient=  Reservation::where(['operation_num'=>$ReadyTraveling->operation_num_reservation])->first();
            if($reservationClient->original_paid==0){
                return \response()->json([
                        'code'=>406,
                        'status'=>false,
                        'message'=>'exist a traveler not pay : his passport:'.$reservationClient->client->passport_num,
                        'data'=>null
                    ]);
                array_push($travelerJurneyNotPayArr,$ReadyTraveling->client->passport_num);
             
            }
        if($ReadyTraveling->original_status==2){
            //يعني خلص خلصتهم كلهم وصلتهم 
            $countFinishedTravelings=$countFinishedTravelings+1;//يعني كل م يغير ستيتس رحلة واحدة  واخلص منها بزود ع هاي واحد عبل م تصير 8 احط باللودنج  0 عشان  هيناك لما ييجي يحجز عند هادا السائق يقدر يلاقيه فاضي ويحجز عندو
        //    if($countFinishedTravelings<=8){//لازم جوه هان نحط الاف هاي عشان نضمن يعد لما تكون الستيتس اللي بدنا اياه وبس اللي عهي 2 
                
                array_push($travelerJurneyArr,$ReadyTraveling->client);
            
            //بعد م خلصنا الرحلة مع ال 8 هدول بدنا من اول م يوصل ل 8 نوقف اللف ونحكيلو غيير حمولة السيارة ل 1 
       

           // }

        }
        //هدول كيف اصلا وانا بنفس الرحلة هاي وانا سائقها في ناس معي ومش معمول لالهم ستيتس 2 كيف بادي بالرحلة وهم معي هدول ما بزبط ابدا بالرحلة الا من اول م احط بدء الرحلة حيصيرو كلهم 2 ويركبو معي ونفس الشي كيف في ناس راكبين معي وهم اصلا منهيين 
           if(count($travelerJurneyNotPayArr)!==0){
        $data=[
            'travelersJurneyNotPay'=>$travelerJurneyNotPayArr
            ];
        return \response()->json([
                        'code'=>407,
                        'status'=>false,
                        'message'=>'found a person or more not pay for a you (driver) , pls check this  traveler ',
                        'data'=>$travelerJurneyNotPayArr
                    ]);   
    }


    }
                               $loading_car=$driver->car->max_loading;


    if(count($travelerJurneyArr)==$loading_car){
        foreach($travelerJurneyArr as $travelerJurney){
                        $clientReadyTraveling= ReadyTraveling::where(['client_id'=>$travelerJurney->id,'status'=>2])->first();

        //change status jurney from 2(In Juerny) into 3 (finished Jurney)
            $clientReadyTraveling->status=3;
            $clientReadyTraveling->save();
            // if(count($travelerJurneyArr)==8){
                $user=User::where(['id'=>$ReadyTraveling->user_id])->first();
                $user->loading_car=0;
                $user->save();
                $success=true;
                
            // }
        }
             if($success){
                 Storage::put('jurney_num','');//عشان انتهت الرحلة فلو بدو يبدا وحدة جديدة يقدر 
                $data=[
                    'travelersFinishedNow'=>$travelerJurneyArr
        
                ];
                return \response()->json([
                    'code'=>200,
                    'status'=>true,
                    'message'=>'now finished your jurney successfully',
                    'data'=>$data
                ]);

        }
    }if(count($travelerJurneyArr)<$loading_car){
        $data=[
                    'travelersWaitFinishingJurney'=>$travelerJurneyArr
        
                ];
                return \response()->json([
                    'code'=>408,
                    'status'=>false,
                    'message'=>'cannt finish jurney because your loading car less than normal its loading',
                    'data'=>$data
                ]);
    }if(count($travelerJurneyArr)>$loading_car){
                $data=[
                    'travelersWaitFinishingJurney'=>$travelerJurneyArr
        
                ];
                return \response()->json([
                    'code'=>409,
                    'status'=>false,
                    'message'=>'cannt finish jurney because your loading car more than normal its loading',
                    'data'=>$data
                ]);
    }
//بردو هنا ما بزبط اصلا يكون اقل من 8 لانو ما بزبط يبدا الرحلة باقل من 8 
//>8 , هادا بنفعش يكون اكتر من 8 بالسيارة فعند البدء لازم اكون مهندلها ما يكون اكتر من 8




}

//هاتلي كل الناس اللي دفعت لهادا السواق  ولو هو معو الشخص ولسا ما دفع عادي ما بكون بجدول الدفع وبكون بجدول الحجوزات لسا والتدكرة تاعته لسا 2 الستيتس تاعتها عشان لسا ما دفعت 
//ومن اول م يدفع بتصير التدكرة تاعته 3 وهو بييجي ع جدول الدفع هنا بالجيت حييجي للسواق هادا اللي التدكرة عندو 
    public function paymentsTravelers(){
      
        //اللي دفعو هم اكيد اللي موجودين بجدول السفر لانهم بكونو ما اجو الا يكون ماكدلهم وما بدفعو الا يكون ماكدلهم اصلا 
    //هنا ما بدو المسافرين اللي دفعو بس لا المسافرين اللي مع هادا السواق سواء دفعو ولا 
       //لكن هو كان السوزر هوو المسافرين والكلينت هو السائق 
       //اي الشخص اللي بيقوم بالاكشن هو اليوزر واللي بنعمللو الاكشن هو الكيلنت 
        //او ممكن نعمل جيت لكل التداكر اللي حتدفع للسواق هادا فلو كانت 3 اي دفعت ولو كانت 2 لسا حتدفع
   //هاتلي من الحجوزات لهادا السواق ن  فهادا بجيب سواء الدافعين ولا لا وحتفرق بينهم من خلال كلمة بييد 0او 1
   $carDriver=User::where(['id'=>auth()->user()->id])->first();
    $paymentsTravelers=  ReadyTraveling::where(['car_id'=>$carDriver->car_id])->with(['car','client'])->get();
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'all travelers for you (driver)',
        'data'=>$paymentsTravelers
    ]);
    return $paymentsTravelers;//هي من عندها لما تعمل لوب عليهم وتلاقي 3 حتخليه بالاخضر ولو مش 3 بالابيض انو لسا حيدفع
      //المهم اعملنا جيت لكل المسافرين مع هادا السواق سواء اللي دفعو او اللي جيدفعو

    }
   
}
