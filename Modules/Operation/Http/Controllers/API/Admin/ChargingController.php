<?php

namespace Modules\Operation\Http\Controllers\API\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\UserOriginalWallet;
use Modules\Auth\Entities\User;
use Modules\Operation\Entities\Charging;
use Modules\Wallet\Entities\SystemWallet;
use Modules\Wallet\Entities\PointsNotAllowedUse;
use Modules\Operation\Http\Requests\AddingToChargingRequest;
class ChargingController extends Controller
{
            public function __construct()
    {
        //for Delegate
         $this->middleware(['permission:addingToCharging_store'])->only('addingToCharging');
         $this->middleware(['permission:getWaitingChargingOperations_read'])->only('getWaitingChargingOperations');         
         $this->middleware(['permission:chargingToUser_update'])->only('chargingToUser');         
         $this->middleware(['permission:chargingToAllUsers_update'])->only('chargingToAllUsers');         
         $this->middleware(['permission:getAllChargingsRecords_read'])->only('getAllChargingsRecords');         
         $this->middleware(['permission:cancelCharging_cancel'])->only('cancelCharging');         
    }
    public function addingToCharging(AddingToChargingRequest $request){
        $data=$request->validated();
      $user=  User::where(['passport_num'=>$data['passport_num']])->first();
        if(!empty($user)){
         $charging= new   Charging();
         $charging->user_id=$user->id;
         $charging->points_count=$data['points_count'];
         $charging->status=1;//pending
         $charging->save();
         return \response()->json([
                                'code'=>201,
                                'status'=>true,
                                'message'=>'this user added into waiting list for charging',
                                'data'=>$charging
                            ]);
        }else{
            return \response()->json([
                                'code'=>404,
                                'status'=>true,
                                'message'=>'this passport num not exist in the system',
                                'data'=>null
                            ]);
        }
    }
    public function getWaitingChargingOperations(Request $request){
      $waitingChargingOperations=  Charging::where(['status'=>1])->with('user')->paginate($request->total);
      return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'getting data WaitingChargingOperations',
                                'data'=>$waitingChargingOperations
                            ]);
    }
    public function chargingToUser($id){
        // $user=  User::where(['passport_num'=>$passport_num])->first();
        // if(!empty($user)){
        //     $chargingToUser = Charging::where(['user_id'=>$user->id])->first();
        //   if(!empty($chargingToUser)){
                     $chargingToUser = Charging::where(['id'=>$id])->first();

                                  $operation_num = mt_rand(1000000000, 9999999999); 
            $chargingToUser->status=2;//charging operation completed
                     $chargingToUser->operation_num=$operation_num;
            $chargingToUser->save();
            //increasing count points in wallet this user
             $PointsNotAllowedUse=PointsNotAllowedUse::where(['user_id'=>$chargingToUser->user_id])->first();
            // dd($PointsNotAllowedUse);
            $latestWallet=SystemWallet::latest()->first();
            if(!empty($PointsNotAllowedUse)){

            $allowed_points_countNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                $PointsNotAllowedUse->points_count=$latestWallet->wallet_num.'_'.($allowed_points_countNum+$chargingToUser->points_count);
                $PointsNotAllowedUse->save();
                      return \response()->json([
                                'code'=>202,
                                'status'=>true,
                                'message'=>'charging operation for this user has been completed successfully',
                                'data'=>$chargingToUser
                            ]);
            }else{//we will create a row in table PointsNotAllowedUse for this user 
                  $operation_num = mt_rand(1000000000, 9999999999); 

         $PointsNotAllowedUse=  new PointsNotAllowedUse();
         $PointsNotAllowedUse->operation_num=$operation_num;
         $PointsNotAllowedUse->user_id=$chargingToUser->user_id;
         $PointsNotAllowedUse->points_count=$latestWallet->wallet_num.'_'.$chargingToUser->points_count;
         $PointsNotAllowedUse->save();
                               return \response()->json([
                                'code'=>202,
                                'status'=>true,
                                'message'=>'charging operation for this user has been completed successfully',
                                'data'=>$chargingToUser
                            ]);

            }
        // }else{
        //                           return \response()->json([
        //                         'code'=>406,
        //                         'status'=>false,
        //                         'message'=>'this user not exist in waiting list to charging',
        //                         'data'=>$chargingToUser
        //                     ]);
        //   }

        // }else{
        //                           return \response()->json([
        //                         'code'=>404,
        //                         'status'=>false,
        //                         'message'=>'this passport num not exist in the system',
        //                         'data'=>$chargingToUser
        //                     ]);
        // }
    }
    public function chargingToAllUsers(){
        //status for all : 2 -> completed charging operation
      $chargingToAllUsers=  Charging::where(['status'=>1])->get();
      if($chargingToAllUsers->count()!==0){
        foreach($chargingToAllUsers as $chargingToUser){
            // $operation_num = mt_rand(1000000000, 9999999999); 
            //   $chargingToUser->status=2;//charging operation completed
            //   $chargingToUser->operation_num=$operation_num;    
            // $chargingToUser->save();
            // //increasing count points in wallet this user
            //  $PointsNotAllowedUse=PointsNotAllowedUse::where(['user_id'=>$chargingToUser->user_id])->first();
            // // dd($PointsNotAllowedUse);
            // $latestWallet=SystemWallet::latest()->first();
            // if(!empty($PointsNotAllowedUse)){

            // $allowed_points_countNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
            //     $PointsNotAllowedUse->points_count=$latestWallet->id.'_'.($allowed_points_countNum+$chargingToUser->points_count);
            //     $PointsNotAllowedUse->save();
            // }else{//we will create a row in table PointsNotAllowedUse for this user 
            //       $operation_num = mt_rand(1000000000, 9999999999); 
            //      $PointsNotAllowedUse=  new PointsNotAllowedUse();
            //      $PointsNotAllowedUse->operation_num=$operation_num;
            //      $PointsNotAllowedUse->user_id=$chargingToUser->user_id;
            //      $PointsNotAllowedUse->points_count=$latestWallet.'_'.$chargingToUser->points_count;
            //      $PointsNotAllowedUse->save();

            // }
               $operation_num = mt_rand(1000000000, 9999999999); 
            $chargingToUser->status=2;//charging operation completed
                     $chargingToUser->operation_num=$operation_num;
            $chargingToUser->save();
            //increasing count points in wallet this user
             $PointsNotAllowedUse=PointsNotAllowedUse::where(['user_id'=>$chargingToUser->user_id])->first();
            // dd($PointsNotAllowedUse);
            $latestWallet=SystemWallet::latest()->first();
            if(!empty($PointsNotAllowedUse)){

            $allowed_points_countNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                $PointsNotAllowedUse->points_count=$latestWallet->wallet_num.'_'.($allowed_points_countNum+$chargingToUser->points_count);
                $PointsNotAllowedUse->save();
                   
            }else{//we will create a row in table PointsNotAllowedUse for this user 
                  $operation_num = mt_rand(1000000000, 9999999999); 

         $PointsNotAllowedUse=  new PointsNotAllowedUse();
         $PointsNotAllowedUse->operation_num=$operation_num;
         $PointsNotAllowedUse->user_id=$chargingToUser->user_id;
         $PointsNotAllowedUse->points_count=$latestWallet->wallet_num.'_'.$chargingToUser->points_count;
         $PointsNotAllowedUse->save();
                              
                              

            }
            
        }
                              return \response()->json([
                                'code'=>203,
                                'status'=>true,
                                'message'=>'charging operation for all users has been completed successfully',
                                'data'=>$chargingToAllUsers
                            ]);

      }else{
                                return \response()->json([
                                'code'=>407,
                                'status'=>false,
                                'message'=>'not found any users in waiting to charging for him',
                                'data'=>null
                            ]);
      }
        


    }
          public function getAllChargingsRecords(Request $request)
    {
     $chargingsRecord  = Charging::where('status',2)->orWhere('status',1)->paginate($request->total);
     
    //بنعمل اظهار لرقم العملية والمبلغ والتاريخ ورقم المعرف اللي هو البيرسنل ايدي تاع الشخص اللي انحجزلو تدكرة يعني الكلينت ايدي 
                                                                         return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'getting data successfully',
                                'data'=>$chargingsRecord
                            ]);
    }
        public function cancelCharging($id){
      $charging=  Charging::where(['id'=>$id])->first();
      if(!empty($charging)){
          $charging->delete();
                                                                               return \response()->json([
                                'code'=>201,
                                'status'=>true,
                                'message'=>'this charging has been deleted',
                                'data'=>$charging
                            ]);
      }else{
                                                                                         return \response()->json([
                                'code'=>404,
                                'status'=>false,
                                'message'=>'this id charging not found',
                                'data'=>null
                            ]);
      }
    }
    
}
