<?php

namespace Modules\Operation\Http\Controllers\API\Admin;

use Modules\Operation\Entities\TypeReservation;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\UserOriginalWallet;
use Modules\Wallet\Entities\UserHiddenWallet;
use Modules\Auth\Entities\User;
use Modules\Auth\Entities\Role;
use Modules\Operation\Entities\Reservation;
use Modules\Wallet\Entities\SystemWallet;
use Modules\Wallet\Entities\PointsNotAllowedUse;

use Modules\Operation\Http\Requests\AllowTypeReservationRequest;

use Illuminate\Support\Facades\Storage;
use Modules\Operation\Entities\CarName;
use DB;

class ReservationController extends Controller
{
            public function __construct()
    {
        //for Delegate
         $this->middleware(['permission:getDataUserToReservation_read'])->only('getDataUserToReservation');
         $this->middleware(['permission:addingToReservation_store'])->only('addingToReservation');         
         $this->middleware(['permission:getWaitingReservationOperations_read'])->only('getWaitingReservationOperations');         
         $this->middleware(['permission:allowTypeReservation_update'])->only('allowTypeReservation');         
         $this->middleware(['permission:reservationToAllUsers_update'])->only('reservationToAllUsers');    
         
        
         
    }
  
    public function storeUserHiddenWallet($pointsCountReservation,$operation_num,$userId){
        $UserHiddenWallet= new UserHiddenWallet();
        $UserHiddenWallet->reservation_points_count='AR_'.$pointsCountReservation;
        $UserHiddenWallet->operation_num=$operation_num;
        $UserHiddenWallet->user_id=$userId;
        $UserHiddenWallet->save();
    }
    public function updateReservation($reservation,$operation_num,$type,$status,$points_count,$carId=null){
       // $type=1->vip, $type=2->comfortable , $type->seating
       //status=1->reservation pending , status=3->reservation completed
        $reservation->operation_num=$operation_num;
        $reservation->type=$type;
        $reservation->status=$status;
        $car = CarName::where(['id'=>$data['car_id']])->first();
        $pointsCountVip=$car->price;
        $reservation->points_count=$points_count;
        $reservation->save();
    }
        public function storeReservation($newReservation,$userResrvationId,$clientId,$type,$travel,$status,$points_count,$carId=null){
            //ضمنت اول م اعمل حجز مين م كان يعمل حينحجز عند سواق معين  عشان لما اعمل اضافة وتاكيد الحجز انا كسواق وهو المسافر اصلا مش موجود لالي فما بزبط 
            //get all drivers
            $operation_num=mt_rand(100000, 999999);
            // $type=1->vip, $type=2->comfortable , $type=3->seating
            //status=1->reservation pending , status=3 ->reservation completed
            $newReservation->user_id=$userResrvationId;
            $newReservation->client_id=$clientId;
            $newReservation->operation_num=$operation_num;
            $newReservation->type=$type;
            $newReservation->type=$travel;
            $newReservation->car_id=$carId;
            $newReservation->status=$status;
            $newReservation->points_count=$points_count;
            return $newReservation;
    }
    
    //All cases for this fun : 1. from not allowed table only 2. from not allow table and  original wallet 3. from original wallet only
    public function operationsInsideWallet ($reservationToUser){//decreaseFromOriginalWalletAndStoreInHiddenWallet
        //decrease from original wallet and store points_count in hidden wallet user
        //decrease from original wallet (col. : allowed_points_count or table PointsNotAllowedUse)
        //1.decrease from  table PointsNotAllowedUse
            $pointsUserFromNotAllowedRedeem= PointsNotAllowedUse::where(['user_id'=>$reservationToUser->client_id])->first();
            if(!empty($pointsUserFromNotAllowedRedeem)){//check if this user have point in not allow table , if not -> will work on original wallet this user oonlyy
             $pointsUserFromNotAllowedRedeemNum = explode('_',$pointsUserFromNotAllowedRedeem->points_count, 2)[1];
             $reservationPointsRemaining=$reservationToUser->points_count-$pointsUserFromNotAllowedRedeemNum;
             if($pointsUserFromNotAllowedRedeemNum!==0){
                if($pointsUserFromNotAllowedRedeemNum>=$reservationToUser->points_count){//if his points in table not allowed , exist count points >= points count to reservation , will take all points required from this table
                    $total_pointsUserInNotAllowed= $pointsUserFromNotAllowedRedeemNum-$reservationToUser->points_count;
                    $latestWallet=SystemWallet::latest()->first();
                    if($total_pointsUserInNotAllowed!==0){//after take points from table not allowed will decrease , so when dcrease -> check points count in table not allowed , if reach to  0 , will delete it and work on original wallet oonlyy
                          $pointsUserFromNotAllowedRedeem->points_count=$latestWallet.'_'.$total_pointsUserInNotAllowed;
                          $pointsUserFromNotAllowedRedeem->save();
                          //2. store points_count in hidden wallet user
                             $operation_num = mt_rand(1000000000, 9999999999); 
                           //  $UserHiddenWallet= new UserHiddenWallet();
                            $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                            //change status reservation this user
                            $reservationToUser->status=3;//reservation operation completed  
                            $reservationToUser->save();
                            return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'reservation for a user has been succefully , via take points for reservation from table not allowed points',
                                'data'=>$reservationToUser
                            ]);
                    }else{// if reach to  0 , will delete it (we finished from reservation , because points reservation = point in not allwed table  , will poknits in this table =0 so will delete it (we dont need any points for reservation now , finished from it ->took all my points from table not allowed )
                    // and work on original wallet oonlyy, but points count for reservation not from original , because we take some points from not allowed table , 
                    //will calculate how much now we need points for reservation
                    //ex: points count for reservation =30 , in not allowed table for this client 10 just , so will take this 10 and delete this row , remain points to reservation = 20 will take it from original wallet
                        $pointsUserFromNotAllowedRedeem->delete();
                        $operation_num = mt_rand(1000000000, 9999999999); 
                       // $UserHiddenWallet= new UserHiddenWallet();
                        $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                        //change status reservation this user
                        $reservationToUser->status=3;//reservation operation completed  
                        $reservationToUser->save();
                        return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'derease these from wallet and delete row this user from not allow because now',
                                'data'=>$reservationToUser
                            ]);

                    }
                }else{//points count in table not allow <$reservationToUser->points_count , will sum it on points in original to know is enough or not
                    $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->client_id])->first();
                    $pointsUserFromNotAllowedRedeemNum = explode('_',$pointsUserFromNotAllowedRedeem->points_count, 2)[1];
                    $totalPointsInWalletAndNotAllowed= $UserOriginalWallet->allowed_points_count+$pointsUserFromNotAllowedRedeemNum;
                    if($totalPointsInWalletAndNotAllowed>=$reservationToUser->points_count){//check sum of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                         //3. decrease from original wallet (col. : allowed_points_count)
                        $remainedPointsWallet= $reservationToUser->points_count-$pointsUserFromNotAllowedRedeemNum;
                            $UserOriginalWallet->allowed_points_count=$UserOriginalWallet->allowed_points_count-$remainedPointsWallet;
                            $UserOriginalWallet->save();
                            $pointsUserFromNotAllowedRedeem->delete();
                            $operation_num = mt_rand(1000000000, 9999999999); 
                         //   $UserHiddenWallet= new UserHiddenWallet();
                            $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                            //change status reservation this user
                            $reservationToUser->status=3;//reservation operation completed  
                            $reservationToUser->save();
                            return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'$totalPointsInOriginalWalletAndNotAllowed >= $reservationToUser->points_count , so derease these from wallet and delete row this user from not allow because now sure empty pointscount',
                                'data'=>$reservationToUser
                            ]);
                    }else{
                                                    return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'this user havent enough points to completing reservation($totalPointsInOriginalWalletAndNotAllowed < $reservationToUser->points_count)',
                                'data'=>$reservationToUser
                            ]);
                    }
            }
                         
         }else{//$pointsUserFromNotAllowedRedeemNum==0, so this client exist in table pioints not allowed but his points =0 in it , so if be this case , must delete it , and after that will go into original wallet
             $pointsUserFromNotAllowedRedeem->delete();
              $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->user_id])->first();
                if($UserOriginalWallet->allowed_points_count>=$reservationToUser->points_count){//check points in original walet  of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                     // decrease from original wallet (col. : allowed_points_count)
                      $remainedPointsWallet= $UserOriginalWallet->allowed_points_count-$reservationToUser->points_count;
                        $UserOriginalWallet->allowed_points_count=$remainedPointsWallet;
                        $UserOriginalWallet->save();
                        $operation_num = mt_rand(1000000000, 9999999999);
                        $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                        //change status reservation this user
                            $reservationToUser->status=3;//reservation operation completed  
                            $reservationToUser->save();
                            return \response()->json([
                                'code'=>200,
                                'status'=>true,
                                'message'=>'reservation for a user has been succefully , via take points for reservation from wallet original',
                                'data'=>$reservationToUser
                            ]);
                }else{
                    return \response()->json([
                        'code'=>400,
                        'status'=>false,
                        'message'=>'this user havent enough points to completing reservation($UserOriginalWallet->allowed_points_count < $reservationToUser->points_count)',
                        'data'=>$reservationToUser
                    ]);
                }
         }
            }else{
                  $UserOriginalWallet=UserOriginalWallet::where(['user_id'=>$reservationToUser->client_id])->first();
                  if(!empty($UserOriginalWallet)){//else: this user haveent original user , this ما بزبط لازم يكون عندو فبهندلها
                    if($UserOriginalWallet->allowed_points_count>=$reservationToUser->points_count){//check points in original walet  of these > = $reservationToUser->points_count : derease these from wallet and delete row this user from not allow because now sure empty pointscount
                     // decrease from original wallet (col. : allowed_points_count)
                      $remainedPointsWallet= $UserOriginalWallet->allowed_points_count-$reservationToUser->points_count;
                        $UserOriginalWallet->allowed_points_count=$remainedPointsWallet;
                        $UserOriginalWallet->save();
                        $operation_num = mt_rand(1000000000, 9999999999); 
                      //  $UserHiddenWallet= new UserHiddenWallet();
                        $this->storeUserHiddenWallet($reservationToUser->points_count,$operation_num,$reservationToUser->user_id);
                        //change status reservation this user
                        $reservationToUser->status=3;//reservation operation completed  
                        $reservationToUser->save();
                        
                        return \response()->json([
                            'code'=>200,
                            'status'=>true,
                            'message'=>'$UserOriginalWallet->allowed_points_count>=$reservationToUser->points_count',
                            'data'=>$reservationToUser
                        ]);
                    }else{
                        return \response()->json([
                            'code'=>400,
                            'status'=>false,
                            'message'=>'this reservation has been rejected , because the user havent enough money for this rervation',
                            'data'=>$reservationToUser
                        ]);
                    }
                  }else{
                      $UserOriginalWallet= new UserOriginalWallet();
                      $UserOriginalWallet->allowed_points_count=0;
                      $UserOriginalWallet->user_id=$reservationToUser->client_id;
                      $UserOriginalWallet->save();
                      return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'this user haveent original user, so we created a wallet quickly , to be exist in next operations',
                        'data'=>$UserOriginalWallet
                    ]);
                  }
            }
            return true;
    }

    public function getDataUserToReservation($passport_num){
        $user=  User::where(['passport_num'=>$passport_num])->first();
        if(!empty($user)){
            $firstNumPhoneNo=substr($user->phone_no, 0, 1);
            $latestThreeNums=substr($user->phone_no, -3,3);
            if($firstNumPhoneNo=='+'){
                $ThreeNumPhoneNoEgypt=substr($user->phone_no, 2, 2);
                $ThreeNumPhoneNoPal=substr($user->phone_no, 5,1);
                $TypeNumPhoneNoEgypt=substr($user->phone_no, 5,1);
            if($user->original_documentation=='0'){
                $user->name='inactive';
            }
            if($ThreeNumPhoneNoPal=='9'){
                $data=[
                    'typePhoneNum'=>'jawwal',
                    'userName'=>$user->name,
                    'latestThreeNums'=>$latestThreeNums
                ];
                return \response()->json([
                    'code'=>200,
                    'status'=>true,
                    'message'=>'getting data successfully',
                    'data'=>$data
                ]);
            }elseif($ThreeNumPhoneNoPal=='6'){
                $data=[
                    'typePhoneNum'=>'oreedeo',
                    'userName'=>$user->name,
                    'latestThreeNums'=>$latestThreeNums
                    ];
                return \response()->json([
                    'code'=>200,
                    'status'=>true,
                    'message'=>'getting data successfully',
                    'data'=>$data
                ]);
            }elseif($ThreeNumPhoneNoEgypt=='01'){
                if($TypeNumPhoneNoEgypt=='0'){
                    $data=[
                        'typePhoneNum'=>'vodafone',
                        'userName'=>$user->name,
                        'latestThreeNums'=>$latestThreeNums
                    ];
                    return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'getting data successfully',
                        'data'=>$data
                    ]);
                }elseif($TypeNumPhoneNoEgypt=='1'){
                     $data=[
                        'typePhoneNum'=>'etisalat',
                        'userName'=>$user->name,
                        'latestThreeNums'=>$latestThreeNums
                    ];
                 return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$data
        ]);
                }elseif($TypeNumPhoneNoEgypt=='2'){
                    $data=[
                        'typePhoneNum'=>'orange',
                        'userName'=>$user->name,
                        'latestThreeNums'=>$latestThreeNums
                    ];
                    return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'getting data successfully',
                        'data'=>$data
                    ]);
                }elseif($TypeNumPhoneNoEgypt=='5'){
                     $data=[
                        'typePhoneNum'=>'we',
                        'userName'=>$user->name,
                        'latestThreeNums'=>$latestThreeNums
                    ];
                    return \response()->json([
                        'code'=>200,
                        'status'=>true,
                        'message'=>'getting data successfully',
                        'data'=>$data
                    ]);
                }elseif($TypeNumPhoneNoEgypt=='015'){
                    return 'we';
            }
        }
    }
            
            if($user->phone_no)
                return $user;
        }else{
            return \response()->json([
                'code'=>400,
                'status'=>false,
                'message'=>'this passport num not exist in the system',
                'data'=>$user
            ]);
        }
    }
    public function addingToReservation(Request $request){
        $data=$request->all();
        
      $client=  User::where(['passport_num'=>$data['passport_num']])->first();
        if(!empty($client)){
            
            if($client->id!==1){
                $reservation= new   Reservation();
                $reservation->user_id=null;//delegate reserv for a client
                ////delegate reserve for a client
                $reservation->client_id=$client->id;
                $reservation->status=1;//pending
                $reservation->save();
                return \response()->json([
                    'code'=>202,
                    'status'=>true,
                    'message'=>'this user added into waiting list for reservation',
                    'data'=>$reservation
                ]);
            }else{
                return \response()->json([
                    'code'=>406,
                    'status'=>false,
                    'message'=>'this is superadmin',
                    'data'=>null
                ]);
            }
                
        }else{
            return \response()->json([
                'code'=>400,
                'status'=>false,
                'message'=>'this passport num not exist in the system',
                'data'=>null
            ]);
        }
    }
    public function getWaitingReservationOperations(Request $request){
        $reservation=  Reservation::where(['user_id'=>null,'status'=>1])->orWhere(['user_id'=>null,'status'=>1])->with(['user','client'])->paginate($request->total);//get reservations that in pending list (allowed for reservation or not)
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$reservation
        ]);
    }
    public function allowTypeReservation(AllowTypeReservationRequest $request,$id){//type=3 -> VIP ,type=2 -> comfortable , type=1 -> seating
    //when click on it , will consider this user in  pending list (allowed to reservation)
        $data=$request->all();
        $reservation = Reservation::where(['id'=>$id])->first();
        if(!empty($reservation)){
           $reservationUser= Reservation::where(['client_id'=>$reservation->client_id])->where('status','!=',1)->count();//check if this client reserve in prevoius time any status without status=1
           if($reservationUser!==0){
               return \response()->json([
                    'code'=>407,
                    'status'=>false,
                    'message'=>'cannt reserv for his,this client exist a revervation for his in prevoius time',
                    'data'=>null
                ]);
           }
           //check in wallet this user
           if($reservation->user_id==null){//this a delegate reserve for a client
            $client=  User::where(['id'=>$reservation->client_id])->first();
               if($client->original_documentation==='0'){
                    return \response()->json([
                        'code'=>408,
                        'status'=>false,
                        'message'=>'rejected to reservation , because this user not vertification',
                        'data'=>null
                    ]);
               }
                $PointsNotAllowedUse=    PointsNotAllowedUse::where(['user_id'=>$reservation->client_id])->first();
                $UserOriginalWallet=  UserOriginalWallet::where(['user_id'=>$reservation->client_id])->first();
                if(!empty($PointsNotAllowedUse)&&!empty($UserOriginalWallet)){
                    $pointsUserFromNotAllowedNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                    $totalPointsCountUser= $pointsUserFromNotAllowedNum +  $UserOriginalWallet->allowed_points_count;
                }else{
                     if(!empty($UserOriginalWallet)){
                        $totalPointsCountUser= $UserOriginalWallet->allowed_points_count;
                     }elseif(!empty($PointsNotAllowedUse)){
                        $pointsUserFromNotAllowedNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                        $totalPointsCountUser= $pointsUserFromNotAllowedNum;
                     }else{
                         $totalPointsCountUser=0;
                     }
                
                }
              
           }else{
                $user=  User::where(['id'=>$reservation->user_id])->first();
                if($user->original_documentation==='0'){
                    return \response()->json([
                        'code'=>408,
                        'status'=>false,
                        'message'=>'rejected to reservation , because this user not vertification',
                        'data'=>null
                    ]);
                }
                $PointsNotAllowedUse=    PointsNotAllowedUse::where(['user_id'=>$reservation->user_id])->first();
                $UserOriginalWallet=  UserOriginalWallet::where(['user_id'=>$reservation->user_id])->first();

                if(!empty($PointsNotAllowedUse)&&!empty($UserOriginalWallet)){
                    $pointsUserFromNotAllowedNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                    $totalPointsCountUser= $pointsUserFromNotAllowedNum +  $UserOriginalWallet->allowed_points_count;
                }else{
                    if(!empty($UserOriginalWallet)){
                       $totalPointsCountUser= $UserOriginalWallet->allowed_points_count;
                    }elseif(!empty($PointsNotAllowedUse)){
                        $pointsUserFromNotAllowedNum = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
                        $totalPointsCountUser= $pointsUserFromNotAllowedNum;
                     }else{
                        $totalPointsCountUser=0;
                    }
                
                }
           }

       if($data['type']==1){//VIP
            $typeReservation=  TypeReservation::where('type',1)->first();
            $car = CarName::where(['id'=>$data['car_id']])->first();
           $pointsCountVip=$car->price;
           if($totalPointsCountUser>=$pointsCountVip){
               if($data['accept']){
                   if($reservation->original_status==1){//this reservation still pending
                        $operation_num = mt_rand(1000000000, 9999999999); 
                       $type=3;
                       $status=2;//in pending list (allowed to reservation)
                       $points_count=$pointsCountVip;
                        $this->updateReservation($reservation,$operation_num,$type,$status,$points_count);
                        return \response()->json([
                            'code'=>203,
                            'status'=>true,
                            'message'=>'this reservation still pending, reservation via type VIP has been successfully',
                            'data'=>$reservation
                        ]);
                   }elseif($reservation->original_status==2){
                        return \response()->json([
                            'code'=>204,
                            'status'=>true,
                            'message'=>'this reservation still pending(allowed)',
                            'data'=>$reservation
                        ]);

               }elseif($reservation->original_status==3){
                    return \response()->json([
                        'code'=>205,
                        'status'=>true,
                        'message'=>'this reservation  now is using, so cannt make reservation again ',
                        'data'=>$reservation
                    ]);
               }
                   
               }else{
                    return \response()->json([
                        'code'=>206,
                        'status'=>true,
                        'message'=>'allowed to reservation via type VIP , pls check on accept to make a reservation',
                        'data'=>$reservation
                    ]);
    
               }
           }else{
                return \response()->json([
                    'code'=>409,
                    'status'=>false,
                    'message'=>'rejected to reservation via type VIP',
                    'data'=>null
                ]);
    
           } 
    }elseif($data['type']==2){//comfortable
        $typeReservation=  TypeReservation::where('type',2)->first();
        if($totalPointsCountUser>=$typeReservation->points_count){
            if($data['accept']){
            if($reservation->original_status==1){//this reservation still pending
                    $operation_num = mt_rand(1000000000, 9999999999); 
                   $type=2;
                   $status=2;//in pending list (allowed to reservation)
                   $points_count=$typeReservation->points_count;
                    $this->updateReservation($reservation,$operation_num,$type,$status,$points_count);
                        return \response()->json([
                            'code'=>203,
                            'status'=>true,
                            'message'=>'this reservation still pending, reservation via type comfortable has been successfully',
                            'data'=>$reservation
                        ]);

               }elseif($reservation->original_status==2){
                    return \response()->json([
                        'code'=>204,
                        'status'=>true,
                        'message'=>'this reservation still pending(allowed)',
                        'data'=>$reservation
                    ]);

               }elseif($reservation->original_status==3){
                    return \response()->json([
                        'code'=>205,
                        'status'=>true,
                        'message'=>'this reservation  now is using, so cannt make reservation again ',
                        'data'=>$reservation
                    ]);
               }
         }else{
            return \response()->json([
                'code'=>206,
                'status'=>true,
                'message'=>'allowed to reservation via type comfortable , pls check on accept to make a reservation',
                'data'=>$reservation
            ]);

           }
       }else{
            return \response()->json([
                'code'=>410,
                'status'=>false,
                'message'=>'rejected to reservation via type comfortable',
                'data'=>null
            ]);
     }   
    }elseif($data['type']==3){//seating
      $typeReservation=  TypeReservation::where('type',3)->first();

     if($totalPointsCountUser>=$typeReservation->points_count){
         if($data['accept']){
            if($reservation->original_status==1){//this reservation still pending
                    $operation_num = mt_rand(1000000000, 9999999999); 
                       $type=1;
                       $status=2;//in pending list (allowed to reservation)
                       $points_count=$typeReservation->points_count;
                        $this->updateReservation($reservation,$operation_num,$type,$status,$points_count);
                        return \response()->json([
                                'code'=>203,
                                'status'=>true,
                                'message'=>'this reservation still pending, reservation via type seating has been successfull',
                                'data'=>$reservation
                            ]);

               }elseif($reservation->original_status==2){
                    return \response()->json([
                        'code'=>204,
                        'status'=>true,
                        'message'=>'this reservation still pending(allowed)',
                        'data'=>$reservation
                    ]);

               }elseif($reservation->original_status==3){
                    return \response()->json([
                        'code'=>205,
                        'status'=>true,
                        'message'=>'this reservation  now is using, so cannt make reservation again ',
                        'data'=>$reservation
                    ]);
               }
         }else{
            return \response()->json([
                'code'=>206,
                'status'=>true,
                'message'=>'allowed to reservation via type seating , pls check on accept to make a reservation',
                'data'=>$reservation
            ]);

           }
       }else{
            return \response()->json([
                'code'=>411,
                'status'=>false,
                'message'=>'rejected to reservation via type seating',
                'data'=>null
            ]);
     }   
    }
        }else{
            return \response()->json([
                'code'=>412,
                'status'=>true,
                'message'=>'this reservation id not found in system , so cannnt accpt on it',
                'data'=>null
            ]);
        }

    }
    
        public function reservationToAllUsers(){//هنا بدل اليوزر ايدي حيكون الكلينت ايدي واليوزر ايدي فاضي لانو الشخص هادا بيتم الحجز لالو من قبل المندوب فمش هو اللي بحجز بالتالي اليوزر ايدي فاضي يعني لما يكون فاضي هو ما حجز المندوب ححجزلو
        //status for all : 3 -> completed reservation operation
            $reservationToAllUsers=  Reservation::where(['status'=>2])->get();
            if($reservationToAllUsers->count()!==0){
            foreach($reservationToAllUsers as $reservationToUser){
                if($reservationToUser->user_id==1){
                    return \response()->json([
                        'code'=>411,
                        'status'=>false,
                        'message'=>'cannt reseve to this user , because this is a super admin',
                        'data'=>$reservationToUser
                    ]);
                }
              $reservationToUser  =$this->operationsInsideWallet($reservationToUser);
                if($reservationToUser==true){
                     return \response()->json([
                        'code'=>207,
                        'status'=>true,
                        'message'=>'reservations for all users has been successfully',
                        'data'=>null
                    ]);   
                }else{
                    return \response()->json([
                        'code'=>413,
                        'status'=>false,
                        'message'=>'occured something during reservation for users',
                        'data'=>null
                    ]);
                }
            }
            }else{
                return \response()->json([
                    'code'=>412,
                    'status'=>false,
                    'message'=>'not found any users in waiting to reservation for him',
                    'data'=>null
                ]);
            }



    }


   
}
