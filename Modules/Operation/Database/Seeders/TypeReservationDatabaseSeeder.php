<?php

namespace Modules\Operation\Database\Seeders;

use App\Models\TypeReservation;
use Modules\Operation\Database\Seeders\TypeReservationSeeder;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TypeReservationDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Add TypeReservation (vip,combfortable,seating) 
       TypeReservation::create([
        'type' => 1,
        'points_count' => 1000,
        'status' => 1
    ]);
    TypeReservation::create([
        'type' => 2,
        'points_count' => 500,
        'status' => 1
    ]);
    TypeReservation::create([
        'type' => 3,
        'points_count' => 100,
        'status' => 1
    ]);
    }
}
