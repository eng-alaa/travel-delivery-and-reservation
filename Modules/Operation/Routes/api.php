<?php
use Modules\Operation\Http\Controllers\API\Admin\ChargingController;
use Modules\Operation\Http\Controllers\API\Admin\ReservationController;
use Modules\Operation\Http\Controllers\API\User\ReservationController as ReservationUserController;

use Illuminate\Http\Request;
Route::middleware(['auth:api'])->namespace('API')->group(function(){
    Route::get('/get-location', [ReservationUserController::class,'getLocation']);
            Route::post('/check-at-once-reservation', [ReservationUserController::class,'checkAtOnceReservation']);
            Route::post('/add-security-code-payment', [ReservationUserController::class,'addSecurityCodePayment']);
            Route::post('/add-security-code-reservation', [ReservationUserController::class,'addSecurityCodeForReservation']);

    Route::prefix('charging')->group(function(){
        Route::post('/adding-to-charging',[ChargingController::class,'addingToCharging']);
        Route::get('/get-waiting-charging-operations', [ChargingController::class,'getWaitingChargingOperations']);
        Route::get('/charging-to-user/{id}', [ChargingController::class,'chargingToUser']);
        Route::get('/charging-to-all-users', [ChargingController::class,'chargingToAllUsers']);
          Route::get('/get-all-chargings-records-paginate', [ChargingController::class,'getAllChargingsRecords']);
                Route::get('/cancel-charging/{id}', [ChargingController::class,'cancelCharging']);
    });
    Route::prefix('reservations')->group(function(){
        Route::get('/get-data-user-to-reservation/{passport_num}',[ReservationController::class,'getDataUserToReservation']);
        Route::post('/adding-to-reservation',[ReservationController::class,'addingToReservation']);
        Route::get('/get-waiting-reservation-operations', [ReservationController::class,'getWaitingReservationOperations']);
        Route::post('/allow-type-reservation/{id}', [ReservationController::class,'allowTypeReservation']);
        Route::get('/reservation-to-all-users', [ReservationController::class,'reservationToAllUsers']);
        
        Route::get('/get-all-reservations-user', [ReservationUserController::class,'getAllReservationsUser']);
        Route::post('/add-documents', [ReservationUserController::class,'addDocuments']);
        Route::get('/get-documentations', [ReservationUserController::class,'getDocumentations']);
        

        Route::get('/get-all-reservations-records-paginate', [ReservationUserController::class,'getAllReservationsRecords']);
        Route::get('/cancel-reservation/{id}', [ReservationUserController::class,'cancelReservation']);
        
        
        Route::get('/type-reservations', [ReservationUserController::class,'typesReservations']);
        Route::post('/add-type-reservation', [ReservationUserController::class,'addTypeReservation']);
        Route::get('/get-points-count-traveler', [ReservationUserController::class,'getPointsCountTraveler']);
        Route::get('/get-types-cars', [ReservationUserController::class,'getTypesCar']);
        Route::get('/get-data-cars-via-type/{typeId}', [ReservationUserController::class,'getDataCarsViaType']);
        Route::get('/get-colors-car/{carId}', [ReservationUserController::class,'getColorsCar']);
        Route::get('/select-color-car/{carId}/{colorId}', [ReservationUserController::class,'selectColorCar']);

        
        Route::post('/add-car-to-reservation', [ReservationUserController::class,'addCarToReservation']);
        Route::get('/get-data-car-user', [ReservationUserController::class,'getDataCarUser']);
        Route::post('/add-reservation', [ReservationUserController::class,'addReservation']);
        
        Route::get('/my-reservations', [ReservationUserController::class,'myReservations']);
        Route::get('/show-reservation/{id}', [ReservationUserController::class,'showReservation']);
        
                    Route::get('/get-all-confirmed-reservations', [ReservationUserController::class,'getAllConfirmedReservations']);
            Route::get('/cancel-confirm-reservation/{id}/{jurney_num}', [ReservationUserController::class,'cancelConfirmReservation']);
        
        
    });    
        Route::prefix('jurnies')->group(function(){

            Route::post('/create-jurney/{typeJurney}', [ReservationUserController::class,'createJurney']);
            Route::get('/open-jurney/{jurneyNum}', [ReservationUserController::class,'openJurney']);
            Route::post('/add-passport-num-into-ready-traveling', [ReservationUserController::class,'addPassportNumIntoReadyTraveling']);
            Route::get('/start-jurney', [ReservationUserController::class,'startJurney']);
            Route::get('/finishing-jurney', [ReservationUserController::class,'finishingJurney']);
        });
        Route::prefix('payments')->group(function(){

            //after confirmed via driver            
            Route::post('/add-payment', [ReservationUserController::class,'addPayment']);
            Route::post('/show-for-payment/{carId}', [ReservationUserController::class,'showForPayment']);
            Route::post('/complete-payment', [ReservationUserController::class,'completePayment']);
            Route::get('/get-all-payments-record', [ReservationUserController::class,'getAllPaymentsRecord']);
            Route::get('/payments-travelers', [ReservationUserController::class,'paymentsTravelers']);

            
        });
});