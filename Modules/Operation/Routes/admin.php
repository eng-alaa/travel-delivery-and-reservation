<?php

use Illuminate\Support\Facades\Route;
// use Modules\Operation\Http\Controllers\API\Admin\ChargingController;
Route::get('ao',function(){
    dd(3);
});
Route::middleware(['auth:api'])->namespace('API')->group(function(){
    Route::prefix('charging')->group(function(){
        Route::post('/adding-to-charging', function(){
            dd(1);
        });
        Route::get('/get-waiting-charging-operations', [ChargingController::class,'getWaitingChargingOperations']);
        Route::get('/charging-to-all-users', [ChargingController::class,'chargingToAllUsers']);
    });
});