<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;

class Charging extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'operation_num',
        'points_count',
        'user_id',
        'status'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
        
        public function getStatusAttribute($value){
        if($value==1){//in pending  (in page delegate)
            return 'Pending';
        }elseif ($value==2) {//in completed  (in page delegate)
            return 'Completed';
        }elseif ($value==3) {//in pending  (in page chargingEmployee)
            return 'Pending';
        }elseif ($value==3) {//in completed  (in page chargingEmployee)
            return 'Completed';
        }
    }
        public function getOriginalStatusAttribute($value){
       return  $this->attributes['status'];
    }
}
