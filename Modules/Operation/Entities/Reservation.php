<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;


class Reservation extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id',
        'client_id',
        'driver_id',
        'points_count',
        'operation_num',
        'type',
        'name_car',
        'status',
        'confirmed',
        'paid'

    ];
    public function getPaidAttribute($value){
        if($value==0){
            return 'Not Pay';
        }elseif ($value==1) {
            return 'Paid';
        }
    }
    public function getOriginalPaidAttribute($value){
        return  $this->attributes['paid'];
     }
    public function getTypeAttribute($value){
        if($value==1){
            return 'VIP';
        }elseif ($value==2) {
            return 'comfortable';
        }elseif ($value==3) {
            return 'seating';
        }
    }
    public function getOriginalTypeAttribute($value){
       return  $this->attributes['type'];
    }
     public function getTravelAttribute($value){
        if($value==0){
            return 'go';
        }elseif ($value==1) {//back 
            return 'back';
        }
    }
    public function getOriginalTravelAttribute($value){
        return  $this->attributes['travel'];
     }
        public function getStatusAttribute($value){
        if($value==1){
            return 'Pending';
        }elseif ($value==2) {//in pending list (allowed to reservation)
            return 'Pending';
        }elseif ($value==3) {
            return 'Completed';
        }
    }
 
        public function getOriginalStatusAttribute($value){
       return  $this->attributes['status'];
    }
        public function user(){
        return $this->belongsTo(User::class);
    }
        public function client(){
        return $this->belongsTo(User::class);
    }
    public function car(){
        return $this->belongsTo(CarName::class);
    }  
}
