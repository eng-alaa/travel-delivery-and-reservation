<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Operation\Entities\CarType;
use Modules\Operation\Entities\Color;
class CarName extends Model
{
    protected $table='cars_names';
            public function user(){
        return $this->belongsTo(User::class);
    }           
    public function carType(){
        return $this->belongsTo(CarType::class);
    }
    public function colors(){
        return $this->belongsToMany(Color::class,'car_colors','car_id','color_id');
    }
}
