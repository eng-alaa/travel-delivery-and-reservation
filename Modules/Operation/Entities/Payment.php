<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Operation\Entities\Reservation;
class Payment extends Model
{
 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id',
        'operation_num',
        'user_id',
        'driver_id',
        'points_count',
        'barcode'
    ];

        public function user(){
        return $this->belongsTo(User::class);
    }
        public function driver(){
        return $this->belongsTo(User::class);
    }
    
    public function reservation(){
        return $this->belongsTo(Reservation::class);
    }
   
    
}
