<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Operation\Entities\CarName;

class Color extends Model
{

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name'
    ];
public function cars(){
        return $this->belongsToMany(CarName::class,'car_colors','color_id','car_id');
    }

}
