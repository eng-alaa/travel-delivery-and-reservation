<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documentation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'client_id'
    ];
    public function client(){
        return $this->belongsTo("Modules\Auth\Entities\User");
    }
        public function user(){
        return $this->belongsTo("Modules\Auth\Entities\User");
    }
}
