<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Operation\Entities\CarName;
class ReadyTraveling extends Model
{


    public function getStatusAttribute($value){
        if($value==1){
            return 'Pending';
        }elseif ($value==2) {//in pending list (allowed to reservation)
            return 'Pending';
        }elseif ($value==3) {
            return 'Completed';
        }
    }
    public function getOriginalStatusAttribute($value){
        return  $this->attributes['status'];
     }
        public function getTravelAttribute($value){
        if($value==0){
            return 'go';
        }elseif ($value==1) {//back 
            return 'back';
        }
    }
    public function getOriginalTravelAttribute($value){
        return  $this->attributes['travel'];
     }
   
    //          public function user(){
    //     return $this->belongsTo(User::class);
    // }
                 public function car(){
        return $this->belongsTo(CarName::class);
    }
        public function client(){
        return $this->belongsTo(User::class);
    }
       public function getConfirmedAttribute($value){
        if($value==0){
            return 'Not Confirmed';
        }elseif ($value==1) {
            return 'Confirmed';
        }
    }
    public function getOriginalConfirmedAttribute($value){
       return  $this->attributes['confirmed'];
    }
}
