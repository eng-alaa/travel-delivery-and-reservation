<?php

namespace Modules\Operation\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeReservation extends Model
{
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'type',
        'points_count',
        'status'
    ];
    public function getTypeAttribute($value){
        if($value==1){
            return 'VIP';
        }elseif ($value==2) {
            return 'comfortable';
        }elseif ($value==3) {
            return 'seating';
        }
    }
    public function getOriginalTypeAttribute($value){
       return  $this->attributes['type'];
    }
}
