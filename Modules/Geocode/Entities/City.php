<?php

namespace Modules\Geocode\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class City extends Model 
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'code',
        'country_id',
        'status'
    ];
    public $guarded = [];
    public function country(){
        return $this->belongsTo("Modules\Geocode\Entities\Country");
    }
    public function towns(){
        return $this->hasMany("Modules\Geocode\Entities\Town");
    }
    public function users(){
        return $this->hasMany("Modules\Geocode\Entities\users");
    }
}
