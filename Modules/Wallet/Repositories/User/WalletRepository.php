<?php
namespace Modules\Wallet\Repositories\User;

use App\Repositories\EloquentRepository;
use DB;
use Illuminate\Support\Str;

 class WalletRepository extends EloquentRepository implements WalletRepositoryInterface{
    //**************for methods myPoints *****************/
    public function totalMyPointsOriginalWallet($model1,$model2){//model1:UserOriginalWallet,model2:PointsNotAllowedUse

      $userId=auth()->user()->id;
      $dataUserOriginalWallet= $model1->where(['user_id'=>$userId])->get();
      if(!empty($dataUserOriginalWallet)){
          
      $allowed_points_count=0;
      $offers_points_count=0;
             $PointsNotAllowedUseCount=0;
 
      foreach($dataUserOriginalWallet as $dataUserOriginal){
          
       $allowed_points_count_number = explode('_',$dataUserOriginal->allowed_points_count, 2)[1];
         $allowed_points_count= $allowed_points_count+($allowed_points_count_number);
       $offers_points_count_number = explode('_',$dataUserOriginal->offers_points_count, 2)[1];
         $offers_points_count= $offers_points_count+($offers_points_count_number);
      }
      }
      $PointsNotAllowedUse=$model2->where(['user_id'=>$userId])->get();
      if(!empty($PointsNotAllowedUse)){
      foreach($PointsNotAllowedUse as $PointNotAllowedUse){
       $PointNotAllowedUseNum = explode('_',$PointNotAllowedUse->points_count, 2)[1];
 
       $PointsNotAllowedUseCount=$PointsNotAllowedUseCount+ $PointNotAllowedUseNum;
    }
          
      }
 
       return $allowed_points_count+$offers_points_count+$PointsNotAllowedUseCount;
    }
    public function  myReservations($model){

      $userId=auth()->user()->id; //from auth
     $reservations= $model->where(['user_id'=>$userId])->get();
      $reservationsTotal=0;
     foreach($reservations as $re){
         if(str_contains($re->reservation_points_count,'_')){
         
      $reservation_points_count_num = explode('_',$re->reservation_points_count, 2)[1];

         $reservationsTotal=$reservationsTotal+$reservation_points_count_num;
     
         }else{
             return 'you must enter reservation_points_count contains wallet_num and points count as this: 2_20';
         }
         }
     return $reservationsTotal;
    }
    public function myPayments(){

      $userId=auth()->user()->id;
    $totalPayments=0;
    //1. get rows all payments for this user
     $payments= DB::table('system_wallet_user')->where(['user_id'=>$userId])->get();
     foreach($payments as $pay){
      //2. get   return_points_count from every row  (payment)
      $totalPayments=$totalPayments+$pay->return_points_count;
     }
     return $totalPayments;
    }
  //**************for methods OrginalWallet *****************/
    public function countReservedPoints($model){

      $userId=auth()->user()->id;

      $dataPointsNotAllowedUses= $model->where(['user_id'=>$userId])->get();
       $reserved_points_count=0;

      foreach($dataPointsNotAllowedUses as $PointsNotAllowedUse){
                  $points_count_num = explode('_',$PointsNotAllowedUse->points_count, 2)[1];
         $reserved_points_count= $reserved_points_count+($points_count_num );
      }
      return  $reserved_points_count.'_R' ;
        }
    public function  countAllowedRedeemedPoints($model){
      $userId=auth()->user()->id;


      $dataAllowedRedeemedPoints=$model->where(['user_id'=>$userId])->get();

     $dataAllowedRedeemedPointsCount=0;
     foreach($dataAllowedRedeemedPoints as $pointAllowedRedeemed){
        $allowed_points_count_num = explode('_',$pointAllowedRedeemed->allowed_points_count, 2)[1];
        $dataAllowedRedeemedPointsCount=$dataAllowedRedeemedPointsCount+ $allowed_points_count_num;
   }


      return    $dataAllowedRedeemedPointsCount.'_A';
    }
    public function countOfferPoints($model){
          $userId=auth()->user()->id;

      $dataOfferPoints= $model->where(['user_id'=>$userId])->get();
      $offers_points_count=0;
      foreach($dataOfferPoints as $dataOfferPoint){
        $offers_points_count_num = explode('_',$dataOfferPoint->offers_points_count, 2)[1];
         $offers_points_count= $offers_points_count+($offers_points_count_num);
      }
       return  $offers_points_count ;
    }
    public function getAllDataOriginalWallet($model,$userId){
     $getAllDataOriginalWallet= $model->where(['user_id'=>$userId])->first();
      return $getAllDataOriginalWallet;
    }
  //**************for methods hiddenWallet *****************//
    public function reservationPoints($model){
      $userId=auth()->user()->id;
      $dataUserHiddenWallet= $model->where(['user_id'=>$userId])->get();
      $reservations=0;
      foreach($dataUserHiddenWallet as $dataUserHidden){
        if(str_contains($dataUserHidden->reservation_points_count,'_')){
      $reservation_points_count_num = explode('_',$dataUserHidden->reservation_points_count, 2)[1];
          $reservations= $reservations+ $reservation_points_count_num;
        }else{
             return 404;//'in db not store as this AR_1'
         }
      }
      return $reservations;
    }
    public function redeemedPoints($model){
      $userId=auth()->user()->id;

      $dataUserHiddenWallet= $model::where(['user_id'=>$userId])->get();
      $redeemedPoints=0;
      foreach($dataUserHiddenWallet as $dataUserHidden){
        if(str_contains($dataUserHidden->redeemed_points_count,'_')){

      $redeemed_points_count_num = explode('_',$dataUserHidden->redeemed_points_count, 2)[1];
          $redeemedPoints= $redeemedPoints+$redeemed_points_count_num ;
          
          }else{
             return 400;//'in db not store as this BR_1'
         }
      }
      return $redeemedPoints;
    }
    public function requestRedeem($model1,$model2,$request){//model1: UserOriginalWallet , model: UserHiddenWallet
    // $data=$request->validated();
      // ModelsUserHiddenWallet::create($data);
      //operation_num: random
      $data=$request->validated();
      $userId=auth()->user()->id;
      //1. get count of allowedPoints for this user to check if this total geater than points from input 
      $dataAllowedPoints=$model1->where(['user_id'=>$userId])->first();
      if(!empty($dataAllowedPoints)){
          
     $allowedPointsCount= $dataAllowedPoints->allowed_points_count;
    $allowed_points_count_numm = explode('_',$allowedPointsCount, 2)[1];

    if($allowed_points_count_numm>=$data['redeemed_points_count']){
      //2. decrease this points_count for this user in table orgnalwallet for this user in col. allowed_points_count
    $dataUserOriginalWallet=  $model1->where(['user_id'=>$userId])->first();
            $allowed_points_count_num = explode('_',$dataUserOriginalWallet->allowed_points_count, 2)[1];
$wallet_num = strtok($dataUserOriginalWallet->allowed_points_count, '_');
      $dataUserOriginalWallet->allowed_points_count=$wallet_num.'_'.($allowed_points_count_num-$data['redeemed_points_count']);
      $dataUserOriginalWallet->save();
      //3. store redeemed points  in hidden wallet
      $operation_num = mt_rand(1000000000, 9999999999); 
        $requestRedeem=  $model2::create(['user_id'=>$userId,'operation_num'=>$operation_num,'redeemed_points_count'=>'BR_'.$data['redeemed_points_count'],'reservation_points_count'=>'0_0']);
        return $requestRedeem;
      }else{
          return 404;
      }
        }else{
        return false;

    }
    }
    public function getAllRedeemRequests($model){
      $userId=auth()->user()->id;

      $getAllRedeemRequests=  $model->where(['user_id'=>$userId])->where('redeemed_points_count','!=','0_0')->get();
        return $getAllRedeemRequests;
      }    
      public function getAllRerevationRequests($model){
      $userId=auth()->user()->id;

        $getAllRerevationRequests=  $model->where(['user_id'=>$userId])->where('rerevationed_points_count','!=','0_0')->get();
          return $getAllRerevationRequests;
        }
    public function cancelRequestRedeem($model1,$model2,$id){//model1,$model2,
    //1. find this redeemRequest to take from it : redeemPoints, userId
      $dataRedeemRequest= $model1::where('id',$id)->first();
      if(isset($dataRedeemRequest)){
          if($dataRedeemRequest->original_status==-1){
              
              
          
              return 416;
          }else{
      $redeemPoints= $dataRedeemRequest->redeemed_points_count;
      if($redeemPoints!=="0_0"){
             $userId= $dataRedeemRequest->user_id;
      //2. sum these points (redeemPoints) on allowed_point_count in UserOriginalWallet
      $dataUserOriginal= $model2::where(['user_id'=>$userId])->first();
        $wallet_num = strtok($redeemPoints, '_');
      if(!empty($dataUserOriginal)){
        if(str_contains($dataUserOriginal->allowed_points_count,'_')){
        $redeemPointsNum = explode('_',$redeemPoints, 2)[1];
        $allowed_points_count_num = explode('_',$dataUserOriginal->allowed_points_count, 2)[1];
      $dataUserOriginal->allowed_points_count=$wallet_num.'_'.($allowed_points_count_num+$redeemPointsNum);
      $dataUserOriginal->save();
          //3. change status this redeemrequest in hiddenwallet
          $dataRedeemRequest->status=-1;
          $dataRedeemRequest->save();
      return true;
          
      }
        }else{
            
          $dataOriginalUserNew=new $model2();
         $dataOriginalUserNew->allowed_points_count=$redeemPoints;
         $dataOriginalUserNew->user_id=$userId;
         $dataOriginalUserNew->offers_points_count="0_0";
         $dataOriginalUserNew->wallet_num=$wallet_num;
         $dataOriginalUserNew->save();
                   //3. change status this redeemrequest in hiddenwallet
          $dataRedeemRequest->status=-1;
          $dataRedeemRequest->save();
      return true;
      
         }
      }else{
      return false;
      }
          }
      
   
      }else{
        return 404;
      }
    }
    public function requestReservation($model1,$model2,$request){//model1:UserOriginalWallet,model2: UserHiddenWallet
      $userId=auth()->user()->id;

    // ModelsUserHiddenWallet::create($data);
      //operation_num: random
      $data=$request->all();
      //1. get count of allowedPoints for this user to check if this total geater than points from input 
      $dataAllowedPoints=$model1::where(['user_id'=>$userId])->first();
      if(!empty($dataAllowedPoints)){
            
     $allowedPointsCount= $dataAllowedPoints->allowed_points_count;
         $allowed_points_count_numm = explode('_',$allowedPointsCount, 2)[1];

    if($allowed_points_count_numm>=$data['reservation_points_count']){
      //2. decrease this points_count for this user in table orgnalwallet for this user in col. allowed_points_count
    $dataUserOriginalWallet=  $model1::where(['user_id'=>$userId])->first();
            $allowed_points_count_num = explode('_',$dataUserOriginalWallet->allowed_points_count, 2)[1];
$wallet_num = strtok($dataUserOriginalWallet->allowed_points_count, '_');
      $dataUserOriginalWallet->allowed_points_count=$wallet_num.'_'.($allowed_points_count_num-$data['reservation_points_count']);
      $dataUserOriginalWallet->save();
      //3. store reservation points  in hidden wallet
      $operation_num = mt_rand(1000000000, 9999999999); 
      $requestReservation  =  $model2::create(['user_id'=>$userId,'operation_num'=>$operation_num,'reservation_points_count'=>'AR_'.$data['reservation_points_count'],'redeemed_points_count'=>'0_0']);
      return true;
        }else{
          return false;
    }
      }else{
          return 404;
      }
    }
    public function cancelRequestReservation($model1,$model2,$id){//model1:UserOriginalWallet,model2: UserHiddenWallet
   
      //1. find this reservationRequest to take from it : reservationPoints, userId
        $dataReservationRequest= $model2::where('id',$id)->first();
        if(!empty($dataReservationRequest)){
            if($dataReservationRequest->status==-1){
                
                return 415;
    
                
            }else{
                $reservationPoints= $dataReservationRequest->reservation_points_count;
                if($reservationPoints!=="0_0"){
                    if($dataReservationRequest->original_status==-1){
                        return 415;
                    }else{
                        $userId= $dataReservationRequest->user_id;
                        //2. sum these points (reservationPoints) on allowed_point_count in UserOriginalWallet
                        $dataUserOriginal= $model1::where(['user_id'=>$userId])->first();
                          $reservationPointsNum = explode('_',$reservationPoints, 2)[1];
                          $wallet_num = strtok($reservationPoints, '_');
                          $allowed_points_count_num = explode('_',$dataUserOriginal->allowed_points_count, 2)[1];
                        $dataUserOriginal->allowed_points_count=$wallet_num.'_'.($allowed_points_count_num+$reservationPointsNum);
                        $dataUserOriginal->save();
                              //3. change status this reservationrequest in hiddenwallet
                              $dataReservationRequest->status=-1;
                              $dataReservationRequest->save();
                        return true;
                    }
                }else{
                  return false;
                  
                 }
            }
        }else{
            return 404;
        }
    }

    public function lastRequestRedeem($model){
            $userId=auth()->user()->id;
    $dataUserInHidden=   $model->where('redeemed_points_count','!=','0_0')->latest()->first();//get last operation for this user in table hidden wallet
    if(empty($dataUserInHidden)){
        return 404;
    }else{
        
    return $dataUserInHidden;
    }
    }
}