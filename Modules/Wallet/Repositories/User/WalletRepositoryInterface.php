<?php
namespace Modules\Wallet\Repositories\User;

use App\Repositories\EloquentRepository;

interface WalletRepositoryInterface{
    public function totalMyPointsOriginalWallet($model1,$model2);
    public function myReservations($model);
    public function myPayments();
    public function countReservedPoints($model);
    public function countAllowedRedeemedPoints($model);
    public function countOfferPoints($model);
    public function reservationPoints($model1);
    public function redeemedPoints($model);
    public function requestRedeem($model1,$model2,$request);
    public function cancelRequestRedeem($model1,$model2,$id);
    public function requestReservation($model1,$model2,$request);
    public function cancelRequestReservation($model1,$model2,$id);
    public function lastRequestRedeem($model);
}