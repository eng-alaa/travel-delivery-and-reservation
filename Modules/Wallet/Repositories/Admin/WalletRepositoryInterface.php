<?php
namespace Modules\Wallet\Repositories\Admin;

use App\Repositories\EloquentRepository;

interface WalletRepositoryInterface{
    public function getRedeemedRequestsPaginateForEmployeeA($model,$request);
    public function getRedeemedRequestsPaginateForEmployeeB($model,$request);
    public function acceptRedeemedRequest($model,$id);
    public function deliveryMoney($model,$request,$id);
    public function getAllDelagtes($model1,$model2);
    public function getAllDrivers($model1,$model2);
    public function connectDelegate($model1,$model2,$id,$request);
    public function connectDriver($model1,$model2,$id,$request);
    public function totalCurrentPointsInSystemWallets($model);
    public function totalAllPointsInSystemWallet($model,$wallet_num);
    public function completedProccessTraveling($model1,$model2,$userId,$request);
}