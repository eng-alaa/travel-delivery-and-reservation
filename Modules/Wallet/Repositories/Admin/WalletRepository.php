<?php
namespace Modules\Wallet\Repositories\Admin;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Str;
use DB;
use Modules\Auth\Entities\User;
use App\Repositories\Auth\Sms\SmsRepository;
use App\Notifications\ChargingOperationRequest;
use Modules\Operation\Entities\Charging;
use Illuminate\Support\Facades\Storage;
 class WalletRepository extends EloquentRepository implements WalletRepositoryInterface{

    public function getRedeemedRequestsPaginateForEmployeeA($model,$request){//for Employee AcceptingRedeemedRequests
      //   $getRedeemedRequests=   $model->where('redeemed_points_count','!=',"0_0")->where('status','!=',2)->where('status','!=',1)->where('status','!=',-1)->where('status','!=',3)->with('user')->paginate($request->total);
         $getRedeemedRequests=   $model->where('redeemed_points_count','!=',"0_0")->where('status',0)->with('user')->paginate($request->total);
      //  $getRedeemedRequests=   $model->where(['reservation_points_count'=>"0"])->get();
         return $getRedeemedRequests;
    }
    
        public function getRedeemedRequestsThatReturned($model,$request){
        //for Employee AcceptingRedeemedRequests
         $getRedeemedRequests=   $model->where('redeemed_points_count','!=',"0_0")->where('status',3)->with('user')->paginate($request->total);
      //  $getRedeemedRequests=   $model->where(['reservation_points_count'=>"0"])->get();
         return $getRedeemedRequests;
    }
    
        public function getRedeemedRequestsPaginateForEmployeeB($model,$request){//for Employee DeliveringMoney
    //     $getRedeemedRequests=   $model->where('redeemed_points_count','!=',"0_0")->where('status','!=',0)->where('status','!=',-1)->with('user')->paginate($request->total);
         $getRedeemedRequests=   $model->where('redeemed_points_count','!=',"0_0")->where('status',1)->with('user')->paginate($request->total);
      //  $getRedeemedRequests=   $model->where(['reservation_points_count'=>"0"])->get();
         return $getRedeemedRequests;
    }
    public function  acceptRedeemedRequest($model,$id){

        //1. find it
        $redeemedRequest=   $model->where(['id'=>$id])->first();
        
        if(isset($redeemedRequest)){
               if($redeemedRequest->redeemed_points_count!=="0_0"){
                //check if it status:1 -> status this request is 1 , arleady accepted
                if($redeemedRequest->original_status=="0"||$redeemedRequest->original_status=="3"){
                    
                    //2. change status into 1
                    $redeemedRequest->status= 1;//accepted via employee redeemRequests
                    $redeemedRequest->save();
                   $user= User::where(['id'=>$redeemedRequest->user_id])->first();
                    //  SmsRepository::send($redeemedRequest->operation_num.'Ready to deliver Money',$user->phone_no);
    
                    return 200;
                 }elseif($redeemedRequest->original_status=="2"){//this is delivered moneny
                    return 2;
                }elseif($redeemedRequest->original_status=="1"){
                    return 1;
                }  elseif($redeemedRequest->original_status=="-1"){//this is canceled
                    return -1;
                } 
            }else{
                        return 418;//this req not redeem req
                }  
           
        }else{
            return 404;
            }
    }
    public function deliveryMoney($model,$request,$id){
        $data=$request->validated();
        //1. find it
        $deliveryMoney=   $model->where(['id'=>$id])->first();
        if(isset($deliveryMoney)){      //check if this is exist or not  
        //check if this operation_num for this id
            if($deliveryMoney->operation_num==$data['operation_num']){
                //2. check if this operation_num request finished from employeeA (status=1)
                if($deliveryMoney->redeemed_points_count!=="0_0"){
                    
                if($deliveryMoney->original_status==="1"){
                        //2. change status into 
                        $deliveryMoney->status= 2;//accepted via employee deliveryMoney
                        $deliveryMoney->save();
                        return 200;
                     }elseif($deliveryMoney->original_status==="0"){//this is request not accept on it from employee redeemRequests
                        return 0;
                    }elseif($deliveryMoney->original_status==="2"){
                        return 2;
                    }  elseif($deliveryMoney->original_status==="-1"){//this is canceled
                        return -1;
                    }   elseif($deliveryMoney->original_status==="3"){//this req come after returned it from employee b into employee a , so must waiting accept on it from employee a
                        return 3;
                    }   
                }else{
                    return 419;//this req not redeem req
                }
            }else{
                return 418; //this operation_num not for this user
            }
        }else{
        return 404;
        }
        
    }
        public function countPointsIndeliveryMoney($model){
        //for Employee DeliveringMoney
         $pointsIndeliveryMoney=   $model->where('redeemed_points_count','!=',"0_0")->where('status','!=',0)->where('status','!=',2)->where('status','!=',-1)->get();
         $pointIndeliveryMoneyCount=0;
         foreach($pointsIndeliveryMoney as $pointIndeliveryMoney){
             
            $pointIndeliveryMoney = explode('_',$pointIndeliveryMoney->redeemed_points_count, 2)[1];
            $pointIndeliveryMoneyCount=$pointIndeliveryMoneyCount+$pointIndeliveryMoney;
             
         }
         

         return $pointIndeliveryMoneyCount;
    }
    public function countRequestsIndeliveryMoney($model){
        //for Employee DeliveringMoney
         $countRequestsIndeliveryMoney=   $model->where('redeemed_points_count','!=',"0_0")->where('status','!=',0)->where('status','!=',2)->where('status','!=',-1)->count();
        return $countRequestsIndeliveryMoney;
    }
            public function countRequestsUnderReviewing($model){
        //for Employee DeliveringMoney
         $countRequestsUnderReviewing=   $model->where('redeemed_points_count','!=',"0_0")->where('status',3)->get();

         return $countRequestsUnderReviewing;
    }
    public function returnRequestIntoEmployeeA($model,$id){
//return status for this req from 2 to 3
      $request=  $model->where(['id'=>$id])->first();
      if(!empty($request)){
      if($request->original_status==="1"){//this req accepted from employee a ,but not reach to take his money to accept on it in employee b through  ex. 12 hour 
        $request->status=3;//return this req , because this user 
        $request->save();
        return 200;
      }elseif($request->original_status==="2"){//this req delivered money, so cannt return it into employee a
        return 414;
      }elseif($request->original_status==="0"){//this req still pending , still waiting from employee a
        return 415;

      }elseif($request->original_status==="3"){//this req aleady returned in employee a
          return 416;
      }elseif($request->original_status==="-1"){//this req canceled , so cannt return into employee a
          return 417;
      }
      }else{
          return 418;
      }

    }
    public function cancelRequestRedeem($model1,$model2,$id){//model1,$model2,
        //1. find this redeemRequest to take from it : redeemPoints, userId
          $dataRedeemRequest= $model1::where('id',$id)->first();
          if(!empty($dataRedeemRequest)){
              if($dataRedeemRequest->original_status==="0"||$dataRedeemRequest->original_status==="3"){
                  
          $redeemPoints= $dataRedeemRequest->redeemed_points_count;
          if($redeemPoints!=="0_0"){
                 $userId= $dataRedeemRequest->user_id;
          //2. sum these points (redeemPoints) on allowed_point_count in UserOriginalWallet
          $dataUserOriginal= $model2::where(['user_id'=>$userId])->first();
            if(str_contains($dataUserOriginal->allowed_points_count,'_')){
            $redeemPointsNum = explode('_',$redeemPoints, 2)[1];
            $wallet_num = strtok($redeemPoints, '_');
            $allowed_points_count_num = explode('_',$dataUserOriginal->allowed_points_count, 2)[1];
          $dataUserOriginal->allowed_points_count=$wallet_num.'_'.($allowed_points_count_num+$redeemPointsNum);
          $dataUserOriginal->save();
          //3. change status this redeemrequest in hiddenwallet
          $dataRedeemRequest->status=-1;
          $dataRedeemRequest->save();
          return true;
            }else{
                 return 'you must enter allowed_points_count contains wallet_num and points count as this: 2_20';
             }
          }else{
          return false;
          }
              }elseif($dataRedeemRequest->original_status=="1"){
                  return 405; //cannt cancel it , because this req now in step delivry Money
              }elseif($dataRedeemRequest->original_status=="2"){
                  return 415; //cannt cancel it , because this req delivered Money
              }elseif($dataRedeemRequest->original_status=="-1"){
                  return 416; //this req arleady canceled
              }
              
          
       
          }else{
            return 404;
          }
        }
    public function getAllDelagtes($model1,$model2){//model:role , model2 : user

        //1. find role delegate
        $roleDelegate=   $model1->where(['name'=>'delegate'])->first();
        //2. find id this role in table role_user to find get all users have this id role
        $usersIdsHasRoleDelegate=  DB::table('role_user')->where(['role_id'=>$roleDelegate->id])->get();
        $delegates=[];
            foreach($usersIdsHasRoleDelegate as $user){
            $delegate=   $model2->findOrFail($user->user_id);
            array_push($delegates,$delegate); 
            }
            return $delegates;
    }
    public function  getAllDrivers($model1,$model2){//model:role , model2 : user
        //1. find role driver
        $roleDriver=   $model1->where(['name'=>'driver'])->first();
        //2. find id this role in table role_user to find get all users have this id role
        $usersIdsHasRoleDriver=  DB::table('role_user')->where(['role_id'=>$roleDriver->id])->get();
        $drivers=[];
        foreach($usersIdsHasRoleDriver as $userId){
            $driver=   $model2->findOrFail($userId->user_id);
        array_push($drivers,$driver); 
        }
        return $drivers;
    }
    public function connectDelegate($model1,$model2,$id,$request){//model:role , model2 : user

        $data=$request->all();
        //1. search if this id is for delegate or not
        $roleIdDelegate=  DB::table('role_user')->where(['user_id'=>$id])->first()->role_id;
        $roleDelegate= $model1->findOrFail($roleIdDelegate);

            if($roleDelegate->name=='delegate'){
                //2. if passport_num is exist or not
                $user=  $model2->where(['passport_num'=>$data['passport_num']])->first();
                if(!empty($user)){
                    //3. if passport_num for this delegate or not
                    if($user->id==$id){
                        $operation_number = mt_rand(1000000000, 9999999999);
$operation_num=strval($operation_number);

                      $chargingToUser= new Charging();
                    $chargingToUser->status=3;//charging operation pending via charging employee not via delegate
                    $chargingToUser->user_id=$id;
                     $chargingToUser->operation_num=$operation_num;
                    $chargingToUser->save();
                    
                    //send notification to user
                    $user->notify(new ChargingOperationRequest($operation_num));//send a notification for this user (operation_num for charging operation)
                        return $operation_num;//start in charging, this user
                    }else{
                        return 400;//this passport_num not for this delegate
                    }

                }else{
                    return 419;//this passport num not found in system
                }

            }else{
                return 415;//this is not delegate to make connecting in charging page
            }
    }
    public function connectDriver($model1,$model2,$id,$request){//model:role , model2 : user
$operation_number = mt_rand(1000000000, 9999999999);
$operation_num=strval($operation_number);
// dd($str);
// $oper=Str::random(40);
// $oper=hexdec($operation_numero);
        $data=$request->all();
        //1. search if this id is for driver or not
        $roleIdDriver=  DB::table('role_user')->where(['user_id'=>$id])->first()->role_id;
        $roleDriver= $model1->findOrFail($roleIdDriver);

            if($roleDriver->name=='driver'){
                //2. if passport_num is exist or not
                $user=  $model2->where(['passport_num'=>$data['passport_num']])->first();
                if(!empty($user)){
                    //3. if passport_num for this driver or not
                    if($user->id==$id){
                            
                      $chargingToUser= new Charging();
                    $chargingToUser->status=3;//charging operation pending via charging employee not via delegate
                    $chargingToUser->user_id=$id;
                     $chargingToUser->operation_num=$operation_num;
                    $chargingToUser->save();
                //   $chargingToUser= Charging::insert(['status'=>3,'user_id'=>$id,'operation_num'=>$operation_num]);
                //   dd($operation_num);
                    // Storage::put('operationNum',$operation_num);
                    //send notification to user
                    $user->notify(new ChargingOperationRequest($operation_num));//send a notification for this user (operation_num for charging operation)
                        return $operation_num;//start in charging, this user
                    }else{
                        return 400;//this passport_num not for this driver
                    }

                }else{
                    return 419;//this passport num not found in system
                }

            }else{
                return 415;//this is not driver to make connecting in charging page
            }
    }

    public function searchClient($model1,$model2,$personal_id){
      $dataClient=  $model2->where(['personal_id'=>$personal_id])->first();
      if(!empty($dataClient)){
                      //1. search if this id is for user or not
            $roleIduser=  DB::table('role_user')->where(['user_id'=>$dataClient->id])->first()->role_id;
            $roleuser= $model1->findOrFail($roleIduser);
                if($roleuser->name=='traveler'){
                    return $dataClient;//search success on this client
    
                }else{
                    return 415;//this is not user to make appear in this search
                }

      }else{
          return 404;
      }
    }

    public function connectClient($model1,$model2,$id){//model1: role , model2:user

        $user=$model2->where(['id'=>$id])->first();
        if(!empty($user)){
            //1. search if this id is for user or not
            $roleIduser=  DB::table('role_user')->where(['user_id'=>$id])->first()->role_id;
            $roleuser= $model1->findOrFail($roleIduser);
                if($roleuser->name=='traveler'){
$operation_number = mt_rand(1000000000, 9999999999);
$operation_num=strval($operation_number);
                      $chargingToUser= new Charging();
                    $chargingToUser->status=3;//charging operation pending via charging employee not via delegate
                    $chargingToUser->user_id=$id;
                     $chargingToUser->operation_num=$operation_num;
                    $chargingToUser->save();
                    //send notification to user
                    $user->notify(new ChargingOperationRequest($operation_num));//send a notification for this user (operation_num for charging operation)
                return $operation_num;//start in charging, this user
    
                }else{
                    return 415;//this is not user to make connecting in charging page
                }

        }else{
            return 404;
        }
    }
    //**************for methods system wallet *****************/
    public function returnPointsIntoOriginalPoints($model){//will return points for  table system_wallet_user into system_wallets
       $returnPoints=0;
        $latestWallet=$model->latest()->first();
        $walletsSystem = DB::table('system_wallet_user')->where(['system_wallet_id'=>$latestWallet->id])->get();
        if(isset($walletsSystem)){

            foreach($walletsSystem as $walletSystem){
              $returnPoints=$returnPoints+$walletSystem->return_points_count; 
            }
            $wallet=$model->where(['id'=>$latestWallet->id])->first();
            if(!empty($wallet)){
            $wallet->users()->detach($wallet->id);
            }
            $latestWallet->original_points_count=$latestWallet->original_points_count+$returnPoints;
    $latestWallet->save();
            return $returnPoints;

        }else{

            return 0;
        }

    }
    public function totalCurrentPointsInSystemWallets($model){
        $systemWallets=$model->get();
        $original_points_count_num=0;
        $points_count_out_to_serve_num=0;
        $points_count_out_to_offer_num=0;
        foreach($systemWallets as $systemWallet){
            $original_points_count_num=$original_points_count_num+$systemWallet->original_points_count;
        }
        
       $systemWalletsUsers= DB::table('system_wallet_user')->get();
       $return_points_count=0;
       foreach($systemWalletsUsers as $systemWalletsUser){
           $return_points_count=$return_points_count+$systemWalletsUser->return_points_count;
       }
       return $original_points_count_num+$return_points_count;
    }
    public function totalAllPointsInSystemWallet($model,$wallet_num){

        $systemWallet=$model->where(['wallet_num'=>$wallet_num])->first();
        if(!empty($systemWallet)){
            $totalAllPointsInSyste1mWallets=  $systemWallet->original_points_count + $systemWallet->points_count_out_to_serve + $systemWallet->points_count_out_to_offer;
              return $totalAllPointsInSystemWallets;
        }
    }
    public function totalPointsInSystemWallet($model){
        $pointsInSystemWallet=0;
        $latestWalletSystem=$model->latest()->first();
        if(!empty($latestWalletSystem)){
 
            // $lastWalletSystemNum= $lastWalletSystem->wallet_num;
            $systemWalletsUsers= DB::table('system_wallet_user')->where(['system_wallet_id'=>$latestWalletSystem->id])->get();
            if($systemWalletsUsers){
                
            $return_points_count=0;
            foreach($systemWalletsUsers as $systemWalletUser){
                $return_points_count=$return_points_count+$systemWalletUser->return_points_count;
            }
            $points_count_out_to_serve_number = explode('_',$latestWalletSystem->points_count_out_to_serve, 2)[1];
            $points_count_out_to_offer_number = explode('_',$latestWalletSystem->points_count_out_to_offer, 2)[1];

        $pointsInSystemWallet=  $return_points_count+ $latestWalletSystem->original_points_count + $points_count_out_to_serve_number + $points_count_out_to_offer_number;
            }
            return $pointsInSystemWallet;
            
            
    }
    }
    public function charging($model1,$model2,$model3,$request,$userId){//model1: systemWallet , model2: pointsAllowedUse , model3: origginalWalletUser

        $data=$request->all();//points_count , offers or out_to_serve 
        
          $chargingOperationNumCount= Charging::where(['operation_num'=>$data['operation_num']])->count();
               if($chargingOperationNumCount!==0){
                   
                $chargingToUserCount=Charging::where(['user_id'=>$userId,'operation_num'=>$data['operation_num']])->count();
                if($chargingToUserCount!==0){//check if this user and have this operation_num is exist or not
                     //1. get last wallet in system_wallet to use it
       $lastWalletSystem= $model1->latest()->first();
       if(isset($lastWalletSystem)){
           //2. check points in system greater than points in req
           if($lastWalletSystem->original_points_count>=$data['points_count']){
            //3. this points_count will take from original_points_count in system_wallet
               $lastWalletSystem->original_points_count=$lastWalletSystem->original_points_count-($data['points_count']);
              if(isset($data['out_to_serve'])&&$data['out_to_serve']==1){
                //4. put it in points_count_out_to_serve in system_wallet
                //write this ($lastWalletSystem->wallet_num.'_') beside points_count to specific these pionts out from which wallet for system 
               $points_count_out_to_serve_number = explode('_',$lastWalletSystem->points_count_out_to_serve, 2)[1];
            //   dd($points_count_out_to_serve_number);
                $lastWalletSystem->points_count_out_to_serve=$lastWalletSystem->wallet_num.'_'.($points_count_out_to_serve_number+$data['points_count']);
                $lastWalletSystem->save();
                //5. adding these points  in points_count in table notAllowedPoints
                $operation_num= Str::random(8);
                $PointsNotAllowedUse= $model2->where(['user_id'=>$userId])->first();
                if(!empty($PointsNotAllowedUse)){
                $points_count_number = explode('_',$PointsNotAllowedUse->points_count, 2)[1];

                $PointsNotAllowedUse->points_count=$lastWalletSystem->wallet_num.'_'.($points_count_number+$data['points_count']);
                $PointsNotAllowedUse->operation_num=$operation_num;//random
                $PointsNotAllowedUse->save();
                    
                }else{
                $operation_num= Str::random(8);
                $PointsNotAllowedUse=new  $model2();
                $PointsNotAllowedUse->user_id=$userId;
                $PointsNotAllowedUse->points_count=$lastWalletSystem->wallet_num.'_'.($data['points_count']);
                $PointsNotAllowedUse->operation_num=$operation_num;//random
                $PointsNotAllowedUse->save();

                }
             

              }elseif(isset($data['out_to_offers'])&&$data['out_to_offers']==1){
                                  $operation_num= Str::random(8);

                //4. put it in points_count_out_to_offer in wallet system 
                //write this ($lastWalletSystem->wallet_num.'_') beside points_count to specific these pionts out from which wallet for system 
                 $points_count_out_to_offer_number = explode('_',$lastWalletSystem->points_count_out_to_offer, 2)[1];
                $lastWalletSystem->points_count_out_to_offer=$lastWalletSystem->wallet_num.'_'.($points_count_out_to_offer_number+$data['points_count']);
                $lastWalletSystem->save();
                //5. adding these points into UserOriginalWallet in offers_points_count
                $UserOriginalWallet= $model3->where(['user_id'=>$userId])->first();
                if(!empty($UserOriginalWallet)){
                $UserOriginalWallet->offers_points_count=$lastWalletSystem->wallet_num.'_'.($points_count_out_to_offer_number+$data['points_count']);
                $UserOriginalWallet->operation_num=$operation_num;//random
                $UserOriginalWallet->save();
                    
                }else{
                    $UserOriginalWallet=new $model3();
                    $UserOriginalWallet->user_id=$userId;
                    $UserOriginalWallet->offers_points_count=$lastWalletSystem->wallet_num.'_'.($points_count_out_to_offer_number+$data['points_count']);
                    $UserOriginalWallet->allowed_points_count="0_0";
                $UserOriginalWallet->operation_num=$operation_num;//random
                $UserOriginalWallet->save();
                }
                    
                
                                return 200;

              }else{//not out_to_serve and not out_to_offers
                  return 415;
              }
             }else{
                 return 400;//'points_count in wallet system not enough'
             }

       }else{
           return -2;//'there is not found any wallet for system'
       }
                    $chargingToUser=Charging::where(['user_id'=>$userId,'operation_num'=>$data['operation_num']])->first();
                    $chargingToUser->status=4;//charging operation completed via charging employee 
                    $chargingToUser->points_count=$data['points_count'];
                    $chargingToUser->save();
                                return 200;
                }else{
                    return 1; //this operation_num not for this user , check it
                }
                   
               }else{
                   return -1; // this operation_num not found , check it
               }
       
    }
    public function completedProccessTraveling($model1,$model2,$userId,$request){//model1: systemWallet, model2: originalWalletUser
        
        $data=$request->all();
        //1. get wallet_num from point_count for this user(req->return_points_count)
        //this $data['return_points_count'] contain : wallet_num_return_points_count
        $wallet_num = strtok($data['return_points_count'], '_');//get wallet_num (before _)
        if(str_contains($data['return_points_count'],'_')){
        $return_points_count = explode('_',$data['return_points_count'], 2)[1];//get return_points_count (after _)
        //2.get id this wallet to store this id in system_wallets_users
      $wallet=  $model1->where(['wallet_num'=>$wallet_num])->first();//id this wallet will store in table system_wallets_users 
    //   DB::table('system_wallets_users')->store(['user_id'=>$userId,'wallet_id'=>$wallet->id,'return_points_count'=>$return_points_count]);
        $system_wallet=$model1->find($wallet->id);
            $system_wallet->users()->attach($userId,['return_points_count'=>$return_points_count]);
           $UserOriginalWallet= $model2->where(['user_id'=>$userId])->first();
           if(str_contains($UserOriginalWallet->allowed_points_count,'_')){
            $allowed_points_count = explode('_',$UserOriginalWallet->allowed_points_count, 2)[1];//get return_points_count (after _)
            
           $UserOriginalWallet->allowed_points_count= $wallet_num.'_'.($allowed_points_count-$return_points_count);
           $UserOriginalWallet->save();
           }
        }else{
            return 'you must enter return_points_count contains wallet_num and points count as this: 2_20';
        }
    }
}