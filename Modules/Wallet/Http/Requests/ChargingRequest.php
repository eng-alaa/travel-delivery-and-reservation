<?php

namespace Modules\Wallet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class ChargingRequest.
 */
class ChargingRequest extends FormRequest
{
    /**
     * @var BaseRepository
    */
    protected $baseRepo;
    /**
     * StoreUserRequest constructor.
     */
    public function __construct(BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // ChargingRequest for only superadministrator and chargingEmployee   
        $authorizeRes= $this->baseRepo->authorizeSuperAndChargingEmployee();
        if($authorizeRes==true){
            return true;
        }else{
            return $this->failedAuthorization();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'out_to_serve' => ['sometimes', 'in:1,0'],
            'out_to_offers' => ['sometimes', 'in:1,0'],
            'points_count'=>['numeric'],
            'operation_num'=>['numeric'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        
        ];
    }
        /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    // protected function failedAuthorization()
    // {
    //     throw new AuthorizationException(__('Only the superadministrator can Store this user.'));
    // }
}
