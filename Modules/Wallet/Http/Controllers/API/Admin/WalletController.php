<?php

namespace Modules\Wallet\Http\Controllers\API\Admin;

use Modules\Wallet\Entities\PointsAllowedRedeemed;
use Modules\Wallet\Entities\PointsNotAllowedUse;
use Modules\Wallet\Entities\SystemWallet;
use Modules\Auth\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\UserOriginalWallet;
use App\Repositories\BaseRepository;
use DB;
use Modules\Auth\Entities\Role;
use Modules\Wallet\Entities\UserHiddenWallet;
use Illuminate\Support\Str;
use Modules\Wallet\Repositories\Admin\WalletRepository;
use Modules\Wallet\Http\Requests\ChargingRequest;
use Modules\Wallet\Http\Requests\DeliveryMoneyRequest;
use Modules\Wallet\Http\Requests\ConnectDelegateRequest;
use Modules\Wallet\Http\Requests\ConnectDriverRequest;
use Illuminate\Support\Facades\Storage;
class WalletController extends Controller
{
     /**
     * @var BaseRepository
     */
    protected $baseRepo;
    /**
     * @var WalletRepository
     */
    protected $walletRepo;
        /**
     * UsersController constructor.
     *
     * @param WalletRepository $walletRepo
     */
    public function __construct(BaseRepository $baseRepo, UserHiddenWallet $hiddenWallet,UserOriginalWallet $originalWallet,SystemWallet $systemWallet, Role $role , User $user,WalletRepository $walletRepo,PointsNotAllowedUse $pointsNotAllowedUse)
    {
        //for redeemRequestsEmployee
         $this->middleware(['permission:redeemRequests_read'])->only('getRedeemedRequestsPaginateForEmployeeA');
         $this->middleware(['permission:redeemRequests_update'])->only('acceptRedeemedRequest');         
         $this->middleware(['permission:countRequestsUnderReviewing_read'])->only('countRequestsUnderReviewing');         
         $this->middleware(['permission:requestRedeem_cancel'])->only('cancelRequestRedeem');         
         
        //for deliveryMoneyEmployee
         $this->middleware(['permission:deliveryMoney_read'])->only('getRedeemedRequestsPaginateForEmployeeB');
         $this->middleware(['permission:deliveryMoney_update'])->only('deliveryMoney');
         $this->middleware(['permission:countPointsIndeliveryMoney_read'])->only('countPointsIndeliveryMoney');
         $this->middleware(['permission:countRequestsIndeliveryMoney_read'])->only('countRequestsIndeliveryMoney');
         $this->middleware(['permission:returnRequestIntoEmployeeA_update'])->only('returnRequestIntoEmployeeA');
        
         //for delegates
         $this->middleware(['permission:getAllDelagtes_read'])->only('getAllDelagtes');
         $this->middleware(['permission:getAllDrivers_read'])->only('getAllDrivers');
         $this->middleware(['permission:connectDelegate_update'])->only('connectDelegate');
         $this->middleware(['permission:connectDriver_update'])->only('connectDriver');
         $this->middleware(['permission:searchClient_read'])->only('searchClient');
         $this->middleware(['permission:connectClient_update'])->only('connectClient');
         $this->middleware(['permission:returnPointsIntoOriginalPoints_update'])->only('returnPointsIntoOriginalPoints');
         $this->middleware(['permission:totalCurrentPointsInSystemWallets_read'])->only('totalCurrentPointsInSystemWallets');
         $this->middleware(['permission:totalAllPointsInSystemWallet_read'])->only('totalAllPointsInSystemWallet');
         $this->middleware(['permission:totalPointsInSystemWallet_read'])->only('totalPointsInSystemWallet');
        //  $this->middleware(['permission:charging_update'])->only('charging');
         $this->middleware(['permission:completedProccessTraveling_update'])->only('completedProccessTraveling');

        // $this->middleware(['permission:users_restore'])->only('restore');
        // $this->middleware(['permission:users_restore-all'])->only('restore-all');
        // $this->middleware(['permission:users_show'])->only('show');
        // $this->middleware(['permission:users_store'])->only('store');
        // $this->middleware(['permission:users_update'])->only('update');
        // $this->middleware(['permission:users_destroy'])->only('destroy');
        // $this->middleware(['permission:users_destroy-force'])->only('destroy-force');
        $this->baseRepo = $baseRepo;
        $this->hiddenWallet = $hiddenWallet;
        $this->originalWallet = $originalWallet;
        $this->systemWallet = $systemWallet;
        $this->pointsNotAllowedUse = $pointsNotAllowedUse;
        $this->user = $user;
        $this->role = $role;
        $this->walletRepo = $walletRepo;
    }

    public function getRedeemedRequestsPaginateForEmployeeA(Request $request){//getRedeemedRequests: from table hiddenWallet for all users 
        $getRedeemedRequests=$this->walletRepo->getRedeemedRequestsPaginateForEmployeeA($this->hiddenWallet,$request);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$getRedeemedRequests
        ]);

    }
    public function getRedeemedRequestsThatReturned(Request $request){
     $getRedeemedRequests=$this->walletRepo->getRedeemedRequestsThatReturned($this->hiddenWallet,$request);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$getRedeemedRequests
        ]);   
    }
        public function getRedeemedRequestsPaginateForEmployeeB(Request $request){//getRedeemedRequests: from table hiddenWallet for all users 
        $getRedeemedRequests=$this->walletRepo->getRedeemedRequestsPaginateForEmployeeB($this->hiddenWallet,$request);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$getRedeemedRequests
        ]);

    }
    public function acceptRedeemedRequest($id){//find this RedeemedRequestand change status it into 1 accepted
        $acceptRedeemedRequest=$this->walletRepo->acceptRedeemedRequest($this->hiddenWallet,$id);
        if($acceptRedeemedRequest==200){
            return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'Redeemed Request has been accepted successfully',
            'data'=>$acceptRedeemedRequest
        ]);
        }elseif($acceptRedeemedRequest==2){
            return \response()->json([
            'code'=>419,
            'status'=>true,
            'message'=>'this is delivered moneny , so status is = 2',
            'data'=>$acceptRedeemedRequest
        ]);
        }elseif($acceptRedeemedRequest==1){
             return \response()->json([
            'code'=>402,
            'status'=>false,
            'message'=>'status this request is 1 , arleady accepted ,so status is = 1',
            'data'=>$acceptRedeemedRequest
        ]);
            
        }elseif($acceptRedeemedRequest=="-1"){
             return \response()->json([
            'code'=>406,
            'status'=>false,
            'message'=>'status this request is -1 , this is canceled',
            'data'=>$acceptRedeemedRequest
        ]);
            
        }elseif($acceptRedeemedRequest==415){
            return \response()->json([
            'code'=>415,
            'status'=>true,
            'message'=>'this passport_num not for this user',
            'data'=>$acceptRedeemedRequest
        ]);
        }elseif($acceptRedeemedRequest==418){
            return \response()->json([
            'code'=>418,
            'status'=>false,
            'message'=>'this req not redeem req',
            'data'=>$acceptRedeemedRequest
        ]);
        }elseif($acceptRedeemedRequest==404){
            return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'this req id not found',
            'data'=>$acceptRedeemedRequest
        ]);
        }
        
       
    }
    public function deliveryMoney($id,DeliveryMoneyRequest $request){//find this RedeemedRequestand change status it into 1 accepted
        $deliveryMoney=$this->walletRepo->deliveryMoney($this->hiddenWallet,$request,$id);

        if($deliveryMoney===200){
            return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'operation delivery Money has been completed successfully',
            'data'=>$deliveryMoney
        ]);
        }
        
 
        if($deliveryMoney===0){
            return \response()->json([
            'code'=>419,
            'status'=>false,
            'message'=>'cannt delivery Money for this user because , this user not accepted on  his RedeemedRequest from emplyee A , status 0',
            'data'=>$deliveryMoney
        ]);
        }elseif($deliveryMoney===2){
             return \response()->json([
            'code'=>415,
            'status'=>false,
            'message'=>'status this request is 2 , arleady delivered ,so status is = 2',
            'data'=>$deliveryMoney
        ]);
            
        }elseif($deliveryMoney===-1){
            return \response()->json([
           'code'=>416,
           'status'=>false,
           'message'=>'status this request is -1 , this is canceled',
           'data'=>$deliveryMoney
       ]);
           
       }elseif($deliveryMoney===3){
        return \response()->json([
       'code'=>417,
       'status'=>false,
       'message'=>'this req come after returned it from employee b into employee a , so must waiting accept on it from employee a',
       'data'=>$deliveryMoney
   ]);
       
   }elseif($deliveryMoney===418){
    return \response()->json([
   'code'=>418,
   'status'=>false,
   'message'=>'this operation_num not for this user',
   'data'=>$deliveryMoney
]);
   
}elseif($deliveryMoney===419){
    return \response()->json([
   'code'=>419,
   'status'=>false,
   'message'=>'this req not redeem req',
   'data'=>$deliveryMoney
]);
   
}elseif($deliveryMoney==404){
                         return \response()->json([
            'code'=>404,
            'status'=>false,
            'message'=>'this deliveredMoneyRequest not found',
            'data'=>$deliveryMoney
        ]);
        }
    }
     public function countPointsIndeliveryMoney(){
    $countPointsIndeliveryMoney=$this->walletRepo->countPointsIndeliveryMoney($this->hiddenWallet);

        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'countPointsIndeliveryMoney',
            'data'=>$countPointsIndeliveryMoney
        ]);
    }    
    public function countRequestsIndeliveryMoney(){
        $countRequestsIndeliveryMoney=$this->walletRepo->countRequestsIndeliveryMoney($this->hiddenWallet);

        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'countRequestsIndeliveryMoney',
            'data'=>$countRequestsIndeliveryMoney
        ]);
    }
    public function returnRequestIntoEmployeeA($id){
        $returnRequestIntoEmployeeA=$this->walletRepo->returnRequestIntoEmployeeA($this->hiddenWallet,$id);
        if($returnRequestIntoEmployeeA===200){
            return \response()->json([
                'code'=>201,
                'status'=>true,
                'message'=>'returnRequestIntoEmployeeA',
                'data'=>$returnRequestIntoEmployeeA
            ]);

        }elseif($returnRequestIntoEmployeeA===414){
            return \response()->json([
                'code'=>414,
                'status'=>false,
                'message'=>'this req delivered money, so cannt return it into employee a',
                'data'=>$returnRequestIntoEmployeeA
            ]);
        }elseif($returnRequestIntoEmployeeA===415){
            return \response()->json([
                'code'=>415,
                'status'=>false,
                'message'=>'this req still pending , still waiting from employee a',
                'data'=>$returnRequestIntoEmployeeA
            ]);
        }elseif($returnRequestIntoEmployeeA===416){
            return \response()->json([
                'code'=>416,
                'status'=>false,
                'message'=>'this req aleady returned in employee a',
                'data'=>$returnRequestIntoEmployeeA
            ]);
        }elseif($returnRequestIntoEmployeeA===417){
            return \response()->json([
                'code'=>416,
                'status'=>false,
                'message'=>'this req canceled , so cannt return into employee a',
                'data'=>$returnRequestIntoEmployeeA
            ]);
        }elseif($returnRequestIntoEmployeeA===418){
            return \response()->json([
                'code'=>418,
                'status'=>false,
                'message'=>'not found this id req',
                'data'=>$returnRequestIntoEmployeeA
            ]);
        }
    }
    public function countRequestsUnderReviewing(){
    $countRequestsUnderReviewing=$this->walletRepo->countPointsIndeliveryMoney($this->hiddenWallet);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'countRequestsUnderReviewing',
            'data'=>$countRequestsUnderReviewing
        ]);
    }
    
    public function cancelRequestRedeem($id){//return these points from         hidden wallet table into originalwallet for this user with his ope , after that remove this reemdem req from hiddenwallet
        $cancelRequestRedeem=$this->walletRepo->cancelRequestRedeem($this->hiddenWallet,$this->originalWallet,$id);
        if($cancelRequestRedeem===true){
          return \response()->json([
              'code'=>201,
              'status'=>true,
              'message'=>'cancelRequestRedeem successfully',
              'data'=>$cancelRequestRedeem
          ]);  
    
        }elseif($cancelRequestRedeem===false){
                  return \response()->json([
              'code'=>400,
              'status'=>true,
              'message'=>'this request not redeemrequest to cancel it , but it is reservationrequest',
              'data'=>$cancelRequestRedeem
          ]); 
        }elseif($cancelRequestRedeem===404){
             return \response()->json([
              'code'=>400,
              'status'=>true,
              'message'=>'this request redeem not found in system',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===405){
             return \response()->json([
              'code'=>405,
              'status'=>true,
              'message'=>'cannt cancel it , because this req now in step delivry Money',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===415){
             return \response()->json([
              'code'=>415,
              'status'=>true,
              'message'=>'cannt cancel it , because this req delivered Money',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===416){
             return \response()->json([
              'code'=>416,
              'status'=>true,
              'message'=>'this req arleady canceled',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }
    }
    public function getAllDelagtes(Request $request){//get all users from table users that have role : delegate
        $getAllDelagtes=$this->walletRepo->getAllDelagtes($this->role,$this->user,$request);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$getAllDelagtes
        ]);
    }
    public function getAllDrivers(){//get all users from table users that have role : driver
        $getAllDrivers=$this->walletRepo->getAllDrivers($this->role,$this->user);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$getAllDrivers
        ]);
        
       }

    public function connectDelegate($id,ConnectDelegateRequest $request){//take this  delgate to connect with page charging specail for his
        $connectDelegate=$this->walletRepo->connectDelegate($this->role,$this->user,$id,$request);

        if($connectDelegate==400){
            return \response()->json([
            'code'=>400,
            'status'=>true,
            'message'=>'this passport_num not for this delegate',
            'data'=>$connectDelegate
        ]);
        }elseif($connectDelegate==419){
            return \response()->json([
            'code'=>419,
            'status'=>true,
            'message'=>'this passport num not found in system',
            'data'=>$connectDelegate
        ]);
        }elseif($connectDelegate==415){
            return \response()->json([
            'code'=>415,
            'status'=>true,
            'message'=>'this is not delegate to make connecting in charging page',
            'data'=>$connectDelegate
        ]);
        }else{
              return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'start in charging, this user',
            'data'=>$connectDelegate
        ]);
        }

    }
public function connectDriver($id,ConnectDriverRequest $request){//take this  delgate to connect with page charging specail for his
        $connectDriver=$this->walletRepo->connectDriver($this->role,$this->user,$id,$request);
        if($connectDriver==400){
            return \response()->json([
            'code'=>400,
            'status'=>true,
            'message'=>'this passport_num not for this driver',
            'data'=>$connectDriver
        ]);
        }elseif($connectDriver==419){
            return \response()->json([
            'code'=>419,
            'status'=>true,
            'message'=>'this passport num not found in system',
            'data'=>$connectDriver
        ]);
        }elseif($connectDriver==415){
            return \response()->json([
            'code'=>415,
            'status'=>true,
            'message'=>'this is not driver to make connecting in charging page',
            'data'=>$connectDriver
        ]);
        }else{
              return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'start in charging, this user',
            'data'=>$connectDriver
        ]);
        }

    }
    
    //**************for methods system wallet *****************/
    public function returnPointsIntoOriginalPoints(){
        $returnPointsIntoOriginalPoints=$this->walletRepo->returnPointsIntoOriginalPoints($this->systemWallet);
        if($returnPointsIntoOriginalPoints==0){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'Not Found (For latest wallet) Any Return Points to move it into SystemWallet',
                'data'=>$returnPointsIntoOriginalPoints
            ]);
        }else{
            
        return \response()->json([
            'code'=>201,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$returnPointsIntoOriginalPoints
        ]);
        }
        
    }
    public function totalCurrentPointsInSystemWallets(){//original_points_count , return_points_count from all wallets system
        $totalCurrentPointsInSystemWallets=$this->walletRepo->totalCurrentPointsInSystemWallets($this->systemWallet);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$totalCurrentPointsInSystemWallets
        ]);

    }
    public function totalAllPointsInSystemWallet($wallet_num){//current and prevoius
        $totalAllPointsInSystemWallets=$this->walletRepo->totalAllPointsInSystemWallets($this->systemWallet,$wallet_num);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$totalAllPointsInSystemWallets
        ]);
    }
    public function totalPointsInSystemWallet(){
        $totalPointsInSystemWallet=$this->walletRepo->totalPointsInSystemWallet($this->systemWallet);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'getting data successfully',
            'data'=>$totalPointsInSystemWallet
        ]); 
    }
   public function charging(Request $request,$userId){
    $charging=$this->walletRepo->charging($this->systemWallet,$this->pointsNotAllowedUse,$this->originalWallet,$request,$userId);
    if($charging==200){
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'charging operation has been successfully',
        'data'=>null
    ]);
        
    
    }elseif($charging==415){
        return \response()->json([
        'code'=>415,
        'status'=>true,
        'message'=>'you must pass in req: out_to_serve =1 or out_to_offers=1',
        'data'=>null
    ]);
    }elseif($charging==400){
        return \response()->json([
        'code'=>400,
        'status'=>true,
        'message'=>'points_count in wallet system not enough',
        'data'=>null
    ]);
    }elseif($charging==404){
                return \response()->json([
        'code'=>400,
        'status'=>true,
        'message'=>'there is not found any wallet for system',
        'data'=>null
    ]);
    }elseif($charging==-1){
                return \response()->json([
        'code'=>402,
        'status'=>true,
        'message'=>'this operation_num not found , check it',
        'data'=>null
    ]);
    }elseif($charging==-2){
                return \response()->json([
        'code'=>406,
        'status'=>true,
        'message'=>'this operation_num not found, check it',
        'data'=>null
    ]);
    }elseif($charging==1){
                return \response()->json([
        'code'=>407,
        'status'=>true,
        'message'=>'this operation_num not for this user , check it',
        'data'=>null
    ]);
    }
    }
    public function completedProccessTraveling(Request $request,$userId){//this route : when driver finished from this user , will say to deleage to do these process ,
        // return all points into systemwallet for userId (finished from service) : store in system_wallets_users -> userId, wallet_num , return_points_count, 
        //descrease  these points from originalwallet in col. allowed_points_count 
        $completedProccessTraveling=$this->walletRepo->completedProccessTraveling($this->systemWallet,$this->originalWallet,$userId,$request);
        return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'Traveling operation has been successfully',
            'data'=>$completedProccessTraveling
        ]);
    }

    
    public function searchClient($personal_id){
        $searchClient=$this->walletRepo->searchClient($this->role,$this->user,$personal_id);
        // return \response()->json([
        //         'code'=>201,
        //         'status'=>true,
        //         'message'=>'getting data client successfully',
        //         'data'=>$searchClient
        //     ]);
        if(gettype($searchClient)=="object"){
            return \response()->json([
                'code'=>201,
                'status'=>true,
                'message'=>'getting data client successfully',
                'data'=>$searchClient
            ]);
        }elseif($searchClient==415){
            return \response()->json([
                'code'=>415,
                'status'=>false,
                'message'=>'this is not user to make appear in this search',
                'data'=>$searchClient
            ]);
        }elseif($searchClient==404){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'Not Found this personal_id in system',
                'data'=>$searchClient
            ]);
        }
    }   
     public function connectClient($id){
        $connectClient=$this->walletRepo->connectClient($this->role,$this->user,$id);

        if($connectClient==415){
            return \response()->json([
                'code'=>415,
                'status'=>false,
                'message'=>'this is not user to make connecting in charging page',
                'data'=>$connectClient
            ]);
        }elseif($connectClient==404){
            return \response()->json([
                'code'=>404,
                'status'=>false,
                'message'=>'Not Found this id in system',
                'data'=>$connectClient
            ]);
        }else{
                        return \response()->json([
                'code'=>200,
                'status'=>true,
                'message'=>'client has been connected with page charging',
                'data'=>$connectClient
            ]);
        }
        
    }

    





}
