<?php

namespace Modules\Wallet\Http\Controllers\API\User;

use Modules\Wallet\Entities\PointsNotAllowedUse;
use Modules\Wallet\Entities\SystemWallet;
use Modules\Wallet\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\UserOriginalWallet;
use App\Repositories\BaseRepository;
use DB;
use Modules\Auth\Entities\Role;
use Modules\Wallet\Entities\UserHiddenWallet;
use Illuminate\Support\Str;
use Modules\Wallet\Repositories\User\WalletRepository;
use Modules\Wallet\Http\Requests\ReservationRequest;
use Modules\Wallet\Http\Requests\RedeemRequest;
class WalletController extends Controller
{   
       /**
     * @var BaseRepository
     */
    protected $baseRepo;
    /**
     * @var WalletRepository
     */
    protected $walletRepo;
        /**
     * UsersController constructor.
     *
     * @param WalletRepository $walletRepo
     */
    public function __construct(BaseRepository $baseRepo, UserHiddenWallet $hiddenWallet,SystemWallet $systemWallet,UserOriginalWallet $originalWallet,PointsNotAllowedUse $pointsNotAllowedUse,WalletRepository $walletRepo)
    {
      $this->middleware(['permission:totalMyPointsOriginalWallet_read'])->only('totalMyPointsOriginalWallet');
      $this->middleware(['permission:myReservations_read'])->only('myReservations');
      $this->middleware(['permission:myPayments_read'])->only('myPayments');
      $this->middleware(['permission:countReservedPoints_read'])->only('countReservedPoints');
      $this->middleware(['permission:countAllowedRedeemedPoints_read'])->only('countAllowedRedeemedPoints');
      $this->middleware(['permission:countOfferPoints_read'])->only('countOfferPoints');
      $this->middleware(['permission:getAllDataOriginalWallet_read'])->only('getAllDataOriginalWallet');
      $this->middleware(['permission:reservationPoints_read'])->only('reservationPoints');
      $this->middleware(['permission:redeemedPoints_read'])->only('redeemedPoints');
    //   $this->middleware(['permission:requestRedeem_update'])->only('requestRedeem');
    //  $this->middleware(['permission:cancelRequestRedeem_update'])->only('cancelRequestRedeem');
      $this->middleware(['permission:requestReservation_update'])->only('requestReservation');
      $this->middleware(['permission:getAllRedeemRequests_read'])->only('getAllRedeemRequests');
      $this->middleware(['permission:getAllReservationRequests_read'])->only('getAllReservationRequests');
      $this->middleware(['permission:cancelRequestReservation_update'])->only('cancelRequestReservation');
      $this->middleware(['permission:lastRequestRedeem_read'])->only('lastRequestRedeem');

        // $this->middleware(['permission:users_read'])->only('index');
        // $this->middleware(['permission:users_trash'])->only('trash');
        // $this->middleware(['permission:users_restore'])->only('restore');
        // $this->middleware(['permission:users_restore-all'])->only('restore-all');
        // $this->middleware(['permission:users_show'])->only('show');
        // $this->middleware(['permission:users_store'])->only('store');
        // $this->middleware(['permission:users_update'])->only('update');
        // $this->middleware(['permission:users_destroy'])->only('destroy');
        // $this->middleware(['permission:users_destroy-force'])->only('destroy-force');
        $this->baseRepo = $baseRepo;
        $this->hiddenWallet = $hiddenWallet;
        $this->systemWallet = $systemWallet;
        $this->originalWallet = $originalWallet;
        $this->pointsNotAllowedUse = $pointsNotAllowedUse;
        $this->walletRepo = $walletRepo;
    }
    

  //**************for methods myPoints *****************/
  //   1. totalPointsOriginalWallet 2. myReservations 3. myPayments
  ///OrginalWallet////
  public function allDataWalletUser(){//allowed_points_count+offers_points_count+points_count in table points_not_allowed_uses
    $totalMyPointsOriginalWallet=$this->totalMyPointsOriginalWallet();
    $myReservations=$this->myReservations();
    $myPayments=$this->myPayments();
    $countReservedPoints=$this->countReservedPoints();
    $countAllowedRedeemedPoints=$this->countAllowedRedeemedPoints();
    $countOfferPoints=$this->countOfferPoints();
    $reservationPoints=$this->reservationPoints();
    $redeemedPoints=$this->redeemedPoints();
    $lastRequestRedeem=$this->lastRequestRedeem();
    $getAllRedeemRequests=$this->getAllRedeemRequests();
    $data=[
        'totalMyPointsOriginalWallet'=>$totalMyPointsOriginalWallet,
        'myReservations'=>$myReservations,
        'myPayments'=>$myPayments,
        'countReservedPoints'=>$countReservedPoints,
        'countAllowedRedeemedPoints'=>$countAllowedRedeemedPoints,
        'countOfferPoints'=>$countOfferPoints,
        'reservationPoints'=>$reservationPoints,
        'redeemedPoints'=>$redeemedPoints,
        'lastRequestRedeem'=>$lastRequestRedeem,
        'getAllRedeemRequests'=>$getAllRedeemRequests
        ];

return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'allDataWalletUser',
        'data'=>$data
    ]);
  }
  public function totalMyPointsOriginalWallet(){//allowed_points_count+offers_points_count+points_count in table points_not_allowed_uses
    
    $totalMyPointsOriginalWallet=$this->walletRepo->totalMyPointsOriginalWallet($this->originalWallet,$this->pointsNotAllowedUse);
    return $totalMyPointsOriginalWallet;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'totalMyPointsOriginalWallet',
        'data'=>$totalMyPointsOriginalWallet
    ]);
  }
  
  ///HiddenWallet////
  public function myReservations(){
    $myReservations=$this->walletRepo->myReservations($this->hiddenWallet);
    return $myReservations;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'myReservations',
        'data'=>$myReservations
    ]);
  }
  ///SystemWallet////
  public function myPayments(){//get all payments for user auth in system wallet
    $myPayments=$this->walletRepo->myPayments($this->systemWallet);
    return $myPayments;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'myPayments',
        'data'=>$myPayments
    ]);
  }
  //**************for methods OrginalWallet *****************/
  public function countReservedPoints(){//points_not_allowed_uses
    $countReseverdPoints=$this->walletRepo->countReservedPoints($this->pointsNotAllowedUse);
    return $countReseverdPoints;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'reserved_points_count',
        'data'=>$countReseverdPoints
    ]);
       

  }
  public function countAllowedRedeemedPoints(){
    $countAllowedRedeemedPoints=$this->walletRepo->countAllowedRedeemedPoints($this->originalWallet);
    return $countAllowedRedeemedPoints;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'AllowedRedeemedPoints',
        'data'=>$countAllowedRedeemedPoints
    ]);
  }
  public function countOfferPoints(){
    $countOfferPoints=$this->walletRepo->countOfferPoints($this->originalWallet);
    return $countOfferPoints;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'getting data successfully',
        'data'=>$countOfferPoints
    ]);
  }
  public function getAllDataOriginalWallet(){
   $userId=auth()->user();
    $getAllDataOriginalWallet=$this->walletRepo->getAllDataOriginalWallet($this->originalWallet,$userId);
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'getting data successfully',
        'data'=>$getAllDataOriginalWallet
    ]);
  }
  //**************for methods hiddenWallet *****************//
  public function reservationPoints(){
   $userId=auth()->user();
    $reservationPoints=$this->walletRepo->reservationPoints($this->hiddenWallet,$userId);
    if($reservationPoints===404){
        return 0;
        return \response()->json([
            'code'=>404,
            'status'=>true,
            'message'=>'in db not store as this AR_1',
            'data'=>$reservationPoints
        ]);
       
    }else{
        return $reservationPoints;
         return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'reservationPoints',
            'data'=>$reservationPoints
        ]);
    }
    
    
  }
  public function redeemedPoints(){
   $userId=auth()->user();
    $redeemPoints=$this->walletRepo->redeemedPoints($this->hiddenWallet,$userId);
    if($redeemPoints==400){
        return 0;
    return \response()->json([
        'code'=>400,
        'status'=>false,
        'message'=>'in db not store as this BR_1',
        'data'=>$redeemPoints
    ]);
        
    }else{
        return $redeemPoints;
            return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'getting data successfully',
        'data'=>$redeemPoints
    ]);
    }
  }

    //******** */
  public function requestRedeem(RedeemRequest $request){//move  from original wallet(allowed_points_count) for this user into this hidden wallet in col. redeemed_points_count
//   $userId=auth()->user();
    $requestRedeem=$this->walletRepo->requestRedeem($this->originalWallet,$this->hiddenWallet,$request);
    if($requestRedeem==false){
        return 'you cannt make requestRedeem , because you havennt enough points in your wallet';
      
    }elseif($requestRedeem==404){
        return  \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'there is not found any points for this user to requestRedeem',
          'data'=>$requestRedeem
      ]);  
    }else{
      return \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'redeemedPoints has been successfully',
          //'data'=>$requestRedeem->user()
          'data'=>$requestRedeem
      ]);
    }
  }

  public function cancelRequestRedeem($id){//return these points from         hidden wallet table into originalwallet for this user with his ope , after that remove this reemdem req from hiddenwallet
    $cancelRequestRedeem=$this->walletRepo->cancelRequestRedeem($this->hiddenWallet,$this->originalWallet,$id);
        if($cancelRequestRedeem===true){
          return \response()->json([
              'code'=>201,
              'status'=>true,
              'message'=>'cancelRequestRedeem successfully',
              'data'=>$cancelRequestRedeem
          ]);  
    
        }elseif($cancelRequestRedeem===false){
                  return \response()->json([
              'code'=>400,
              'status'=>true,
              'message'=>'this request not redeemrequest to cancel it , but it is reservationrequest',
              'data'=>$cancelRequestRedeem
          ]); 
        }elseif($cancelRequestRedeem===404){
             return \response()->json([
              'code'=>400,
              'status'=>true,
              'message'=>'this request redeem not found in system',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===405){
             return \response()->json([
              'code'=>405,
              'status'=>true,
              'message'=>'cannt cancel it , because this req now in step delivry Money',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===415){
             return \response()->json([
              'code'=>415,
              'status'=>true,
              'message'=>'cannt cancel it , because this req delivered Money',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }elseif($cancelRequestRedeem===416){
             return \response()->json([
              'code'=>416,
              'status'=>true,
              'message'=>'this req arleady canceled',
              'data'=>$cancelRequestRedeem
          ]); 
    
        }
  }



  public function requestReservation(ReservationRequest $request){//move  from         // $data=$request->validated();
    $userId=auth()->user();
    $requestReservation=$this->walletRepo->requestReservation($this->originalWallet,$this->hiddenWallet,$request,$userId);
    if($requestReservation===404){
              return \response()->json([
          'code'=>404,
          'status'=>true,
          'message'=>'this user havent any points allowing to use it',
          'data'=>$requestReservation
      ]); 
    }elseif($requestReservation===false){
         return \response()->json([
          'code'=>400,
          'status'=>true,
          'message'=>'you cannt make requestRedeem , because you havennt enough points in your wallet',
          'data'=>$requestReservation
      ]); 

    }elseif($requestReservation===true){

      return \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'requestReservation has been  successfully',
          'data'=>$requestReservation
      ]);   
    }
}

  public function getAllRedeemRequests(){
   $userId=auth()->user();
    $getAllRedeemRequests=$this->walletRepo->getAllRedeemRequests($this->hiddenWallet,$userId);
    
    if($getAllRedeemRequests==true){
        return $getAllRedeemRequests;
      return \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'getting data successfully',
          'data'=>$getAllRedeemRequests
      ]);  
  }
  }

  public function getAllReservationRequests(){
   $userId=auth()->user();
    $getAllReservationRequests=$this->walletRepo->getAllReservationRequests($this->hiddenWallet,$userId);
    if($getAllReservationRequests==true){
        return $getAllReservationRequests;
      return \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'getting data successfully',
          'data'=>$getAllReservationRequests
      ]);  
  }
  }

  public function cancelRequestReservation($id){//return these points from hidden wallet table into originalwallet for this user with his ope , after that remove this reemdem req from hiddenwallet
    $cancelRequestReservation=$this->walletRepo->cancelRequestReservation($this->originalWallet,$this->hiddenWallet,$id);
    if($cancelRequestReservation===true){
      return \response()->json([
          'code'=>200,
          'status'=>true,
          'message'=>'RequestReservation has been canceled',
          'data'=>$cancelRequestReservation
      ]);   

    }elseif($cancelRequestReservation===false){
         return \response()->json([
          'code'=>400,
          'status'=>false,
          'message'=>'this request not reservationrequest to cancel it , but it is redeemrequest',
          'data'=>$cancelRequestReservation
      ]);   

    }elseif($cancelRequestReservation===404){
         return \response()->json([
          'code'=>404,
          'status'=>false,
          'message'=>'this request reservation not found in system',
          'data'=>$cancelRequestReservation
      ]);   

    }elseif($cancelRequestReservation===415){
         return \response()->json([
          'code'=>404,
          'status'=>false,
          'message'=>'this request already canceled',
          'data'=>$cancelRequestReservation
      ]);   

    }
  }
  public function lastRequestRedeem(){//get all rows for this user for redeemed_points_count !=0 (this req for redeem) and get the last one 
    $lastRequestRedeem=$this->walletRepo->lastRequestRedeem($this->hiddenWallet);
    if($lastRequestRedeem===404){
        return 0;
        return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'not found any redeemreq for this user',
        'data'=>$lastRequestRedeem
    ]);   
    }else{
        return $lastRequestRedeem;
    return \response()->json([
        'code'=>200,
        'status'=>true,
        'message'=>'getting data successfully',
        'data'=>$lastRequestRedeem
    ]);    
        
    }
  }

}
