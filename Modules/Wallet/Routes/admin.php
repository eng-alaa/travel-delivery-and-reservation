<?php

use Illuminate\Support\Facades\Route;
use Modules\Wallet\Http\Controllers\API\Admin\WalletController;
use Modules\Operation\Http\Controllers\API\Admin\ChargingController;
use Modules\Operation\Http\Controllers\API\Admin\ReservationController;

Route::middleware(['auth:api'])->namespace('API')->group(function(){

              

    Route::prefix('wallet')->group(function(){
        Route::get('/get-all-delagtes', [WalletController::class,'getAllDelagtes']);
        Route::post('/connect-delegate/{id}', [WalletController::class,'connectDelegate']);
        Route::get('/get-all-drivers', [WalletController::class,'getAllDrivers']);
        Route::post('/connect-driver/{id}', [WalletController::class,'connectDriver']);
        Route::post('/search-client/{personal_id}', [WalletController::class,'searchClient']);
        Route::post('/connect-client/{id}', [WalletController::class,'connectClient']);
        /////////////////routes for system wallet////////
        Route::prefix('system')->group(function(){
            ///////***//////get redeemedRequets for employee A(employee accept redeemed requests) , for employee B(employee for delivery batch and reviewing)/////////////
            Route::get('/get-redeemed-requests-paginate-for-employee-a', [WalletController::class,'getRedeemedRequestsPaginateForEmployeeA']);
            Route::get('/get-redeemed-requests-that-returned', [WalletController::class,'getRedeemedRequestsThatReturned']);
            Route::get('/get-redeemed-requests-paginate-for-employee-b', [WalletController::class,'getRedeemedRequestsPaginateForEmployeeB']);
            //for employee A(employee accept redeemed requests)//
            Route::get('/accept-redeemed-request/{id}', [WalletController::class,'acceptRedeemedRequest']);
            //for employee B(employee for delivery batch and reviewing)//
            Route::post('/delivery-money-for-user-has-redeemed-request/{id}', [WalletController::class,'deliveryMoney']);
            Route::get('/return-request-into-employee-a/{id}', [WalletController::class,'returnRequestIntoEmployeeA']);
            Route::get('/count-points-in-delivery-money', [WalletController::class,'countPointsIndeliveryMoney']);
            Route::get('/count-requests-in-delivery-money', [WalletController::class,'countRequestsIndeliveryMoney']);
            Route::get('/count-requests-under-reviewing', [WalletController::class,'countRequestsUnderReviewing']);


            Route::get('/return-points-into-original-points', [WalletController::class,'returnPointsIntoOriginalPoints']);
            
            Route::get('/cancel-redeem-request-from-admin/{id}', [WalletController::class,'cancelRequestRedeem'])->name('api.user.wallet.hidden.cancel-redeem-request-from-admin');
            Route::get('/total-current-points-in-system-wallets', [WalletController::class,'totalCurrentPointsInSystemWallets'])->name('api.admin.wallet.system.total-current-points-in-system-wallets');
            Route::get('/total-points-in-system-wallet', [WalletController::class,'totalPointsInSystemWallet'])->name('api.admin.wallet.system.total-points-in-system-wallet');
            Route::post('/charging/{user_id}', [WalletController::class,'charging'])->name('api.admin.wallet.system.charging');
            Route::post('/completed-proccess-traveling/{user_id}', [WalletController::class,'completedProccessTraveling'])->name('api.admin.wallet.system.completed-proccess-traveling');

        });
    });
});
