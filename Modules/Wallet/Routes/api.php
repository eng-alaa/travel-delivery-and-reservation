<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Wallet\Http\Controllers\API\User\WalletController;
Route::middleware(['auth:api'])->namespace('API\User')->group(function(){
    Route::prefix('users/wallet')->group(function(){
        //////////////////routes for myPoints////////////////
        Route::get('/all-data-wallet-user', [WalletController::class,'allDataWalletUser'])->name('api.user.wallet.original.all-data-wallet-user');

        Route::prefix('my-points')->group(function(){
            Route::get('/total', [WalletController::class,'totalMyPointsOriginalWallet'])->name('api.user.wallet.original.total-myPoints-original-wallet');
            Route::get('/reservations', [WalletController::class,'myReservations'])->name('api.user.wallet.original.my-reservations');
            Route::get('/payments', [WalletController::class,'myPayments'])->name('api.user.wallet.original.my-payments');

        });
        //////////////////routes for original wallet////////////////
        Route::prefix('original')->group(function(){
            Route::get('/count-reserved-points', [WalletController::class,'countReservedPoints'])->name('api.user.wallet.original.count-reserved-points');
            Route::get('/count-allowed-redeemed-points', [WalletController::class,'countAllowedRedeemedPoints'])->name('api.user.wallet.original.count-allowed-redeemed-points');
            Route::get('/count-offer-points', [WalletController::class,'countOfferPoints'])->name('api.user.wallet.original.count-offer-points');
            Route::get('/get-all-data-original-wallet/{userId}', [WalletController::class,'getAllDataOriginalWallet'])->name('api.user.wallet.original.get-all-data-original-wallet');

        });
        
            /////////////////routes for hidden wallet/////////////
        Route::prefix('hidden')->group(function(){
            Route::get('/reservation-points', [WalletController::class,'reservationPoints'])->name('api.user.wallet.hidden.reservation-points');
            Route::get('/redeemed-points', [WalletController::class,'redeemedPoints'])->name('api.user.wallet.hidden.redeemed-points');
            Route::post('/redeem-request', [WalletController::class,'requestRedeem'])->name('api.user.wallet.hidden.request-redeem');
            Route::get('/get-all-redeem-requests', [WalletController::class,'getAllRedeemRequests'])->name('api.user.wallet.hidden.get-all-redeem-requests');
            Route::get('/get-all-reservation-requests', [WalletController::class,'getAllReservationRequests'])->name('api.user.wallet.hidden.get-all-reservation-requests');
            Route::get('/cancel-redeem-request/{id}', [WalletController::class,'cancelRequestRedeem'])->name('api.user.wallet.hidden.cancel-redeem-request');
            Route::post('/reservation-request', [WalletController::class,'requestReservation'])->name('api.user.wallet.hidden.request-reserved');
            Route::get('/cancel-reservation-request/{id}', [WalletController::class,'cancelRequestReservation'])->name('api.user.wallet.hidden.cancel-reserved-request');
            Route::get('/last-request-redeem', [WalletController::class,'lastRequestRedeem'])->name('api.user.wallet.hidden.last-request-redeem');
        });
    });


});