<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
class UserHiddenWallet extends Model
{
    protected $table="users_hidden_wallets";
  /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'operation_num',
        'user_id',
        'reservation_points_count',
        'redeemed_points_count',
        'status'
    ];
        public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatusAttribute($value){
        if($value==0){
            return 'Pending';
        }elseif ($value==1) {
            return 'Accepted';
        }elseif ($value==3) {
            return 'Returned';
        }elseif ($value==2) {
            return 'Delivered';
        }elseif ($value==-1) {
            return 'Canceled';
        }
    }
    public function getOriginalStatusAttribute($value){
       return  $this->attributes['status'];
    }
}
