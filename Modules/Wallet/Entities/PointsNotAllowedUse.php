<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointsNotAllowedUse extends Model
{
    protected $table="points_not_allowed_uses";
        /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'operation_num',
        'user_id',
        'points_count',
    ];
    public function getPoints_countAttribute($value){
        return 'A_'.$value;
    }
}
