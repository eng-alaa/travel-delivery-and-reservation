<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemWallet extends Model
{
    
    protected $table="system_wallets";
      /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'wallet_num',
        'original_points_count',
        'points_count_out_to_serve',
        'points_count_out_to_offer'
    ];
    public function users(){
            return $this->belongsToMany('Modules\Auth\Entities\User')
    	->withPivot('return_pints_count');
    }
}
