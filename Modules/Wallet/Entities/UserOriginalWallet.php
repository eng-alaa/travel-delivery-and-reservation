<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOriginalWallet extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'operation_num',
        'user_id',
        'wallet_num',
        'allowed_points_count',
        'offers_points_count',
        'reservation_points_count',
        'redeemed_points_count',
        'status'
    ];
    protected $table="users_original_wallets";
    public function getOffers_Points_CountAttribute($value){
        return 'R_'.$value;
    }
}
