<?php

namespace Modules\Wallet\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\UserHiddenWallet;

/**
 * Class UserHiddenWalletSeederTableSeeder.
 */
class UserHiddenWalletSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {

        UserHiddenWallet::create([
            'operation_num' => '2547',
            'user_id' => 7,
            'reservation_points_count' => '0_0',
            'redeemed_points_count' => 'BR_2',
        ]);        
        UserHiddenWallet::create([
            'operation_num' => '2540',
            'user_id' => 7,
            'reservation_points_count' => '0_0',
            'redeemed_points_count' => 'BR_2',
        ]);
        UserHiddenWallet::create([
            'operation_num' => '2541',
            'user_id' => 7,
            'reservation_points_count' => 'AR_2',
            'redeemed_points_count' => '0_0',

        ]);
        UserHiddenWallet::create([
            'operation_num' => '2542',
            'user_id' => 7,
            'reservation_points_count' => 'AR_2',
            'redeemed_points_count' => '0_0',

        ]);

    }
}
