<?php

namespace Modules\Wallet\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\SystemWallet;


/**
 * Class SystemWalletSeederTableSeeder.
 */
class SystemWalletSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {

        SystemWallet::create([
            'wallet_num' => 1,
            'original_points_count' => 30000,
            'points_count_out_to_serve' =>'0_0' ,
            'points_count_out_to_offer' => '0_0',
        ]);

    }
}
