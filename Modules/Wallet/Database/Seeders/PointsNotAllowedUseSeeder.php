<?php

namespace Modules\Wallet\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\PointsNotAllowedUse;

/**
 * Class PointsNotAllowedUseSeederTableSeeder.
 */
class PointsNotAllowedUseSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {

        PointsNotAllowedUse::create([
            'operation_num' => '10171',
            'user_id' => 7,
            'points_count' => '1_10'
        ]);

    }
}
