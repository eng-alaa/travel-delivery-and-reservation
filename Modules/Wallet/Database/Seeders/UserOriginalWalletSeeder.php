<?php

namespace Modules\Wallet\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\UserOriginalWallet;

/**
 * Class UserOriginalWalletSeederTableSeeder.
 */
class UserOriginalWalletSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {

        UserOriginalWallet::create([
            'operation_num'=> 10447,
            'wallet_num' => 1,
            'user_id'=>7,
            'allowed_points_count' => '1_20',
            'offers_points_count' => '0_0'
        ]);

    }
}
