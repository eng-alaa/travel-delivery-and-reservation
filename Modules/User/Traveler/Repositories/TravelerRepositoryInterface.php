<?php
namespace Modules\User\Traveler\Repositories;

interface TravelerRepositoryInterface
{
    public function delivery();
    public function intravenous();
    public function packages();
    public function formats();
}