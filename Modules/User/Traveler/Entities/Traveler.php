<?php

namespace Modules\User\Traveler\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Traveler extends Model
{
    use HasFactory, Notifiable;
    use SoftDeletes;
    public $table= 'users';

}
