<?php

namespace Modules\User\Traveler\Http\Controllers;

use App\Repositories\BaseRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TravelerController extends Controller
{
       /**
     * @var BaseRepository
     */
    protected $baseRepo;
        /**
     * @var TravelerRepository
     */
    protected $travelerRepo;
        /**
     * @var Traveler
     */
    protected $traveler;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo,Traveler $traveler,TravelerRepository $travelerRepo)
    {
        $this->baseRepo = $baseRepo;
        $this->traveler = $traveler;
        $this->travelerRepo = $travelerRepo;
    }


    public function delivery(){
        
        $users=$this->userRepo->delivery();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    public function intravenous(){
        
        $users=$this->userRepo->intravenous();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    public function packages(){
        
        $users=$this->userRepo->packages();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    public function formats(){
        
        $users=$this->userRepo->formats();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('traveler::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('traveler::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('traveler::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('traveler::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
