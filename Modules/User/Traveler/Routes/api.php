<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('traveler')->middleware(['role:traveler'])->namespace('API')->group(function(){
    Route::get('telecomm-panel', [TravelerController::class,'telecommPanel'])->name('api.traveler.telecomm-panel');
    Route::get('fuel-panel', [TravelerController::class,'fuelPanel'])->name('api.traveler.fuel-panel');
    Route::get('passenger-panel', [TravelerController::class,'passengerPanel'])->name('api.traveler.passenger-panel');

});