<?php

namespace Modules\User\Driver\Http\Controllers;

use App\Repositories\BaseRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class DriverController extends Controller
{
        /**
     * @var BaseRepository
     */
    protected $baseRepo;
        /**
     * @var User
     */
    protected $user;
        /**
     * @var DriverRepository
     */
    protected $driverRepo;
        /**
     * @var Driver
     */
    protected $driver;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo,Driver $driver,DriverRepository $driverRepo)
    {
        $this->baseRepo = $baseRepo;
        $this->driver = $driver;
        $this->driverRepo = $driverRepo;
    }

    public function telecommPanel(){
        
        $drivers=$this->driverRepo->telecommPanel();
        return \response()->json([
            'status'=>200,
            'data'=>$drivers
        ]);
    }
    public function fuelPanel(){
        
        $drivers=$this->driverRepo->fuelPanel();
        return \response()->json([
            'status'=>200,
            'data'=>$drivers
        ]);
    }
    public function passengerPanel(){
        
        $drivers=$this->driverRepo->passengerPanel();
        return \response()->json([
            'status'=>200,
            'data'=>$drivers
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('driver::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('driver::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('driver::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('driver::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
