<?php
namespace Modules\Auth\Repositories\Frontend\User;

interface UserRepositoryInterface
{
    public function telecommPanel();
    public function fuelPanel();
    public function passengerPanel();
}