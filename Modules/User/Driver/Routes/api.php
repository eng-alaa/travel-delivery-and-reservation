<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->namespace('API')->group(function(){
    Route::prefix('driver')->middleware(['role:chauffeur'])->group(function(){
        Route::get('telecomm-panel', [DriverController::class,'telecommPanel'])->name('api.driver.telecomm-panel');
        Route::get('fuel-panel', [DriverController::class,'fuelPanel'])->name('api.driver.fuel-panel');
        Route::get('passenger-panel', [DriverController::class,'passengerPanel'])->name('api.driver.passenger-panel');
    });
});