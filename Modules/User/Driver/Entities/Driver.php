<?php

namespace Modules\User\Driver\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory, Notifiable;
    use SoftDeletes;
    public $table= 'users';

}
