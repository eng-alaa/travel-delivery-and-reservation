<?php

namespace Modules\User\Delegate\Http\Controllers;

use App\Repositories\BaseRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class DelegateController extends Controller
{
   /**
     * @var BaseRepository
     */
    protected $baseRepo;
        /**
     * @var DelegateRepository
     */
    protected $delegateRepo;
        /**
     * @var Delegate
     */
    protected $delegate;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo,Delegate $delegate,DelegateRepository $delegateRepo)
    {
        $this->baseRepo = $baseRepo;
        $this->delegate = $delegate;
        $this->delegateRepo = $delegateRepo;
    }
    public function chargingPanel(){
        
        $users=$this->userRepo->chargingPanel();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    public function reservationPanel(){
        
        $users=$this->userRepo->reservationPanel();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('delegate::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('delegate::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('delegate::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('delegate::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
