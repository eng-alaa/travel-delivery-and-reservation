
<?php
namespace Modules\User\Delegate\Repositories;

use App\Repositories\EloquentRepository;

class DelegateRepository extends EloquentRepository implements UserRepositoryInterface
{
        /**
     * for delegate.
     */
    public function chargingPanel(){

    }
    public function reservationPanel(){

    }
    
}