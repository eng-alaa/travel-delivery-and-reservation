<?php

namespace Modules\Auth\Entities;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Auth\Entities\Role;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Operation\Entities\CarName;
class User extends Authenticatable
{
    use LaratrustUserTrait,HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'locale',
        'name',
        'personal_id',
        'passport_num',
         'security_code',
        'password',
        'phone_no',
        'birth_date',
        'end_date_passport',
        'loading_car',
        'status',
        'documentation',
        'confirmed',
        'agreement'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [

    ];
    public function roles(){
        return $this->belongsToMany(Role::class,'role_user','user_id','role_id');
    }
    public function permissions(){
        return $this->belongsToMany("Modules\Auth\Entities\Permission",'permission_user','user_id','permission_id');
    }
    public function passportImages(){
        return $this->hasMany('App\Models\PassportImage');
    }
    public function accountRecoveries(){
        return $this->hasMany(AccountRecovery::class);
    }
    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
        public function car(){
        return $this->belongsTo(CarName::class);
    }
    public function getDocumentationAttribute($value){
        if($value==0){
            return 'Pending';
        }elseif ($value==1) {
            return 'Verified';
        }elseif ($value==-1) {
            return 'Reject Verification';
        }
    }
    public function getOriginalDocumentationAttribute($value){
       return  $this->attributes['documentation'];
    }
}
