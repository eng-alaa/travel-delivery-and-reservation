<?php
namespace Modules\Auth\Repositories\User;

use App\GeneralClasses\MediaClass;
use App\Models\Image as ModelsImage;
use App\Repositories\EloquentRepository;
use Image;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Crypt;
use Modules\Auth\Entities\User;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class UserRepository extends EloquentRepository implements UserRepositoryInterface
// class UserRepository implements UserRepositoryInterface
{

    public function countryUser($user){
        $countryUser= $user->profile->country;
        return $countryUser;
    }
    public function cityUser($user){
        $cityUser= $user->profile->city;
        return $cityUser;
    }
    public function townUser($user){
        $townUser= $user->profile->town;
        return $townUser;
    }
    //get all users that have role : driver
    public function drivers($model,$roleId){//$model->user,role: driver
        $drivers = $model->whereHas('roles', function($q) use($roleId){
            $q->where('id', $roleId);
        })->with('roles')->get();
        return $drivers;
    }
    //get all users that have role : traveler
    public function travelers($model,$roleId){//$model->user,role: traveler
        $travelers = $model->whereHas('roles', function($q) use($roleId){
            $q->where('id', $roleId);
        })->with('roles')->get();
        return $travelers;
    }


    //get all users that have role : delegate
    public function delegates($model,$roleId){//$model->user,role: delegate
        $delegates = $model->whereHas('roles', function($q) use($roleId){
            $q->where('id', $roleId);
        })->with('roles')->get();
    return $delegates;
    }
    public function getAllUsersPaginate($model,$request){
    $modelData=$model->where('locale',config('app.locale'))->paginate($request->total);
       return  $modelData;
   
    }
    // methods overrides
    public function store($request,$model){
        $data=$request->validated();
        $data['locale']=Session::get('applocale');
        $password=Str::random(8);
        $data['password']=Hash::make($password);

        
        $enteredData=  Arr::except($data ,['passport_images'],['image']);

        $user= $model->create($enteredData);
        if(!empty($data['roles'])){
            $user->roles()->attach($data['roles']);//to create roles for a user
        }


            if(!empty($data['image'])){
                if($request->hasFile('image')){
                    $file_path_original_image_user= MediaClass::store($request->file('image'),'profile-images');//store profile image
                    $data['image']=$file_path_original_image_user;
                }else{
                    $data['image']=$user->image;
                }
                $user->image()->create(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
            }
            if($request->hasFile('passport_images')){
                $filesPassport=[];
                $files= $request->file('passport_images'); //upload file 
                foreach($files as $file){
                    $file_path_original= MediaClass::store($file,'passport-images');//store passport images
                    $data['passport_images']=$file_path_original;
                    array_push($filesPassport,['filename'=>$file_path_original]);
                }
             //   dd($filesPassport);
                $user->passportImages()->createMany($filesPassport);
            }
            return $user;
    }
        public function update($request,$id,$model){

        $user=$this->find($id,$model);
        $data= $request->validated();
        $password=Str::random(8);
        $data['password']=Hash::make($password);

        $enteredData=  Arr::except($data ,['passport_images'],['image']);
        $user->update($enteredData);
        


     if(!empty($data['image'])){
           if($request->hasFile('image')){
               $file_path_original= MediaClass::store($request->file('image'),'profile-images');//store profile image
               $data['image']=$file_path_original;

           }else{
               $data['image']=$user->image;
           }
         if($user->image){
            //   dd($data['image']);
             $user->image()->update(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
   
         }else{
   
             $user->image()->create(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
         }
     }
       if($request->hasFile('passport_images')){

           $files= $request->file('passport_images'); //upload file 
          // dd($files);
           $filesPassport=[];
           foreach($files as $file){ 
          // dd($file);
            //   MediaClass::delete($user->passportImages);
                 // dd($file);
               $file_path_original= MediaClass::store($file,'passport-images');//store passport images
             array_push($filesPassport,['filename'=>$file_path_original]);
             if($user->passportImages->count()!==0){
                   foreach($user->passportImages as $passportImage){
                       if($passportImage->filename!==$file_path_original){
                           $user->passportImages()->delete();
                       }
                   }   
               }
           }
            $user->passportImages()->createMany($filesPassport);


       }
        if(!empty($data['roles'])){
            $user->syncRoles($data['roles']);//to update roles a user
         //   $user->roles()->toggle($data['roles']);//to update roles a user
        }

        return $user;
    }
        public function restorePasswordUser($id,$model){
                //to make restorePasswordUser for an item must be this item  not found in items table  , must be found in trash items
        $itemInTableitems = $this->find($id,$model);//find this item from  table items
        if(!empty($itemInTableitems)){//this item not found in items table
                $password=Str::random(8);
                
         $hahedPassword=Hash::make($password);
        Storage::put('password',$password);

            $itemInTableitems->password=$hahedPassword;
            $itemInTableitems->save();
                    // Send sms to phone
       // $this->smsRepo->send($password,$itemInTableitems->phone_no);
                
                return 200;
            
        }else{
            return 400;
        }
    }
    public function forceDelete($id,$model){
        //to make force destroy for an item must be this item  not found in items table  , must be found in trash items
        $itemInTableitems = $this->find($id,$model);//find this item from  table items
        if(empty($itemInTableitems)){//this item not found in items table
            $itemInTrash= $this->findItemOnlyTrashed($id,$model);//find this item from trash 
            if(empty($itemInTrash)){//this item not found in trash items
                return 404;
            }else{
                $itemInTrash->detachRoles($itemInTrash->roles);
                // MediaClass::delete($itemInTrash->image);
                // MediaClass::delete($itemInTrash->passportImages);
                // $itemInTrash->passportImages()->delete();
                $itemInTrash->forceDelete();
                
                return 200;
            }
        }else{
            return 400;
        }


    }

    
}
