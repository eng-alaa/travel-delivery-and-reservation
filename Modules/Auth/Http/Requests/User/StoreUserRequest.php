<?php

namespace Modules\Auth\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class StoreUserRequest.
 */
class StoreUserRequest extends FormRequest
{
    /**
     * @var BaseRepository
    */
    protected $baseRepo;
    /**
     * StoreUserRequest constructor.
     */
    public function __construct(BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Store user for only superadministrator        
        $authorizeRes= $this->baseRepo->authorize();
        if($authorizeRes==true){
            return true;
        }else{
            return $this->failedAuthorization();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_no' => ['required','string','max:255',Rule::unique('users')],
            'personal_id' => ['required','numeric',Rule::unique('users')],
            'passport_num' => ['required','numeric',Rule::unique('users')],
            'passport_images'=>['nullable', 'array'],
            'passport_images.*'=>['sometimes','mimes:jpeg,bmp,png,gif,svg,pdf'],
            'image'=>['nullable'],
            'image.*'=>['sometimes','mimes:jpeg,bmp,png,gif,svg,pdf'],
            'birth_date'=>['nullable','date'],
            'end_date_passport'=>['nullable','date'],
            'status' => ['sometimes', 'in:1,0'],
            'agreement' => ['sometimes', 'in:1,0'],
            'confirmed' => ['sometimes', 'in:1,0'],
            'roles' => ['sometimes'],
            'roles.*'=>['exists:roles,id']
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'roles.*.exists' => __('One or more roles were not found or are not allowed to be associated with this user type.'),
            'passport_images.*.exists' => __('One or more passport images were not found or are not allowed to be associated with this user type.'),

        
        ];
    }
        /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException(__('Only the superadministrator can Store this user.'));
    }
}
