<?php

namespace Modules\Auth\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequest;
use Modules\Auth\Http\Requests\User\StoreUserRequest;
use Modules\Auth\Http\Requests\User\UpdateUserRequest;
use Modules\Auth\Http\Requests\User\DeleteUserRequest;
// use Modules\Auth\Repositories\User\UserRepository;
use Modules\Auth\Repositories\Role\RoleRepository;
use Modules\Auth\Repositories\Permission\PermissionRepository;
use Modules\Geocode\Repositories\Country\CountryRepository;
use Modules\Geocode\Repositories\City\CityRepository;
use Modules\Geocode\Repositories\Town\TownRepository;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Repositories\User\UserRepository;
use Modules\Auth\Entities\User;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * @var BaseRepository
     */
    protected $baseRepo;
    /**
     * @var UserRepository
     */
    protected $userRepo;
        /**
     * @var User
     */
    protected $user;
    /**
     * @var RoleRepository
     */
    protected $roleRepo;
    
    /**
     * @var PermissionRepository
     */
    protected $permissionRepo;

    /**
     * @var CountryRepository
     */
    protected $countryRepo;
    /**
     * @var CityRepository
     */
    protected $cityRepo;
    
    /**
     * @var TownRepository
     */
    protected $townRepo;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo, User $user,UserRepository $userRepo, RoleRepository $roleRepo,PermissionRepository $permissionRepo, CountryRepository $countryRepo,CityRepository $cityRepo,TownRepository $townRepo)
    {
        $this->middleware(['permission:users_read'])->only('index');
        $this->middleware(['permission:users_trash'])->only('trash');
        $this->middleware(['permission:users_restore'])->only('restore');
        $this->middleware(['permission:users_restore-all'])->only('restore-all');
        $this->middleware(['permission:users_show'])->only('show');
        $this->middleware(['permission:users_store'])->only('store');
        $this->middleware(['permission:users_update'])->only('update');
        $this->middleware(['permission:users_destroy'])->only('destroy');
        $this->middleware(['permission:users_destroy-force'])->only('destroy-force');
        $this->baseRepo = $baseRepo;
        $this->user = $user;
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
        $this->permissionRepo = $permissionRepo;
        $this->countryRepo = $countryRepo;
        $this->cityRepo = $cityRepo;
        $this->townRepo = $townRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $users=$this->userRepo->all($this->user);
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }
        public function getAllUsersPaginate(Request $request){
        
        $users=$this->userRepo->getAllUsersPaginate($this->user,$request);
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }

    public function activation($id){
        $user=  $this->user->find($id);
        $user->status=1;
        $user->save();
        return response()->json([
            'status'=>200,
            'message'=>'Congrats,  account  user has been activated'
        ]);
         
     }
    
    public function drivers(){
        $roleId=3;//user type : driver
        $drivers=$this->userRepo->drivers($this->user,$roleId);
        return \response()->json([
            'status'=>200,
            'data'=>$drivers
        ]);
    }

    public function travelers(){
        $roleId=4;//user type : traveler
        $travelers=$this->userRepo->travelers($this->user,$roleId);//role: traveler
        return \response()->json([
            'status'=>200,
            'data'=>$travelers
        ]);
    }
    //methods for types user
    public function delegates(){
        $roleId=5;//user type : delegate
        $delegates=$this->userRepo->delegates($this->user,$roleId);
        return \response()->json([
            'status'=>200,
            'data'=>$delegates
        ]);
    }

    // methods for trash
    public function trash(Request $request){
        $users=$this->userRepo->trash($this->user,$request);

   //     $users=$this->userRepo->trash();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
      $user=  $this->userRepo->store($request,$this->user);
        return \response()->json([
            'status'=>200,
            'message'=>'stored successfully',
            'user'=>$user
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=$this->userRepo->find($id,$this->user);
        if(empty($user)){
            return \response()->json([
                'status'=>404,
                'data'=>'there is not exit this user'
            ]);
        }else{
            return \response()->json([
                'status'=>200,
                'data'=>$user
            ]);
        }
    }

 

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request,$id)
    {
        $this->userRepo->update($request,$id,$this->user);
        
        return \response()->json([
            'status'=>200,
            'message'=>'updated successfully'
        ]);

    }

    //methods for restoring
    public function restore($id){
        
        $user =  $this->userRepo->restore($id,$this->user);
        
        if(empty($user)){
         return \response()->json([
             'status'=>404,
             'message'=>'this user not found in trash to restore it '
         ]);    
        }else{
            return \response()->json([
                'status'=>200,
                'message'=>'restored successfully'
            ]);
        }

    }
    public function restoreAll(){
        $user =  $this->userRepo->restoreAll($this->user);
        
        if(empty($user)){
         return \response()->json([
             'status'=>404,
             'message'=>' not found any user in trash to restore all it '
         ]);    
        }else{
            return \response()->json([
                'status'=>200,
                'message'=>'restored successfully'
            ]);
        }

    }
    public function restorePasswordUser($id){
               $user= $this->userRepo->restorePasswordUser($id,$this->user);
               $password=Storage::get('password');
       if(empty($user)){//this user not found in table users
        return \response()->json([
            'status'=>404,
            'message'=>'this user not found in table users '
        ]); 
       }else{
           return \response()->json([
               'status'=>200,
               'message'=>'password for this user has been reset  successfully',
               'password'=>$password
           ]);
       }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteUserRequest $request,$id)
    {
       $user= $this->userRepo->destroy($id,$this->user);
       if(empty($user)){//this user not found in table users
        return \response()->json([
            'status'=>404,
            'message'=>'this user not found in table users '
        ]); 
       }else{
           return \response()->json([
               'status'=>200,
               'message'=>'destroyed  successfully'
           ]);
       }
    }
    public function forceDelete(DeleteUserRequest $request,$id)
    {
        //to make force destroy for a user must be this user  not found in users table  , must be found in trash Categories
        $user=$this->userRepo->forceDelete($id,$this->user);
        if($user==404){
            return \response()->json([
                'status'=>404,
                'message'=>'this user not found in users table and  trash users to delete it by forcely'
            ]); 
        }elseif($user==200){
            return \response()->json([
                'status'=>200,
                'message'=>'force deleted all successfully'
            ]); 
        }elseif($user==400){
            return \response()->json([
                'status'=>400,
                'message'=>'this user  found in Categories table so you cannt   delete it by forcely , you can delete it Temporarily after that delete it by forcely  '
            ]); 
        }
    }
    public function allNotifications(){
        $user=auth()->user();
        $notifications=[];
        $resultNotifications=[];
        foreach ($user->unreadNotifications as $notification) {
    $resultNotifications= array_push($notifications,$notification);
}    
        
        return $notifications;
    }

}
