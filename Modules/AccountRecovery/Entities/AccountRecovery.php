<?php

namespace Modules\AccountRecovery\Entities;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PassportImage;
use Modules\Auth\Entities\User;

class AccountRecovery extends Model
{
    public $table="account_recoveries";

    protected $fillable = [
        'user_id',
        'phone_no',
        'password',
        'is_mobile',
        'is_property_rights',
        'status'
    ];
    public function passportImages(){
        return $this->hasMany(PassportImage::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}

