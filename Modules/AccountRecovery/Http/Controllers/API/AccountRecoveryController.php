<?php

namespace Modules\AccountRecovery\Http\Controllers\API;

use App\Models\PassportImage;
use Modules\Auth\Entities\User;
use App\Repositories\BaseRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\AccountRecovery\Entities\AccountRecovery;
use Modules\AccountRecovery\Http\Requests\AccountRecoveryByMobileRequest;
use Modules\AccountRecovery\Http\Requests\AccountRecoveryByPropertRightsRequest;
use Modules\AccountRecovery\Repositories\AccountRecoveryRepository;

class AccountRecoveryController extends Controller
{
     /**
     * @var AccountRecoveryRepository
     */
    protected $accountRecoveryRepo;
         /**
     * @var AccountRecovery
     */
    protected $accountRecovery;
             /**
     * @var User
     */
    protected $user;
             /**
     * @var PassportImage
     */
    protected $passportImage;

    /**
     * AccountsRecoveryController constructor.
     *
     * @param AccountRRecoveryepository $AccountsRecovery
     */
    public function __construct(BaseRepository $baseRepo, AccountRecoveryRepository $accountRecoveryRepo, AccountRecovery $accountRecovery,User $user, PassportImage $passportImage)
    {
        $this->baseRepo = $baseRepo;
        $this->accountRecoveryRepo = $accountRecoveryRepo;
        $this->accountRecovery = $accountRecovery;
        $this->user = $user;
        $this->passportImage = $passportImage;
    }


    public function updateUserAccountRecoveryByPropertRights(AccountRecoveryByPropertRightsRequest $request,$id){
        $updateUserAccountRecovery=  $this->accountRecoveryRepo->updateUserAccountRecoveryByPropertRights($request,$this->accountRecovery,$this->user,$id);
        if($updateUserAccountRecovery===true){ 
           $password= Storage::get('password');
            return \response()->json([
              'status'=>200,
              'message'=>'account  has been recoveried for this user',
              'password'=>$password
          ]);
        }
        elseif($updateUserAccountRecovery===false){
             return \response()->json([
              'status'=>404,
              'message'=>'user for this account has been deleted',
          ]);  
        }
        elseif($updateUserAccountRecovery===1){
            return \response()->json([
              'status'=>400,
              'message'=>' this account arleay has been recoveried for this user',
          ]);  
        }
      }
 public function updateUserAccountRecoveryByMobile(AccountRecoveryByMobileRequest $request,$id){
    $updateUserAccountRecoveryByMobile=  $this->accountRecoveryRepo->updateUserAccountRecoveryByMobile($request,$this->accountRecovery,$this->user,$id);
        if($updateUserAccountRecoveryByMobile===true){ 
            return \response()->json([
              'status'=>200,
              'message'=>'account  has been recoveried for this user'
          ]);
        }
        elseif($updateUserAccountRecoveryByMobile===false){
             return \response()->json([
              'status'=>404,
              'message'=>'user for this account has been deleted',
          ]);  
        }
        elseif($updateUserAccountRecoveryByMobile===1){
            return \response()->json([
              'status'=>400,
              'message'=>' this account arleay has been recoveried for this user',
          ]);  
        }
 }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function accountsRecoveryByPropertyRightsPaginate(Request $request)
    {
        $accountsRecovery= $this->accountRecoveryRepo->accountsRecoveryByPropertyRightsPaginate($this->accountRecovery,$request);
        return \response()->json([
         'status'=>200,
         'data'=>$accountsRecovery
     ]);
    }
    public function accountsRecoveryByMobilePaginate(Request $request)
    {
        $accountsRecovery= $this->accountRecoveryRepo->accountsRecoveryByMobilePaginate($this->accountRecovery,$request);
        return \response()->json([
         'status'=>200,
         'data'=>$accountsRecovery
     ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $showUserAccountRecovery=  $this->accountRecoveryRepo->showUserAccountRecovery($this->accountRecovery,$id);
        return \response()->json([
          'status'=>200,
          'data'=>$showUserAccountRecovery
      ]);
        }
}
