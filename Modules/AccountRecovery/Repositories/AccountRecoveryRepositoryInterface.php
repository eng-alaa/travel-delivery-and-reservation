
<?php
namespace Modules\AccountRecovery\Repositories;


interface AccountRecoveryRepositoryInterface
{
   public function accountsRecovery($model);
   public function showUserAccountRecovery($model,$id);
   public function updateUserAccountRecovery($request,$model1,$model2,$id);

}