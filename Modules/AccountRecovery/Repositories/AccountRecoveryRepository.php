<?php
namespace Modules\AccountRecovery\Repositories;

use App\Repositories\Auth\Sms\SmsRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\Storage;
use App\GeneralClasses\MediaClass;


// class AccountRecoveryRepository extends EloquentRepository implements AccountRecoveryRepositoryInterface{
    class AccountRecoveryRepository extends EloquentRepository {
        
        /**
     * @var SmsRepository
     */
    protected $smsRepo;
    public function __construct(SmsRepository $smsRepo){
        $this->smsRepo = $smsRepo;
    }



    public function accountsRecoveryByMobilePaginate($model,$request){
                    $modelData=$model->where(['is_mobile'=>1])->with('user')->paginate($request->total);

         return $modelData;
 
     }
         public function accountsRecoveryByPropertyRightsPaginate($model,$request){
                                 $modelData=$model->where(['is_property_rights'=>1,'status'=>0])->with('user')->paginate($request->total);

         return $modelData;
 
     }
 
    public function showUserAccountRecovery($model,$id){
       $userAccountRecovery= $model->with(['user','passportImages'])->find($id);
       return $userAccountRecovery;
    }

     



        //update data user for account recovery
        public function updateUserAccountRecoveryByPropertRights($request,$model1,$model2,$id){//model1: accountRecovery , model2: user , id : id accountRecovery
            //get account recovery id
        $accountRecovery = $model1->find($id);
        // if($accountRecovery->status=="1"){
        //     return 1;
        // }else{
         $data=$request->validated();
        //  dd($data['passport_num']);
         $user= $model2->where(['passport_num'=>$data['passport_num']])->first();
         if(!empty($user)){
        $user->status=1;
         $user->phone_no=$data['phone_no'];
      
        //get all images passport for this account recovery update these passportImagesForAccountRecovery will become for user_id not account_recovery_id
      foreach($accountRecovery->passportImages()->get() as $accountRecoveryPassportImage){
         $accountRecoveryPassportImage->user_id=$accountRecovery->user_id;
         $accountRecoveryPassportImage->account_recovery_id=null;
         $accountRecoveryPassportImage->save();
      }
            //     $filesPassport=[];
            //   foreach($accountRecovery->passportImages as $file){
            //         MediaClass::delete($user->passportImages);
            //         $file_path_original= MediaClass::store($file,'passport-images');//store passport images
            //       array_push($filesPassport,['filename'=>$file_path_original]);
            //       if($user->passportImages->count()!==0){
            //             foreach($user->passportImages as $passportImage){
            //                 if($passportImage->filename!==$file_path_original){
            //                     $user->passportImages()->delete();
            //                 }
            //             }   
            //         }
            //     }
                //  $user->passportImages()->createMany($filesPassport);
                          $user->save();

      $data['password']=mt_rand(100000, 999999);
      //update status account recovery into 1 because this account has been recoveried
        $accountRecovery->status=1;
        $accountRecovery->save();
    //   $accountRecovery->update(['status'=>1]);
      Storage::put('password',$data['password']);
       // Send sms to phone is new
        // $this->smsRepo->send($data['password'],$user->phone_no);
      return true;
         }else{
             return false;
         }
         
            
        // }
    }
    
    public function updateUserAccountRecoveryByMobile($request,$model1,$model2,$id){
        $accountRecovery = $model1->find($id);
        if($accountRecovery->status=="1"){
            return 1;
        }else{
         $data=$request->validated();
         $user= $model2->where(['passport_num'=>$data['passport_num']])->first();
         if(!empty($user)){
            $password= $accountRecovery->password;
            $user->status=1;
            $user->password=$password;
            $user->save();
            $accountRecovery->status=1;
            $accountRecovery->save();
                    // Send sms to phone is new
        // $this->smsRepo->send('your account has been recovierd (reset password successfully)',$user->phone_no);
        return true;
         }else{
          return false;   
         }
        }
         }



}
