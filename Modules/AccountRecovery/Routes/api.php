<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\AccountRecovery\Http\Controllers\API\AccountRecoveryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('admin')->middleware(['auth:api'])->namespace('API')->group(function(){
    Route::prefix('accounts-recovery')->group(function(){
        Route::get('by-property-rights', [AccountRecoveryController::class,'accountsRecoveryByPropertyRightsPaginate'])->name('api.admin.users.accounts-recovery-by-property-rights');
        Route::get('by-mobile', [AccountRecoveryController::class,'accountsRecoveryByMobilePaginate'])->name('api.admin.users.accounts-recovery-by-mobile');

        Route::get('show-user-account-recovery/{id}', [AccountRecoveryController::class,'show'])->name('api.admin.users.show-user-account-recovery-by-property-rights');
        Route::post('update-user-account-recovery-by-property-rights/{id}', [AccountRecoveryController::class,'updateUserAccountRecoveryByPropertRights'])->name('api.admin.users.update-user-account-recovery');        
        Route::post('update-user-account-recovery-by-mobile/{id}', [AccountRecoveryController::class,'updateUserAccountRecoveryByMobile'])->name('api.admin.users.update-user-account-recovery-by-mobile');
    });
});
