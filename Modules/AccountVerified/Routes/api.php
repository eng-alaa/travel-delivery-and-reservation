<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\AccountVerified\Http\Controllers\API\AccountVerifiedController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('admin')->middleware(['auth:api'])->namespace('API')->group(function(){
    Route::prefix('accounts-verified')->group(function(){
        Route::get('/', [AccountVerifiedController::class,'accountsVerifiedPaginate'])->name('api.admin.users.accounts-verified');
        Route::get('show-user-account-verify/{id}', [AccountVerifiedController::class,'show'])->name('api.admin.users.show-user-account-verify');
        Route::post('update-user-account-verify/{id}/{userId}', [AccountVerifiedController::class,'updateUserAccountVerified'])->name('api.admin.users.update-user-account-verify'); 
    });
});