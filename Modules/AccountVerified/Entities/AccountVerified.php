<?php

namespace Modules\AccountVerified\Entities;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PassportImage;
use Modules\Auth\Entities\User;

class AccountVerified extends Model
{
 
    public $table="account_verifieds";
    protected $fillable = [
        'passport_num',
        'name'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}

