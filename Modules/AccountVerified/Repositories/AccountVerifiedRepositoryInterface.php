
<?php
namespace Modules\AccountRecovery\Repositories;


interface AccountVerifyRepositoryInterface
{
    public function accountsVerify($model);
   public function showUserAccountVerify($model,$id);
   public function updateUserAccountVerify($request,$model1,$model2,$id,$userId);
}