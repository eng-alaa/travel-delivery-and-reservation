<?php
namespace Modules\AccountVerified\Repositories;

use App\Repositories\Auth\Sms\SmsRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\Storage;

// class AccountVerifiedRepository extends EloquentRepository implements AccountVerifyRepositoryInterface{
    class AccountVerifiedRepository extends EloquentRepository {
        
        /**
     * @var SmsRepository
     */
    protected $smsRepo;
    public function __construct(SmsRepository $smsRepo){
        $this->smsRepo = $smsRepo;
    }



    public function accountsVerifiedPaginate($model,$request){
            $modelData=$model->with('user')->paginate($request->total);

         return $modelData;
 
     }

    public function showUserAccountVerified($model,$id){
       $userAccountVerify= $model->with(['user'])->find($id);
       return $userAccountVerify;
    }

     



        //update data user for account Verify
        public function updateUserAccountVerified($request,$model1,$model2,$id,$user_id){//model1: accountVerify , model2: user , id : id accountVerify
            //get account Verify id
        $accountVerify = $model1->find($id);
        if($accountVerify->status=="1"){
            return 1;
        }else{
         
         $data=$request->validated();
        //  dd($data['passport_num']);
         $user= $model2->where(['id'=>$user_id])->first();
         if(!empty($user)){
        $user->status=1;
         $user->passport_num=$data['passport_num'];
         $user->name=$data['name'];
        $user->save();

   //update accountVerified this user ->name passportnum
        $accountVerify->name=$data['name'];
        $accountVerify->passport_num=$data['passport_num'];
      //update status account Verify into 1 because this account has been verified
        $accountVerify->status=1;
        $accountVerify->save();
       // Send sms to phone is new
        // $this->smsRepo->send('your account has been verified',$user->phone_no);
      return true;
         }else{
             return false;
         }
         
            
        }
    }
    




}
