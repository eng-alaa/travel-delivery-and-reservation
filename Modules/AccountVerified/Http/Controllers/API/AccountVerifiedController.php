<?php

namespace Modules\AccountVerified\Http\Controllers\API;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\User;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;
use Modules\AccountVerified\Entities\AccountVerified;
use Modules\AccountVerified\Http\Requests\AccountVerifyRequest;
use Modules\AccountVerified\Repositories\AccountVerifiedRepository;

class AccountVerifiedController extends Controller
{
      /**
     * @var AccountVerifiedRepository
     */
    protected $accountVerifiedRepo;
         /**
     * @var AccountVerified
     */
    protected $accountVerified;
             /**
     * @var User
     */
    protected $user;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo, AccountVerifiedRepository $accountVerifiedRepo, AccountVerified $accountVerified,User $user)
    {
        $this->baseRepo = $baseRepo;
        $this->accountVerifiedRepo = $accountVerifiedRepo;
        $this->accountVerified = $accountVerified;
        $this->user = $user;
    }


    public function updateUserAccountVerified(AccountVerifyRequest $request,$id,$user_id){
        $updateUserAccountVerified=  $this->accountVerifiedRepo->updateUserAccountVerified($request,$this->accountVerified,$this->user,$id,$user_id);
        
        if($updateUserAccountVerified===true){ 
           $password= Storage::get('password');
            return \response()->json([
              'status'=>200,
              'message'=>'account  has been verified for this user',
              'password'=>$password
          ]);
        }
        elseif($updateUserAccountVerified===false){
             return \response()->json([
              'status'=>404,
              'message'=>'user for this account has been deleted',
          ]);  
        }
        elseif($updateUserAccountVerified===1){
            return \response()->json([
              'status'=>400,
              'message'=>' this account arleady has been verified for this user',
          ]);  
        }
      }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function accountsVerifiedPaginate(Request $request)
    {
        $accountsVerified= $this->accountVerifiedRepo->accountsVerifiedPaginate($this->accountVerified,$request);
        
        return \response()->json([
         'status'=>200,
         'data'=>$accountsVerified
     ]);
    }
         
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $showUserAccountVerified=  $this->accountVerifiedRepo->showUserAccountVerified($this->accountVerified,$id);
        return \response()->json([
          'status'=>200,
          'data'=>$showUserAccountVerified
      ]);
        }
}
