<?php
namespace Modules\Profile\Repositories;

use App\GeneralClasses\MediaClass;
use App\Repositories\EloquentRepository;
use App\Notifications\verificationRequest;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

// class ProfileRepository extends EloquentRepository implements ProfileRepositoryInterface{
class ProfileRepository extends EloquentRepository {



    public function show($model,$userId){
        $modelData=$model->where(['id'=>$userId])->with(['image','passportImages'])->first();
        return $modelData;
    }

    public function storeImage($request,$userId,$model){

        $user=$this->find($userId,$model);
        $data= $request->validated();
        if(!empty($data['image'])){
            if($request->hasFile('image')){
                $file_path_original= MediaClass::store($request->file('image'),'profile-images');//store profile image
                                    $file_path_original_image_user= str_replace("public/","",$file_path_original);
                $data['image']=$file_path_original_image_user;
            }else{
                $data['image']=$user->image;
            }
            $user->image()->create(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
        }
    }

    public function updateImage($request,$id,$model){
        $user=$this->find($id,$model);
        $data= $request->validated();
        
        if(!empty($data['image'])){
            if($request->hasFile('image')){
                $file_path_original= MediaClass::store($request->file('image'),'profile-images');//store profile image
                                    $file_path_original_image_user= str_replace("public/","storage/",$file_path_original);
                //  dd($file_path_original_image_user);
                $data['image']=$file_path_original_image_user;        
                if($user->image){
                    $user->image()->update(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
          
                }else{
          
                    $user->image()->create(['url'=>$data['image'],'imageable_id'=>$user->id,'imageable_type'=>'Modules\Auth\Entities\User']);
                }
            }else{
                $data['image']=$user->image;
            }
      }
    }


    public function requestDocumentation($model,$userId,$req){
        $data=$req->validated();
      $user=  $model->find($userId);
    //   dd($user->original_documentation);
      if($user->original_documentation=="-1"){
          return 400;
      }elseif($user->original_documentation=="1"){
          return 415;
      }else{
          $user->documentation=1;//request documentation under reviewing
          $user->security_code=$data['security_code'];

          $user->save();
          
            return true;
      }
    }
    
        public function updatePassword($request,$model){
        $userId=auth()->guard('api')->user()->id;
       $user= $model->where(['id'=>$userId])->first();
       $data=$request->validated();
            $loggedInPassword=   Storage::get('loggedInPassword');
         //   dd($loggedInPassword);
                if($loggedInPassword==$data['old_password']){
                    $newPassword=Hash::make($data['new_password']);
                    // $confirmationNewPassword=Hash::make($data['confirmation_new_password']);
                    if($data['new_password']==$data['confirmation_new_password']){
                        $user->password=$newPassword;
                        $user->save();
                        return $data['new_password'];
                    }else{
                        return 402; //new password not match with confirmation new password
                    }
                }else{
                    return 400;//old password not correct , pls try again 
                }

    }

    //for admin
    public function acceptingOnRequestDocumentation($model,$userId){
        $user=  $model->find($userId);
        if($user->original_documentation=="2"){//this is accepted
            return 415;
        }elseif($user->original_documentation=="1"||$user->original_documentation=="0"){//this is pending to accept on it
            $user->documentation=2;//request documentation has been accepted
            $user->save();
            
            //send notification to user
             $user->notify(new verificationRequest($user,$user->documentation));//send a notification for this user (accepted on request)
            // Notification::send($user,new verificationRequest($user->documentation));
            return 200;
        }elseif($user->original_documentation=="-1"){//this is rejected
            return 416;
        }
    }
    public function rejectionRequestDocumentation($model,$userId){
        $user=  $model->find($userId);
        if($user->original_documentation=="-1"){//this rejected
            return 419;
        }elseif($user->original_documentation=="2"){
            return 420 ; //cannt reject it, because this accepted on it before time
        }elseif($user->original_documentation==1||$user->original_documentation=="0"){//this pending to accept on it or reject
            $user->documentation="-1";//request documentation has been rejected
            $user->save();
            $user->notify(new verificationRequest($user));//send a notification for this user (rejected on request)
            //send notification to user

            return 200;
        }
    }
}