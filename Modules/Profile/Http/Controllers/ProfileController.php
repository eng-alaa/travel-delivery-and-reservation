<?php

namespace Modules\Profile\Http\Controllers;

use App\Repositories\BaseRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\User;
use Modules\Profile\Http\Requests\AcceptOnRequestDocumentationRequest;
use Modules\Profile\Http\Requests\RejectionRequestDocumentationRequest;


use Modules\Profile\Http\Requests\ShowProfileRequest;
use Modules\Profile\Http\Requests\StoreProfileRequest;
use Modules\Profile\Http\Requests\UpdateProfileRequest;
use Modules\Profile\Repositories\ProfileRepository;
use Symfony\Component\HttpKernel\Profiler\Profile;
use  Modules\Profile\Http\Requests\UpdatePasswordRequest;
use Illuminate\Support\Facades\Storage;

use Modules\Profile\Http\Requests\AddSecurityCodeRequest;
class ProfileController extends Controller
{
     /**
     * @var ProfileRepository
     */
    protected $profileRepo;
         /**
     * @var User
     */
    protected $user;
    
    /**
     * ProfileController constructor.
     *
     * @param ProfileRepository $Profile
     */
    public function __construct(BaseRepository $baseRepo, ProfileRepository $profileRepo, User $user)
    {
        //traveler, delegate
      $this->middleware(['permission:profile_read'])->only('show');
      $this->middleware(['permission:profile_store'])->only('store');
      $this->middleware(['permission:profile_update'])->only('update');
      $this->middleware(['permission:profile_request-documentation'])->only('requestDocumentation');
      //superAdmin
      $this->middleware(['permission:profile_accept-request-documentation'])->only('acceptingOnRequestDocumentation');
      $this->middleware(['permission:profile_reject-request-documentation'])->only('rejectionRequestDocumentation');

        $this->baseRepo = $baseRepo;
        $this->profileRepo = $profileRepo;
        $this->user = $user;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        $userId=auth()->user()->id;
        $this->profileRepo->storeImage($request,$userId,$this->user);
        return \response()->json([
            'status'=>200,
            'message'=>'stored your image successfully'
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(ShowProfileRequest $request)
    {
                $userId=auth()->user()->id;

        $show=  $this->profileRepo->show($this->user,$userId);
        return \response()->json([
            'status'=>200,
            'data'=>$show->load(['car'])
        ]);


    }
//for user
    public function requestDocumentation(AddSecurityCodeRequest $req){
                $userId=auth()->user()->id;

      $requestDocumentation=  $this->profileRepo->requestDocumentation($this->user,$userId,$req);
    //   dd($requestDocumentation);
      if($requestDocumentation===true){

          return \response()->json([
              'code'=>200,
                'status'=>true,
                'message'=>'your request verification under reviewing',
                'data'=>null
            ]);
      }elseif($requestDocumentation===415){
              return \response()->json([
              'code'=>415,
                'status'=>false,
                'message'=>'you already request verification',
                'data'=>null
            ]);

      }elseif($requestDocumentation===400){
              return \response()->json([
              'code'=>400,
                'status'=>false,
                'message'=>'your request has been rejected',
                'data'=>null
            ]);

      }
    }
        public function updatePassword(UpdatePasswordRequest $request){
                // $userId=auth()->guard('api')->user()->id;

      $userUpdatedPassword=  $this->profileRepo->updatePassword($request,$this->user);

                          $token=Storage::get('token');

          
            if($userUpdatedPassword==400){
              return \response()->json([
                  'code'=>400,
                  'status'=>false,
            'message'=>'old password not correct , pls try again ',
            'data'=>null
        ]);
          }elseif($userUpdatedPassword==402){
              return \response()->json([
                  'code'=>402,
                  'status'=>false,
            'message'=>'new password not match with confirmation new password',
            'data'=>null
        ]);
          }else{
              $data=[
                  'new password'=>$userUpdatedPassword,
                  'token'=>$token
                  ];
                              return \response()->json([
            'code'=>200,
            'status'=>true,
            'message'=>'updated successfully',
            'data'=>$data
        ]);
          }
        
      
    }

//for admin
    public function acceptingOnRequestDocumentation(AcceptOnRequestDocumentationRequest $request,$userId){
       $acceptingOnRequestDocumentation= $this->profileRepo->acceptingOnRequestDocumentation($this->user,$userId);
       if($acceptingOnRequestDocumentation==200){
           return \response()->json([
                'status'=>200,
                'message'=>'request documentation has been accepted'
            ]);

       }elseif($acceptingOnRequestDocumentation==415){
        return \response()->json([
            'status'=>415,
            'message'=>'this has been accepted  before time'
        ]);
       }elseif($acceptingOnRequestDocumentation==416){
        return \response()->json([
            'status'=>416,
            'message'=>'this has been rejected  before time'
        ]);
       }
    } 
    public function rejectionRequestDocumentation(RejectionRequestDocumentationRequest $request,$userId){
       $rejectionRequestDocumentation= $this->profileRepo->rejectionRequestDocumentation($this->user,$userId);
       if($rejectionRequestDocumentation==200){
           return \response()->json([
                'status'=>200,
                'message'=>'request documentation has been rejected'
            ]);

       }elseif($rejectionRequestDocumentation==419){
           return \response()->json([
                'status'=>419,
                'message'=>'this has been rejected  before time'
            ]);

       }elseif($rejectionRequestDocumentation==420){
        return \response()->json([
            'status'=>420,
            'message'=>'cannt reject it, because this accepted on it before time  '
        ]);
       }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('profile::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateProfileRequest $request)
    {
        $userId=auth()->user()->id;
        $this->profileRepo->updateImage($request,$userId,$this->user);
        return \response()->json([
            'code'=>201,
            'status'=>true,
            'message'=>'updated successfully',
            'data'=>null
        ]);


    }

}
