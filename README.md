<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Project
Project is a template for laravel project for  a web  And api  with using
### techniques:
- Hmvc
- Repositories 
- Interfaces
- Traits
- Validation Requests
- Auth , Roles , Permissions  by using Laratrust

## How to use:
This project is still in the builing stage..... In general:
- clone the repo
- cd template-laravel
- npm install
- npm run dev
- php artisan generate:key
- change name db , username ,pass in file .env after that make : php artisan module:migrate , php artisan module:seed
- php artisan serve

## Developed By
- **[Eng-Alaa Badra](https://www.linkedin.com/in/eng-alaa-badra/)**

## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
