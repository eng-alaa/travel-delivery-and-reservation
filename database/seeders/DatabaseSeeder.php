<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Auth\Database\Seeders\AuthDatabaseSeeder;
use Modules\Geocode\Database\Seeders\GeocodeDatabaseSeeder;
use Modules\Profile\Database\Seeders\ProfileDatabaseSeeder;
use Modules\Wallet\Database\Seeders\WalletDatabaseSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthDatabaseSeeder::class);//run AuthDatabaseSeeder seeder that it in module AuthDatabaseSeeder(user,role,permission)
        // $this->call(GeocodeDatabaseSeeder::class);//run GeocodeDatabaseSeeder seeder 
        $this->call(WalletDatabaseSeeder::class);
       
    }
}
