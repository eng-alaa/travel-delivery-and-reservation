<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_names', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('car_type_id');
            $table->foreign('car_type_id')
                ->references('id')
                ->on('cars_types')
                ->onDelete('cascade');
            // $table->string('color');
            $table->string('period_traveling');
            $table->string('price');
            $table->interger('current_loading');
            $table->interger('max_loading');
            $table->string('features');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_names');
    }
}
