<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('wallet_num');

            $table->string('original_points_count');
            $table->string('points_count_out_to_serve');//points for user , will travel via it
            $table->string('points_count_out_to_offer');//points for user , buy it for offer
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_wallets');
    }
}
