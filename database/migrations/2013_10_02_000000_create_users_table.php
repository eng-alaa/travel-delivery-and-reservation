<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('locale')->default(config('app.locale'));
            $table->string('name')->nullable();
            $table->integer('personal_id')->nullable();
            $table->integer('passport_num')->nullable();
            $table->string('password')->nullable();
            $table->string('phone_no');
            
            $table->unsignedBigInteger('car_id')->nullable();
            $table->foreign('car_id')
                ->references('id')
                ->on('cars_types')
                ->onDelete('cascade');
            $table->number('loading_car')->default(0);

            $table->tinyInteger('status')->default(1);
            $table->integer('security_code');
            $table->tinyInteger('documentation')->default(0);
            $table->tinyInteger('confirmed')->default(0);
            $table->tinyInteger('agreement')->default(0);
            $table->rememberToken();
            $table->date('birth_date')->nullable();
            $table->date('end_date_passport')->nullable();
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
