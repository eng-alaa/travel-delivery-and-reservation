<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadyTravelingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ready_travelings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('car_id')->nullable();
            $table->foreign('car_id')
                ->references('id')
                ->on('cars')
                ->onDelete('cascade');

            $table->unsignedBigInteger('client_id')->nullable();
            $table->foreign('client_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

                
            $table->integer('jurney_num')->nullable();
                        $table->unsignedBigInteger('operation_num_reservation')->nullable();
            $table->foreign('operation_num_reservation')
                ->references('operation_num')
                ->on('reservations')
                ->onDelete('cascade')->nullable();
            $table->tinyInteger('travel')->default(0);//go
            $table->tinyInteger('confirmed')->default(0);

            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ready_travelings');
    }
}
