<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => true,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'superadministrator' => [
            'users' => 'r,t,res,res-a,sh,c,s,e,u,d,f-d',
            'roles' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'permissions' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            // 'accounts-recovery-by-propert-rights' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            // 'accounts-recovery-by-mobile' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'profile' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'countries' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'cities' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'towns' => 'r,t,res,r-a,sh,c,s,e,u,d,f-d',
            'payments' => 'c,r,u,d',
            'redeemRequests' => 'r,u',
            'deliverMoney' => 'r,u',
            //for wallet (traveler)
            'totalMyPointsOriginalWallet'=>'r',
            'myReservations'=>'r',
            'myPayments'=>'r',
            'countReservedPoints'=>'r',
            'countAllowedRedeemedPoints'=>'r',
            'countOfferPoints'=>'r',
            'getAllDataOriginalWallet'=>'r',
            'reservationPoints'=>'r',
            'redeemedPoints'=>'r',
            'requestRedeem'=>'u',
            'requestRedeem'=>'canc',
            'requestReservation'=>'u',
            'getAllRedeemRequests'=>'r',
            'getAllReservationRequests'=>'r',
            'cancelRequestReservation'=>'u',
            'lastRequestRedeem'=>'r',
            //for module wallet(charging employee)
            'getAllDelagtes' => 'r',
            'getAllDrivers' => 'r',
            'connectDelegate' => 'u',
            'connectDriver' => 'u',
            'searchClient' => 'r',
            'connectClient'=>'u',
            'returnPointsIntoOriginalPoints'=>'u',
            'totalCurrentPointsInSystemWallets'=>'r',
            'totalAllPointsInSystemWallet'=>'r',
            'totalPointsInSystemWallet'=>'r',
            'charging'=>'u',
            'completedProccessTraveling'=>'u',
             //for module operation(charging,reservation)
            //charging
            'addingToCharging' => 's',
            'getWaitingChargingOperations' => 'r',
            'chargingToUser' => 'u',
            'chargingToAllUsers' => 'u',
            'getAllChargingsRecords'=>'r',
            'cancelCharging'=>'canc',
            //reservation
            'getDataUserToReservation' => 'r',
            'addingToReservation'=>'s',
            'getWaitingReservationOperations'=>'r',
            'allowTypeReservation'=>'u',
            'reservationToAllUsers'=>'u',
             'getAllReservationsRecords'=>'r',
            'cancelReservation'=>'canc',
            //for module wallet(redeemRequestsEmployee)
            'redeemRequests' => 'r,u',
            'countRequestsUnderReviewing'=>'r',
            'cancelRequestRedeem'=>'update',
            //for module wallet(deliveryMoneyEmployee)
            'deliveryMoney' => 'r,u',
            'countPointsIndeliveryMoney'=>'r',
            'countRequestsIndeliveryMoney'=>'r',
            'returnRequestIntoEmployeeA'=>'u',
            //module profile
            'profile' => 'r,s,u,ac,rej'


        ],
        'administrator' => [
           

        ],
        'chargingEmployee'=>[
            
            'getAllDelagtes' => 'r',
            'getAllDrivers' => 'r',
            'connectDelegate' => 'u',
            'connectDriver' => 'u',
            'searchClient' => 'r',
            'connectClient'=>'u',
            'returnPointsIntoOriginalPoints'=>'u',
            'totalCurrentPointsInSystemWallets'=>'r',
            'totalAllPointsInSystemWallet'=>'r',
            'totalPointsInSystemWallet'=>'r',
            'charging'=>'u',
            'completedProccessTraveling'=>'u',
            ],
       
        'delegate' => [
            //for module operation(charging,reservation)
            //charging
            'addingToCharging' => 's',
            'getWaitingChargingOperations' => 'r',
            'chargingToUser' => 'u',
            'chargingToAllUsers' => 'u',
            'getAllChargingsRecords'=>'r',
            'cancelCharging'=>'canc',
            //reservation
            'getDataUserToReservation' => 'r',
            'addingToReservation'=>'s',
            'getWaitingReservationOperations'=>'r',
            'allowTypeReservation'=>'u',
            'reservationToAllUsers'=>'u',
            'getAllReservationsRecords'=>'r',
            'cancelReservation'=>'canc',
            //module profile
            'profile' => 'r,s,u,req'
        ],        
        'redeemRequestsEmployee' => [
            //for module wallet
            'redeemRequests' => 'r,u',
            'countRequestsUnderReviewing'=>'r',
            'cancelRequestRedeem'=>'update',
            //module profile
            'profile' => 'r,s,u,req'
        ],        
        'deliveryMoneyEmployee' => [
            //for module wallet
            'deliveryMoney' => 'r,u',
            'countPointsIndeliveryMoney'=>'r',
            'countRequestsIndeliveryMoney'=>'r',
            'returnRequestIntoEmployeeA'=>'u',
            //module profile
            'profile' => 'r,s,u,req'
        ],

        'traveler' => [
            'totalMyPointsOriginalWallet'=>'r',
            'myReservations'=>'r',
            'myPayments'=>'r',
            'countReservedPoints'=>'r',
            'countAllowedRedeemedPoints'=>'r',
            'countOfferPoints'=>'r',
            'getAllDataOriginalWallet'=>'r',
            'reservationPoints'=>'r',
            'redeemedPoints'=>'r',
            'requestRedeem'=>'u',
            'cancelRequestRedeem'=>'u',
            'requestReservation'=>'u',
            'getAllRedeemRequests'=>'r',
            'getAllReservationRequests'=>'r',
            'cancelRequestReservation'=>'u',
            'lastRequestRedeem'=>'r',
            //reservations
            'getAllReservationsUser'=>'r',
            // 'reseveForClient'=>'s',
            'addDocuments'=>'s',
            'getDocumentations'=>'r',
            'getAllReservationsRecords'=>'r',
            'cancelReservation'=>'u',
            'typesReservations'=>'r',
            'addTypeReservation'=>'s',
            'getPointsCountTraveler'=>'r',
            'getTypesCar'=>'r',
            'getDataCarsViaType'=>'r',
            'addCarToReservation'=>'s',
            'getDataCar'=>'r',
            'addReservation'=>'s',
            'myReservations'=>'r',
            'showReservation'=>'sh',


            //module profile
            'profile' => 'r,s,u,req'
        ],
        'driver'=>[
                        //jurnies
            'createJurney'=>'s',
            'openJurney'=>'r',
            'addPassportNumIntoReadyTraveling'=>'s',
            'startJurney'=>'u',
            'finishingJurney'=>'u',
            //payments
            'showForPayment'=>'sh',
            'addPayment'=>'s',
            'completePayment'=>'u',
            'getAllPaymentsRecord'=>'r',
            'paymentsTravelers'=>'r',       
            'getAllConfirmedReservations'=>'r',
            'cancelConfirmReservation'=>'u',
            ]
    ],

    'permissions_map' => [
        'r' => 'read',
        't' => 'trash',
        'res' => 'restore',
        'r-a' => 'restore-all',
        'sh' => 'show',
        'c' => 'create',
        's' => 'store',
        'e' => 'edit',
        'u' => 'update',
        'd' => 'destroy',
        'f-d' => 'force-destroy',
        'req'=>'request-documentation',
        'ac'=>'accept-request-documentation',
        'rej'=>'reject-request-documentation',
        'canc'=>'cancel'
    ]
];
