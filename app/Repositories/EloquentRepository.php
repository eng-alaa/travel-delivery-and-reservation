<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;

class EloquentRepository
{
    public function all($model){
    $modelData=$model->where('locale',config('app.locale'))->where('status',1)->get();
       return  $modelData;
   }
   public  function trash($model,$request){
       $modelData=$this->findAllItemsOnlyTrashed($model)->paginate($request->total);
        return $modelData;
    }
    public function find($id,$model){
        $item=$model->where('id',$id)->first();
        if(!empty($item)){
            $item=$model->findOrFail($id);
        }
        return $item;
    }
    public function findItemOnlyTrashed($id,$model){        
        $itemInTrash=$model->onlyTrashed()->where('id',$id)->first();//item in trash
        if(empty($itemInTrash)){
            return $itemInTrash; 
        }else{
            $item=$model->onlyTrashed()->findOrFail($id);//find this item from trash
            return $item;
       }
    }
    public function findAllItemsOnlyTrashed($model){        
        $itemsInTrash=$model->onlyTrashed()->get();//items in trash
       if(empty($itemsInTrash)){
          return $itemsInTrash; 
       }else{

           $items=$model->onlyTrashed();//get items from trash
           return $items;
       }
    }
    public function store($request,$model){
        // dd(1);
        $data=$request->validated();
        $item= $model->create($data);
        return $item;
    }
    public function update($request,$id,$model){
        $item=$this->find($id,$model);
        $item->update($request->validated());
        return $item;
    }
    //methods for restoring
    public function restore($id,$model){
        $item = $this->findItemOnlyTrashed($id,$model);//get this item from trash 
        if(!empty($item)){//this item not found in trash to restore it
            $item->restore();
        }
        return $item;
    }
    public function restoreAll($model){
        $items = $this->findAllItemsOnlyTrashed($model);//get  items  from trash
        if(!empty($items)){//there is not found any item in trash
            $items = $items->restore();//restore all items from trash into items table
        }
        return $items;
        
    }
    //methods for deleting
    public function destroy($id,$item){
        $item=$this->find($id,$item);
        if(!empty($item)){//this item not found in table items
            $item->delete($item);
        }
        return $item;
    }

    public function forceDelete($id,$model){
        //to make force destroy for an item must be this item  not found in items table  , must be found in trash items
        $itemInTableitems = $this->find($id,$model);//find this item from  table items
        if(empty($itemInTableitems)){//this item not found in items table
            $itemInTrash= $this->findItemOnlyTrashed($id,$model);//find this item from trash 
            if(empty($itemInTrash)){//this item not found in trash items
                return 404;
            }else{
                $itemInTrash->forceDelete();
                return 200;
            }
        }else{
            return 400;
        }


    }


}
