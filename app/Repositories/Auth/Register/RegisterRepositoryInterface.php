<?php
namespace App\Repositories\Auth\Register;

interface RegisterRepositoryInterface{
    public function store($request,$model);
}