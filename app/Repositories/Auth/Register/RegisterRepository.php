<?php
namespace App\Repositories\Auth\Register;

use App\GeneralClasses\MediaClass;
use App\Repositories\Auth\Sms\SmsRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Modules\AccountRecovery\Entities\AccountRecovery;
use Modules\AccountVerified\Entities\AccountVerified;
class RegisterRepository extends EloquentRepository implements RegisterRepositoryInterface{
        
    /**
     * @var SmsRepository
     */
    protected $smsRepo;
    public function __construct(SmsRepository $smsRepo){
        $this->smsRepo = $smsRepo;
    }
    public function firstStepRegister($request,$model){//this step to send code into phone_no just
    $finished= Storage::get('finished');
 if($finished!==""){
        return 3;
        
    }
    $userId=Storage::get('userId');
    if($userId!==""){
        return false;
    }
   
  

        $data=$request->validated();//model confirmation code , req1->phone_no
        
        // cannt store this user in db , because he must finished from step1, step2
        $code=mt_rand(100000, 999999);//random
        //store it in local storage 
        Storage::put('code',$code);
        Storage::put('phone_no',$request->phone_no);
        //store code for this user in confirmationcodes table(will become this code for this user only)
        $data['code']=$code;
        $e=$model->create(['phone_no'=>$data['phone_no'],'code'=>$code]);//$data2-> phone_no , code
        // Send code  to phone
    //    $this->smsRepo->send($code,$data['phone_no']); 
    Storage::put('sentCode',1);
    Storage::put('step1',1);
    Storage::put('code',$code);
         $code=Storage::get('code');
        // dd($code);
//   $sentCode=Storage::get('sentCode');
//     if($sentCode!==""){
//         return 2;//sent code into user
//     }
         return true;
    

    }
    
    public function confirmationCode($request,$model){//this step only for confirmation code

        $code=Storage::get('code');
        $phone_no=Storage::get('phone_no');

        if($phone_no==null){
            return 0;
        }
        if($request->code==$code){
            // find the code
            $confirmationCode = $model->firstWhere('code', $request->code);
            // check if it does not expired: the time is one hour 
            if ($confirmationCode->created_at > now()->addHour()) {
                $confirmationCode->delete();
                return response(['message' => trans('passwords.code_is_expire')], 422);
            }
            Storage::put('confirmed',1);
          
        return true;
    }else{
        return false;
    }
        
   }
   public function secondStepRegisterPass($request,$model){//this step to enter pass to can enter into website via this pass and random personal_id for this user
       $finished= Storage::get('finished');
 if($finished!==""){
        return 3;
 }
               $step1=Storage::get('step1');
        if($step1==""){
            return -1;
        }
       $data=$request->validated();//$model->user , $request-> password
       $confirmed=Storage::get('confirmed');
       if($confirmed==''){
           return 0;
       }
     $phone_no=Storage::get('phone_no');
     if($phone_no==""){
         return false;//user  should be in first step after that move int this step2
     }
    $personal_id=mt_rand(100000, 999999);//random
    $password=Hash::make($data['password']);
    Storage::put('password',$data['password']);
    Storage::put('personal_id',$personal_id);
   $userReg= $model->create(['password'=>$password,'confirmed'=>1,'personal_id'=>$personal_id,'phone_no'=>$phone_no]);//to can this user enter into website if he need
    Storage::put('userId',$userReg->id);
    Storage::put('step2',1);
    return true;
   }
    public function thirdStepRegister($request,$model){//this step to complete operation registration this user
        //$model: user

               $finished= Storage::get('finished');
 if($finished!==""){
        return 3;
 }

               $confirmed=Storage::get('confirmed');
       if($confirmed==''){
           return -3;
       }
 
        Storage::put('finished',null);
      if($request->agreement==0){
        return 0;//user dont make Approval of the agreement 
    }
    $userId=Storage::get('userId');
      if($userId==null){
          return 1;//user  should be in first step after that move int this step2
      }
      $user=  $model->where(['id'=>$userId])->first();
      $confirmedUser=$user->confirmed;
      if($confirmedUser==0){
          return 2;//user make confimation for code that we send it to you
      }
       // $request->  passport_num, passport_images, agreement
        $data=$request->validated();


        $data['locale']=Session::get('applocale');
        $enteredData=  Arr::except($data ,['passport_images'],['role']);
        
        $userUpdated= $user->update($enteredData);//store passport_num , agreemet
        if($request->hasFile('passport_images')){
            $filesPassport=[];
            $files= $request->file('passport_images'); //upload file 
            foreach($files as $file){
                $file_path_original= MediaClass::store($file,'passport-images');//store passport images
                $data['passport_images']=$file_path_original;
                array_push($filesPassport,['filename'=>$file_path_original]);
            }
            $user->passportImages()->createMany($filesPassport);
        }
        //    $this->smsRepo->send($password,$user->phone_no); 
       // DB::table('role_user')->insert(['role_id'=>3,'user_id'=>$user['id']]);//role id =3 -> user
        $user->roles()->attach([8]);//to create roles for a user
        AccountVerified::create(['user_id'=>$user->id,'status'=>0]);//store in this table to wait accept on this user from superAdmin
        Storage::put('finished',1);
        
        return true;

    }
   
    public function fourthStepRegister($request,$model){
             $data=$request->validated();
                 $userId=Storage::get('userId');
                       if($userId==null){
          return 1;//user  should be in first step after that move int this step2
      }
                          $user=  $model->where(['id'=>$userId])->first();
                    $user->security_code=$data['security_code'];
                    $user->save();
                       event(new Registered($user));
                    return $user;
                

    }

   public function updatePhoneNo($request,$model){
    $personal_id=session()->get('personal_id');
    $userReg=  $model->where('personal_id',$personal_id);
    $userReg->update('phone_no',$request->phone_no);
    return true;
   }
}