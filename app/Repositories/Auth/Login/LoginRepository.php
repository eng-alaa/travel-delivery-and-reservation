<?php
namespace App\Repositories\Auth\Login;

use Modules\Auth\Entities\User;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class LoginRepository extends EloquentRepository implements LoginRepositoryInterface{
    public function startStepsReg(){
                 Storage::delete('step1','step2','userId','code','confirmed','sentCode','finished','step1','loggedInPassword');
                      Storage::put('userId',null);
            Storage::put('step1',null);
            Storage::put('step2',null);
    Storage::put('code',null);
    Storage::put('confirmed',null);
    Storage::put('sentCode',null);
    Storage::put('finished',null);
        Storage::put('sentCode',null);
        Session::put('loggedInPassword',null);
        return response()->json([
                    'status'=>200,
                    'message'=>'now you can go into register1'
                    ]);
    }
    public function login($request){
        $data=$request->validated();
                Storage::put('loggedInPassword',$data['password']);
            $loggedInPassword=   Storage::get('loggedInPassword');
                // dd($loggedInPassword);
      //  if(!Hash::check($data['password'],))
        if(!auth()->attempt($data)){
            return false;
        }else{
            $accessToken=auth()->user()->createToken('token')->accessToken;
                                Storage::put('token',$accessToken);

            return $accessToken;
        }

    }
    
    public function logout($request){
         Storage::delete('step1','step2','userId','code','confirmed','sentCode','finished','step1','loggedInPassword');
        $request->user()->token()->revoke();
            Storage::put('userId',null);
            Storage::put('step1',null);
            Storage::put('step2',null);
    Storage::put('code',null);
    Storage::put('confirmed',null);
    Storage::put('sentCode',null);
    Storage::put('finished',null);
        Storage::put('sentCode',null);
        // Storage::put('loggedInPassword',null);

        return true;
    }
    
    // methods overrides

}