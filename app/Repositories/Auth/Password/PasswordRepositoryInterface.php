<?php
namespace App\Repositories\Auth\Password;

interface PasswordRepositoryInterface{
    public function forgotPassword($request,$model1,$model2);
    public function resetPassword($request,$model1,$model2);
}