<?php
namespace App\Repositories\Auth\Password;

use App\GeneralClasses\MediaClass;
use App\Repositories\Auth\Sms\SmsRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PasswordRepository  implements PasswordRepositoryInterface{
        /**
     * @var SmsRepository
     */
    protected $smsRepo;
    public function __construct(SmsRepository $smsRepo){
        $this->smsRepo = $smsRepo;
    }
    public function forgotPassword($request,$model1,$model2){
        $data=$request->validated();

        // Delete all old code that user send before.
        $model2->where('passport_num', $data['passport_num'])->delete();
        // Generate random code
        $data['code'] = mt_rand(100000, 999999);
        // Create a new code
        $model2->create($data);
        Storage::put('code',$data['code']);
        Storage::put('passport_num',$data['passport_num']);
        $user= $model1->where(['passport_num'=>$data['passport_num']])->first();
        // Send sms to phone
       // $this->smsRepo->send($data['code'],$user->phone_no);
        return true;
    }
    public function checkCode($request,$model){
         // find the code
         $passwordReset = $model->firstWhere('code', $request->code);

         // check if it does not expired: the time is one hour
         if ($passwordReset->created_at > now()->addHour()) {
             $passwordReset->delete();
             return response()->json([
                'status'=>false,
                 'code'=>400,
                'message'=>'code is expire',
                'data'=>$passwordReset

            ]);
         }
         return true;
 
         
    }
    public function resetPassword($request,$model1,$model2){
        // find user's passport_num 
        $user = $model1->firstWhere('passport_num', $request->passport_num);
        // $user->password=Hash::make($request->password);
        // $user->save();
        
        $model2->create(['user_id'=>$user->id,'phone_no'=>null,'is_mobile'=>1,'password'=>Hash::make($request->password)]);
         
        return true;
    }

    public function propertyRights($request,$model1,$model2){
        $user= $model1->where(['passport_num'=>$request->passport_num])->first();
        if($user){           
             //$data -> passport_images, passport_num,phone_no
             $data=$request->validated();
             $enteredData=  Arr::except($data ,'passport_num',['passport_images']);
            $accountRecoveryUser= $model2->create(['user_id'=>$user->id,'phone_no'=>$data['phone_no'],'property_rights'=>1]);
         //create passport_images for accoubt recovery this user , but not agree on his , until now
            if($request->hasFile('passport_images')){
                $files= $request->file('passport_images'); //upload file 
                $filesPassport=[];
                foreach($files as $file){
                    $file_path_original= MediaClass::store($file,'passport-images');//store passport images
                   array_push($filesPassport,['filename'=>$file_path_original]);
                 
                }
                 $accountRecoveryUser->passportImages()->createMany($filesPassport);
            }
            $accountRecoveryUser->is_property_rights=1;
            $accountRecoveryUser->save();
            return true;
           
 
        }else{
            return false;
         
        }
 
    }
}