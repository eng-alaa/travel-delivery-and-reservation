<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class ThirdStepRegisterRequest.
 */
class ThirdStepRegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'passport_num' => ['required','string','max:255','unique:users'],
            'passport_images'=>['sometimes', 'array'],
           // 'passport_images.*'=>['mimes:jpeg,bmp,png,gif,svg,pdf'],
            'status' => ['sometimes', 'in:1,0'],
            'agreement' => ['required','sometimes', 'in:1,0'],
            // 'role'=>['required','exists:roles,id']
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'passport_images.*.exists' => __('One or more passport images were not found or are not allowed '),

        
        ];
    }

}
