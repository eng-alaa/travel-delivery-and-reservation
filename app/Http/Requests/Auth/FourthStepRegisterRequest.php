<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\BaseRepository;
use Illuminate\Validation\Rules;

/**
 * Class FourthStepRegisterRequest.
 */
class FourthStepRegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'security_code' => ['required','numeric','digits:4']
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'passport_images.*.exists' => __('One or more passport images were not found or are not allowed '),

        
        ];
    }

}
