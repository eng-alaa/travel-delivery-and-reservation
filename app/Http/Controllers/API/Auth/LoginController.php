<?php

namespace App\Http\Controllers\API\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Repositories\Auth\Login\LoginRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class LoginController extends Controller
{
    /**
     * @var LoginRepository
     */
    protected $loginRepo;
    public function __construct(LoginRepository $loginRepo){
        $this->loginRepo = $loginRepo;
    }
        public function startStepsReg(){
                 Storage::delete('step1','step2','userId','code','confirmed','sentCode','finished','step1');
                      Storage::put('userId',null);
            Storage::put('step1',null);
            Storage::put('step2',null);
    Storage::put('code',null);
    Storage::put('confirmed',null);
    Storage::put('sentCode',null);
    Storage::put('finished',null);
        Storage::put('sentCode',null);
        return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'now you can go into register1',
                    'data'=>null
                    ]);
    }
    public function authLogin(){
        return response()->json([
            'status'=>401,
            'message'=>'You havent authorization in this website'
        ]);
    }
    public function userToken(){
        $user=auth('api')->user();
        $user->with('roles')->get();
         return response()->json([
            'status'=>200,
            'user'=>auth('api')->user()
        ]);
    }
    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */


    public function store(LoginRequest $request){
 $loginUser=  $this->loginRepo->login($request);
        if(!$loginUser){
            return response()->json([
                'status'=>false,
                'code' => 400,
                'message' => 'Invalid credentials',
                'data'=> null
            ]);
        }else{
            $user=auth()->user();
            $data=[
                "token"=>$loginUser,
                "user"=>$user,
                "roles"=>$user->roles
                ];
            return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'logged in successfully',
                    'data'=>$data
                ]);
        }
      }
       
        
        
    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        // try{
      $logout=  $this->loginRepo->logout($request);
        if($logout==true){
            return response()->json([
                'status'=>200,
                'message'=>'logout successfully'
            ]); 
        }
        // }catch(\Exception $ex){
        //    return response()->json([
        //        'status'=>500,
        //        'message'=>'There is something wrong, please try again'
        //    ]);  
        // } 
    }
}
