<?php
namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\CheckCodeRequest;
use App\Models\PasswordReset;
use App\Repositories\Auth\Password\PasswordRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class CheckCodeController extends Controller
{
        /**
     * @var PasswordReset
     */
    protected $PasswordReset;
            /**
     * @var PasswordRepository
     */
    protected $passwordRepo;
    public function __construct(PasswordReset $passwordReset,PasswordRepository $passwordRepo){
        $this->passwordRepo = $passwordRepo;
        $this->passwordReset = $passwordReset;

    }
    public function __invoke(CheckCodeRequest $request)
    {
        // if($request->mobile==1){
            $checkCode=$this->passwordRepo->checkCode($request,$this->passwordReset);
            if($checkCode==true){
                return response()->json([
                    'status'=>true,
                'code'=>200,
                'message'=>'code is valid',
                'data'=>$checkCode
            ]);
            }
        // }else{
        //     return response()->json([
        //         'status'=>400,
        //         'message'=>'you should select a mobile or Property rights to make a reset password',
        //     ]);
        // }

    }
}