<?php
namespace App\Http\Controllers\API\Auth;

use App\GeneralClasses\MediaClass;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PropertyRightRequest;
use Modules\PropertyRights\Entities\PropertyRights;
use App\Models\PasswordReset;
use Modules\Auth\Entities\User;
use App\Repositories\Auth\Password\PasswordRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\AccountRecovery\Entities\AccountRecovery;

class PropertyRightController extends Controller
{
                /**
     * @var User
     */
    protected $user;
            /**
     * @var AccountRecovery
     */
    protected $accountRecovery;
            /**
     * @var PasswordRepository
     */
    protected $passwordRepo;
    public function __construct(AccountRecovery $accountRecovery,User $user,PasswordRepository $passwordRepo){
        $this->user = $user;
        $this->passwordRepo = $passwordRepo;
        $this->accountRecovery = $accountRecovery;

    }
    public function __invoke(PropertyRightRequest $request)
    {
        // if($request->property_rights==1){
            $propertyRights=$this->passwordRepo->propertyRights($request,$this->user,$this->accountRecovery);

            if($propertyRights==true){
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'your request under reviewing , pls wait to accept on it , will reach to you a sms when accepting on it',
                    'data'=>$propertyRights
                ]);
            }else{
                return response()->json([
                                        'status'=>false,

                    'code'=>400,
                    'message'=>'passport num. not found in the website, pls enter Write it right.',
                                        'data'=>$propertyRights

                ]);

            }
        // }elseif($request->mobile==1){
        //     return response()->json([
        //                             'status'=>false,

        //         'code'=>400,
        //         'message'=>'you should select Property rights to make a reset password by --Property rights-- ',
        //                             'data'=>$propertyRights

        //     ]);
        // }
        // else{
        //     return response()->json([
        //                             'status'=>false,

        //         'code'=>400,
        //         'message'=>'you should select a mobile or Property rights to make a reset password',
        //                             'data'=>$propertyRights

        //     ]);
        // }
    }
}