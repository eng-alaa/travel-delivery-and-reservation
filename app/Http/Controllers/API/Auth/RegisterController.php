<?php

namespace  App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ConfirmationCodeRequest;
use App\Http\Requests\Auth\FirstStepRegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\PasswordStepRegisterRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\ThirdStepRegisterRequest;
use App\Http\Requests\Auth\FourthStepRegisterRequest;
use App\Models\ConfirmationCode;
use App\Models\Profile;
use App\Models\Role;
use Modules\Auth\Entities\User;
use App\Providers\RouteServiceProvider;
use App\Repositories\Auth\Register\RegisterRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;
use DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Modules\Profile\Entities\Profile as EntitiesProfile;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * @var RegisterRepository
     */
    protected $regRepo;
    /**
     * @var User
     */
    protected $user;
        /**
     * @var ConfirmationCode
     */
    protected $confirmationCode;
            /**
     * @var ConfirmationCodeRequest
     */
    protected $confirmationCodeRequest;
    public function __construct(RegisterRepository $regRepo,User $user,ConfirmationCode $confirmationCode){
        $this->regRepo = $regRepo;
        $this->user = $user;
        $this->confirmationCode = $confirmationCode;

    }
    
    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function firstStepRegister(FirstStepRegisterRequest $request)
    {
        // try{
          $regUser =  $this->regRepo->firstStepRegister($request, $this->confirmationCode);
        $code=Storage::get('code');
          //store in confirmationcode
            if($regUser===true){
                $data=[
                    'regUser'=>$regUser,
                    'code'=>$code
                    ];
                return response()->json([
                    
                    'status'=>true,
                    'code'=>200,
                    'message'=>'finished from first step of registration ,move into step2: register your password ,  but before that, we send to your phone : code ,pls check this',
                    'data'=>$data
                    ]);
            }elseif($regUser==false){
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you arleady registered in this website , so you can move into step3 of registeration',
                    'data'=>null
                    ]);
            }elseif($regUser===2){
                return response()->json([
                     'status'=>false,
                    'code'=>400,
                    'message'=>'we sent to you the code aleady , so you can see it in your mobile and go into route confirmation code to write it',
                    'data'=>null
                    ]);
            }elseif($regUser===3){
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you cannt enter here , because you finished from three steps registration ',
                    'data'=>null
                    ]);
            }
        // }catch(\Exception $ex){
        //     return response()->json([
        //         'status'=>500,
        //         'message'=>'There is something wrong, please try again'
        //     ]);  
        // } 
    }
    public function secondStepRegisterPass(PasswordStepRegisterRequest $request)
    {
       // try{

          $regUserPass =  $this->regRepo->secondStepRegisterPass($request,$this->user);
                    $data=[
                    'regUser'=>$regUserPass
                    ];
          if($regUserPass===3){
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you cannt enter here , because you finished from three steps registration ',
                    'data'=>$data
                    ]);
            }if($regUserPass===-1){
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you cannt enter here , because you should go into register step1 before step2 ',
                    'data'=>$data
                    ]);
            }elseif($regUserPass==true){
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'finished from registration your password step ,move into step3 ',
                    'data'=>$data
                    ]);
            }elseif($regUserPass==false){
                 return response()->json([
                     'status'=>false,
                'code'=>400,
                'message'=>'you mustnt make this -> request this route , because this route need to go into register1 ,and confiem code after that you can move into this route(register password)',
                'data'=>$data
            ]);
            }elseif($regUserPass==0){
                                return response()->json([
                                    'status'=>false,
                'code'=>400,
                'message'=>'you must be go into route confirmation code to confirm your code after that you can go into this route',
                'data'=>$data
            ]); 
            }
       
        // }catch(\Exception $ex){
        //     return response()->json([
        //         'status'=>500,
        //         'message'=>'There is something wrong, please try again'
        //     ]);  
        // } 
    }
    
    //step3 for register
    public function thirdStepRegister(ThirdStepRegisterRequest $request)
    {
       // try{
         $userId = Storage::get('userId');
                         $regUser =  $this->regRepo->thirdStepRegister($request,$this->user,$userId);  
         if(!empty($userId)){
            $password = Storage::get('password');
            if(!empty($password)){
                
        if($regUser===3){
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you cannt enter here again , because you finished from it (three steps registration) ',
                    'data'=>$regUser
                    ]);
            }elseif($regUser===true){
                      $personal_id=Storage::get('personal_id');
                $data=[
                    'regUser'=>$regUser,
                     'personal_id'=>$personal_id,
                          'password'=>$password
                    ];
   
                      return response()->json([
                          'status'=>true,
                          'code'=>200,
                          'message'=>'registered successfully, will send to you the aggreement after reviweing your account,but  now you can make login via your data entered ',
                          'data'=>$data
                         
                      ]);
                  }elseif($regUser===1){
                      return response()->json([
                          'status'=>false,
                          'code'=>400,
                          'message'=>'you should be in first step after that move int this step2',
                          'data'=>$regUser
                      ]);
                  }elseif($regUser===0){
                      return response()->json([
                          'status'=>false,
                          'code'=>400,
                          'message'=>'you should make Approval of the agreement',
                          'data'=>$regUser
                      ]);
                  }elseif($regUser===2){
                      return response()->json([
                          'status'=>false,
                          'code'=>400,
                          'message'=>'you should make confimation for code that we send it to you',
                                                    'data'=>$regUser

                      ]);
                  }elseif($regUser===3){
                      return response()->json([
                          'status'=>false,
                          'code'=>400,
                          'message'=>'you cannt enter inro step1 because you finished from all steps , now you can make login vie your data entered ',
                          'data'=>$regUser
                      ]);
                  }
            }else{
                return response()->json([
                    'status'=>false,
                    'code'=>400,
                    'message'=>'you mustnt make this -> request this route , because this route need to go into register your password after that you can move into this route(register2)',
                    'data'=>$regUser
                ]);     
            }
         }else{
            return response()->json([
                'status'=>false,
                'code'=>400,
                'message'=>'you must dont work this , request this route , because this route need to go into register1 after that you can move into this route(register2)',
                'data'=>$regUser
            ]);
        }
        // }catch(\Exception $ex){
        //     return response()->json([
        //         'status'=>500,
        //         'message'=>'There is something wrong, please try again'
        //     ]);  
        // } 
    }
    public function fourthStepRegister(FourthStepRegisterRequest $request)
    {
        // try{
          $regUser =  $this->regRepo->fourthStepRegister($request, $this->user);
          if(is_object($regUser)){
          return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'registered completed',
                'data'=>$regUser
            ]);
              
          }elseif($regUser==400){
                        return response()->json([
                'status'=>true,
                'code'=>400,
                'message'=>'user  should be in first step after that move int this step2',
                'data'=>null
            ]);
          }
          
    }
    public function confirmationCode(ConfirmationCodeRequest $request){
        $checkCode=$this->regRepo->confirmationCode($request,$this->confirmationCode,$this->user);

        if($checkCode==true){
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'code is valid,,now move into step2 ',
                'data'=>$checkCode
            ]);
        }elseif($checkCode==false){
            return response()->json([
                'status'=>false,
                'code'=>400,
                'message'=>'Wrong....pls, write code  again ',
                                'data'=>$checkCode

            ]);
        }elseif($checkCode==0){
            return response()->json([
                'status'=>false,
                'code'=>400,
                'message'=>'pls, go into register step1 before confirm code',
                                'data'=>$checkCode

            ]);
        }
        
    }

    public function updatePhoneNo(Request $request){
     $updatePhoneNo=   $this->regRepo->updatePhoneNo($request,$this->user);
        if($updatePhoneNo==true){
            return  response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'your phone no has been updated',
                'data'=>$updatePhoneNo
            ]);
        }
    }
}
