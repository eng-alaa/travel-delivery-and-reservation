<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPasswordRequest;
use Modules\Auth\Entities\User;
use App\Providers\RouteServiceProvider;
use App\Repositories\Auth\Password\PasswordRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\AccountRecovery\Entities\AccountRecovery;
class ResetPasswordController extends Controller
{
    /**
     * @var PasswordRepository
     */
    protected $passwordRepo;
    public function __construct(User $user,AccountRecovery $accountRecovery,PasswordRepository $passwordRepo){
        $this->user = $user;
        $this->accountRecovery = $accountRecovery;
        $this->passwordRepo = $passwordRepo;
    }
    public function __invoke(ResetPasswordRequest $request)
    {
            $passwordReset=$this->passwordRepo->resetPassword($request,$this->user,$this->accountRecovery);
        // if($request->mobile==1){
            if($passwordReset==true){
                                return response()->json([
                    'status'=>true,
                'code'=>200,
                'message'=>'your request(reset pass) under reviewing , pls wait to accept on it , will reach to you a sms when accepting on it',
                'data'=>$passwordReset
            ]);
            }
        // }else{
        //                                     return response()->json([
        //             'status'=>false,
        //         'code'=>400,
        //         'message'=>'you should select a mobile or Property rights to make a reset password',
        //         'data'=>$passwordReset
        //     ]);
        // }
    }

}
