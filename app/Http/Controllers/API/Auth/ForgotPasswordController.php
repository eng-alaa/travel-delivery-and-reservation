<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\SelectOptionRequest;
use App\Mail\SendCodeResetPassword;
use App\Models\PasswordReset;
use Modules\Auth\Entities\User;
use App\Repositories\Auth\Password\PasswordRepository;
use App\Repositories\Auth\Sms\SmsRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    /**
     * @var User
     */
    protected $user;
    /**
     * @var PasswordReset
     */
    // protected $passwordReset;
    /**
     * @var PasswordRepository
     */
    protected $passwordRep;
    
    /**
     * @var SmsRepository
     */
    protected $smsRepo;
    public function __construct(User $user,PasswordReset $passwordReset,PasswordRepository $passwordRep,SmsRepository $smsRepo){
        $this->user = $user;
        $this->passwordRep = $passwordRep;
        $this->passwordReset = $passwordReset;
        $this->smsRepo = $smsRepo;
    }

    // Sends code into Email,phone to make PasswordReset 
    public function __invoke(ForgotPasswordRequest $request)
    {
        // if($request->mobile==1){//seclet from options : mobile
            $forgotPassword=$this->passwordRep->forgotPassword($request,$this->user,$this->passwordReset);
            
            $code=Storage::get('code');
            if($forgotPassword==true){
                             return response()->json([
                'status'=>true,
                 'code'=>200,
                'message'=>'code has been sent into your phone',
                'data'=>$code

            ]);
    
            }
        // }else{
        //     return response()->json([
        //         'status'=>400,
        //         'message'=>'you should select a mobile or Property rights to make a reset password',
        //     ]);
        // }
    }
    public function selectOption(SelectOptionRequest $request){
        if($request->mobile==1){
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'now you can make reset password through mobile',
                'data'=>'mobile'
            ]);
        }elseif($request->property_rights==1){
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'now you can make reset password through Property rights',
                'data'=>'property_rights'
            ]);

            
        }else{
            return response()->json([
                'status'=>false,
                'code'=>400,
                'message'=>'you should select a mobile or Property rights to make a reset password',
                'data'=>'not property_rights and not mobile'
            ]);
        }
    }
}
