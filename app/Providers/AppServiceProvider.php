<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Storage;
use Modules\Wallet\Entities\SystemWallet;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */

    public function register()
    {
        Session::put('applocale','en');
        // Storage::put('userId',null);
        // Storage::put('step1',null);
        // Storage::put('step2',null);
        // Storage::put('code',null);
        // Storage::put('confirmed',null);
        // Storage::put('sentCode',null);
        // Storage::put('finished',null);
        // Storage::put('sentCode',null);
        // systemWallet::latest()->first();
        // if($systemWallet->original_points_count<=1){
        //     $newSystemWallet=new SystemWallet();
        //     $newSystemWallet->original_points_count=50000;
        //     $newSystemWallet->save();

        // }

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
