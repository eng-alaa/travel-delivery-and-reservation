<?php

namespace Modules\Location\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Auth\Entities\User;
class Location extends Model
{
    use SoftDeletes;    
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'longitude',
        'latitude'
    ];
        public function user(){
        return $this->belongsTo(User::class);
    }
    
    
}
