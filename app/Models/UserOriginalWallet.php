<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOriginalWallet extends Model
{
    use HasFactory;
    protected $table="users_original_wallets";
}
