<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Wallet\Entities\PointsNotAllowedUse;
use Modules\Wallet\Entities\UserOriginalWallet;
use DB;
class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
$schedule->command('queue:work', [
'--max-time' => 300
])->withoutOverlapping();
        //after 24 hour , will move point from table points_not_allowed into original_wallet table
        // $schedule->command('points:allowed')->everyMinute()->runInBackground();
       //after 48 hour , will become : redeemRequets status from 0 into 1 , means that these requests will move from redeemrequets employee status=0 into delivery money employee status=1
    //    $schedule->command('redeemrequets:changedstatus')->everyMinute();
       //daily , will check wallet system if empty or not , if empty : create a new
      // $schedule->command('systemwallet:createnew')->everyMinute();
       $schedule->command('systemwallet:createnew')->cron('* * */2 * *');

        

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
