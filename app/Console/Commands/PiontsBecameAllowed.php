<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Wallet\Entities\PointsNotAllowedUse;
use Modules\Wallet\Entities\UserOriginalWallet;

class PiontsBecameAllowed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'points:allowed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $PointsNotAllowedUse = PointsNotAllowedUse::get();
        foreach($PointsNotAllowedUse as $PointNotAllowedUse){
                $UserOriginalWallet= UserOriginalWallet::where(['user_id'=>$PointNotAllowedUse->user_id])->get();
                if(!empty($UserOriginalWallet)){
                    foreach($UserOriginalWallet as $wallet){
                        $pointsAllowed= PointsNotAllowedUse::where(['user_id'=>$wallet->user_id])->first();
                        if(!empty($pointsAllowed)){
                            $pointsCountNumFromPointsNotAllowedUse = explode('_',$pointsAllowed->points_count, 2)[1];
                            $pointsAllowedCountNumFromWallet = explode('_',$wallet->allowed_points_count, 2)[1];
                            $totalPointAllowed=$pointsCountNumFromPointsNotAllowedUse+$pointsAllowedCountNumFromWallet;
                            $wallet->allowed_points_count='AR_'.$totalPointAllowed;
                            $wallet->save();
                            $PointNotAllowedUse->delete();
                        }
    
                    }
                }//else : not found this userin originalwallet , so must be original wallet for user after that be points for his : points in table pointsNotAllowed
                    
                    
        }
        return 0;
    }
}
