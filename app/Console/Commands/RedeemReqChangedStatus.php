<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Wallet\Entities\UserHiddenWallet;

class RedeemReqChangedStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redeemrequets:changedstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $UserHiddenWallet= UserHiddenWallet::get();
       foreach($UserHiddenWallet as $wallet){
          if($wallet->redeemed_points_count!=='0_0'){
              if($wallet->status!==1){
                  $wallet->status=1;
                  $wallet->save();
              }
          } 
       }
        return 0;
    }
}
