<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Wallet\Entities\SystemWallet;

class SystemWalletNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'systemwallet:createnew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $systemWallet= SystemWallet::latest()->first();
       info($systemWallet);
       if(!empty($systemWallet)){

           if($systemWallet->original_points_count===0){
               info('deeeeeeeee');
               $newSystemWallet=new SystemWallet();
               $newSystemWallet->original_points_count=50000;
               $newSystemWallet->save();
  
           }
       }else{
            $newSystemWallet=new SystemWallet();
            $newSystemWallet->original_points_count=50000;
            $newSystemWallet->save();

       }
        return 0;
    }
}
